﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Dominio.Models.Querys
{
    public class Qry_Proyecto
    {
        public static List<Proyecto> proyecto_cliente(List<int> idsCliente)
        {
            using (var db = new AsistenciaEntities())
            {
                var lstProyecto = db.Proyecto.Where(s => idsCliente.Contains(s.IdCliente)).ToList();
                return lstProyecto;
            }
        }

        public static List<Proyecto> proyecto_usuario(int idUsuario)
        {
            using (var db = new AsistenciaEntities())
            {
                var lstProyecto = (from up in db.UsuarioProyecto.Where(s => s.IdUsuario == idUsuario)
                                   join p in db.Proyecto on up.IdProyecto equals p.IdProyecto
                                   select p).ToList();
                return lstProyecto;
            }
        }

    }
}
