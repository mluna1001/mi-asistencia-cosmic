﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MiAsistencia.Dominio.Models.Querys
{
    public class Qry_Bitacora
    {
        Cosmic.Response response = new Cosmic.Response();

        public static Cosmic.Response SaveBinnacle(List<Bitacora> lstBitacora)
        {
            Cosmic.Response response = new Cosmic.Response();

            if (lstBitacora != null || lstBitacora.Count > 0)
            {
                using (var db = new AsistenciaEntities())
                {
                    using (var transaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            var listainsert = lstBitacora.Select(l => {
                                l.FechaAlta = DateTime.Now;
                                return l;
                            });
                            db.Bitacora.AddRange(listainsert);
                            db.SaveChanges();
                            transaction.Commit();
                            response.Message = "Bitacora cargada correctamente";
                            response.Success = true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            response.Message = "Intenta subir más tarde";
                            response.Success = false;
                            response.excepcion = ex;
                        } 
                    }
                }
            }

            if (response.excepcion != null)
            {
                var mappedPath = HttpContext.Current.Request.Url.ToString();
                Qry_Log.InsertLog(response.excepcion, lstBitacora[0].IdUsuario, null, mappedPath);
            }

            return response;
        }
    }
}
