﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Dominio.Models.Querys
{
    public class Qry_Cliente
    {
        public static List<Cliente> cliente_usuario(int idUsuario)
        {
            using (var db = new AsistenciaEntities())
            {
                var lstCliente = (from c in db.Cliente
                                  join uc in db.UsuarioCliente.Where(s => s.IdUsuario == idUsuario) on c.IdCliente equals uc.IdCliente
                                  select c).ToList();
                return lstCliente;
            }
        }
    }
}
