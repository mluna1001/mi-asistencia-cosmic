﻿using Cosmic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Dominio.Models.Querys
{
    public class Qry_RespaldoBDUsuario
    {
        public static Response Save(RespaldoBDUsuario respaldoBD)
        {
            Response response = new Response();

            if (respaldoBD != null)
            {
                using (var db = new AsistenciaEntities())
                {
                    try
                    {
                        db.RespaldoBDUsuario.Add(respaldoBD);
                        db.SaveChanges();
                        response.Success = true;
                        response.Message = "La base de datos ha sido salvada en el servidor. Fecha: " + respaldoBD.Fecha;
                    }
                    catch (Exception ex)
                    {
                        response.Success = false;
                        response.Message = "La base de datos no pudo salvarse en el servidor. Intenta más tarde";
                        response.excepcion = ex;
                    } 
                }
            }

            return response;
        }
    }
}
