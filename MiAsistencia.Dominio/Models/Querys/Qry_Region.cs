﻿using MiAsistencia.Dominio.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Dominio.Models.Querys
{
    public class Qry_Region
    {
        public static List<RegionDto> region_proyecto(List<int> lstProyecto)
        {
            using (var db = new AsistenciaEntities())
            {
                var lstRegion = (from pr in db.ProyectoRegion.Where(s => lstProyecto.Contains(s.IdProyecto))
                                 join r in db.Region on pr.IdRegion equals r.IdRegion
                                 select new RegionDto()
                                 {
                                     IdCliente = r.IdCliente,
                                     IdProyecto = pr.IdProyecto,
                                     Descripcion = r.Descripcion,
                                     IdRegion = r.IdRegion
                                 }).ToList();

                return lstRegion;
            }
        }

        public static List<Region> region_usuario(int idUsuario)
        {
            using (var db = new AsistenciaEntities())
            {
                var lstRegion = (from pr in db.UsuarioRegion.Where(s => s.IdUsuario == idUsuario)
                                 join r in db.Region on pr.IdRegion equals r.IdRegion
                                 select r).ToList();

                return lstRegion;
            }
        }
    }
}
