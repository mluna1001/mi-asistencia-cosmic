﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Dominio.Models.Querys
{
    public class Qry_Log
    {
        public static void InsertLog(Exception exception, int? IdUsuario, int? IdTienda, string proceso)
        {
            try
            {
                using (var db = new AsistenciaEntities())
                {
                    Log log = new Log
                    {
                        Proceso = proceso,
                        ExcepcionError = exception.InnerException != null ? exception.Message + "/" + exception.InnerException.Message : exception.Message,
                        IdUsuario = IdUsuario,
                        IdTienda = IdTienda,
                        Fecha = DateTime.Now
                    };

                    db.Log.Add(log);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
