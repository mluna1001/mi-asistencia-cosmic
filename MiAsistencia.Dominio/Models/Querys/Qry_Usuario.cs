﻿using MiAsistencia.Dominio.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cosmic;
using System.Data.SqlClient;
using MiAsistencia.Comun.Models.DTO;

namespace MiAsistencia.Dominio.Models.Querys
{
    public class Qry_Usuario
    {
        public static UsuarioDto GetUsuario(int IdUsuario)
        {
            UsuarioDto usuario = new UsuarioDto();

            using (var db = new AsistenciaEntities())
            {
                usuario.IdUsuario = IdUsuario;
                usuario.UsuarioCliente = db.UsuarioCliente.FirstOrDefault(u => u.IdUsuario == IdUsuario);
                usuario.UsuarioProyecto = db.UsuarioProyecto.FirstOrDefault(u => u.IdUsuario == IdUsuario);
                usuario.UsuarioRegion = db.UsuarioRegion.FirstOrDefault(u => u.IdUsuario == IdUsuario);
                usuario.UsuarioLunch = db.UsuarioLunch.FirstOrDefault(u => u.IdUsuario == IdUsuario);
            }

            return usuario;
        }

        public static List<UsuarioDto> lstUsuario(List<Region> lstRegion)
        {
            List<UsuarioDto> lstUsuario = new List<UsuarioDto>();

            using (var db = new AsistenciaEntities())
            {
                string sRegion = lstRegion.Select(f => f.IdRegion).ToList().intToString();
                var parameter = new SqlParameter("@IdRegion", sRegion);
                return db.Database.SqlQuery<spu_Usuario_Region_Result>("exec spu_Usuario_Region @IdRegion", parameter).To<List<UsuarioDto>>().ToList();
            }
        }

        public static List<UsuarioDto> getTreeUser(List<Region> lstRegion)
        {
            List<UsuarioDto> lstUsuario = new List<UsuarioDto>();

            using (var db = new AsistenciaEntities())
            {
                string sRegion = lstRegion.Select(f => f.IdRegion).ToList().intToString();
                var parameter = new SqlParameter("@IdRegion", sRegion);
                return db.Database.SqlQuery<spu_Asistencias_Result>("exec spu_Usuario_Region @IdRegion", parameter).To<List<UsuarioDto>>().ToList();
            }
        }

        public static Response Register(int IdUsuario)
        {
            var response = new Response();

            UsuarioCliente usuarioCliente = new UsuarioCliente
            {
                IdCliente = (int)PublicRegister.Cliente.Publico,
                IdUsuario = IdUsuario
            };

            UsuarioProyecto usuarioProyecto = new UsuarioProyecto
            {
                IdProyecto = (int)PublicRegister.Proyecto.Publico,
                IdUsuario = IdUsuario
            };

            UsuarioRegion usuarioRegion = new UsuarioRegion
            {
                IdRegion = (int)PublicRegister.Region.Publico,
                IdUsuario = IdUsuario
            };

            using (var db = new AsistenciaEntities())
            {
                try
                {
                    db.UsuarioCliente.Add(usuarioCliente);
                    db.UsuarioProyecto.Add(usuarioProyecto);
                    db.UsuarioRegion.Add(usuarioRegion);
                    db.SaveChanges();

                    response.Success = true;
                    response.Message = "Su usuario ha sido registrado correctamente";
                }
                catch (Exception ex)
                {
                    response.excepcion = ex;
                    response.Success = false;
                    response.Message = "En estos momentos no puede realizarse el registro. Intente nuevamente.";
                }
            }

            return response;
        }

        public static Response Register(UsuarioRegistro userInserted)
        {
            var response = new Response();

            UsuarioCliente usuarioCliente = new UsuarioCliente
            {
                IdCliente = (int)PublicRegister.Cliente.Publico,
                IdUsuario = userInserted.IdUsuario
            };

            UsuarioProyecto usuarioProyecto = new UsuarioProyecto
            {
                IdProyecto = (int)PublicRegister.Proyecto.Publico,
                IdUsuario = userInserted.IdUsuario
            };

            UsuarioRegion usuarioRegion = new UsuarioRegion
            {
                IdRegion = (int)PublicRegister.Region.Publico,
                IdUsuario = userInserted.IdUsuario
            };

            using (var db = new AsistenciaEntities())
            {
                try
                {
                    db.UsuarioCliente.Add(usuarioCliente);
                    db.UsuarioProyecto.Add(usuarioProyecto);
                    db.UsuarioRegion.Add(usuarioRegion);
                    db.SaveChanges();

                    response.Success = true;
                    response.Message = "Su usuario ha sido registrado correctamente";
                }
                catch (Exception ex)
                {
                    response.excepcion = ex;
                    response.Success = false;
                    response.Message = "En estos momentos no puede realizarse el registro. Intente nuevamente.";
                }
            }

            return response;
        }

        public static Response ExistsUserInUC(usuario model)
        {
            var response = new Response();

            using (var db = new UsuarioCosmic.UsuarioCosmicEntities())
            {
                try
                {
                    var user = db.Usuario.FirstOrDefault(u => u.NoEmpleado == model.NoEmpleado);
                    if (user != null)
                    {
                        int iduser = user.IdUsuario;

                        using (var transaction = db.Database.BeginTransaction())
                        {
                            var programaUsuario = new UsuarioCosmic.ProgramaUsuario { IdUsuario = iduser, IdPrograma = Guid.Parse("1e9e8237-7819-4931-B73f-04b51f6c280d"), Acceso = true };
                            db.ProgramaUsuario.Add(programaUsuario);

                            // UsuarioPerfil
                            var perfil = new UsuarioCosmic.UsuarioPerfil { IdPerfil = (int)Comun.Models.DTO.PublicRegister.Perfil.Empleado, IdUsuario = iduser };
                            db.UsuarioPerfil.Add(perfil);

                            // Menu
                            var menu = (from mp in db.PerfilMenu
                                        where mp.IdPerfil == (int)Comun.Models.DTO.PublicRegister.Perfil.Empleado
                                        select new
                                        {
                                            IdMenu = mp.IdMenu,
                                            IdUsuario = iduser,
                                            Orden = mp.Orden
                                        }).To<List<UsuarioCosmic.UsuarioMenu>>();
                            db.UsuarioMenu.AddRange(menu);
                            db.SaveChanges();

                            transaction.Commit();
                        }

                        response.Message = "El perfil del usuario se ha registrado correctamente.";
                        response.Data = iduser;
                        response.Success = true;
                    }
                    else
                    {
                        response.Message = "El usuario no existe, debe registrarse";
                        response.Success = false; 
                    }
                }
                catch (Exception ex)
                {
                    response.Message = "El usuario no existe, debe registrarse";
                    response.Success = false;
                    response.excepcion = ex;
                }
            }

            return response;
        }

        public static Response Exists(usuario model)
        {
            var response = new Response();
            Empleado.UsuarioAlta usuarioAlta = new Empleado.UsuarioAlta();
            List<Programas> programs = new List<Programas>();

            using (var db = new Empleado.EmpleadoCosmicEntities())
            {
                usuarioAlta = db.UsuarioAlta.FirstOrDefault(u => u.empleado.Equals(model.NoEmpleado.ToString()));
            }

            using (var db = new UsuarioCosmic.UsuarioCosmicEntities())
            {
                programs = (from u in db.Usuario
                            join pu in (db.ProgramaUsuario.Where(pu => pu.Acceso == true)) on u.IdUsuario equals pu.IdUsuario
                            join p in db.Programa on pu.IdPrograma equals p.IdPrograma
                            where u.NoEmpleado == model.NoEmpleado
                            select new Programas { Programa = p.Programa1, IdPrograma = p.IdPrograma }).Distinct().ToList();
            }

            if (usuarioAlta != null)
            {
                response.Success = true;
                response.Message = "Usuario existente.";
                usuarioAlta = usuarioAlta.To<Empleado.UsuarioAlta>();
                usuarioAlta.Programas = programs;
                response.Data = usuarioAlta;
            }
            else
            {
                response.Success = false;
                response.Message = "Usuario no existe.";
            }

            return response;
        }

        public static List<OptionTree> GetChildren(int idParent, string idProyecto, string idRegion)
        {
            using (var db = new AsistenciaEntities())
            {
                var param = new SqlParameter("@IdUsuario", idParent);
                var param2 = new SqlParameter("@IdProyecto", idProyecto);
                var param3 = new SqlParameter("@IdRegion", idRegion);
                return db.Database.SqlQuery<spu_Usuario_Children_Result>("exec spu_Usuario_Children @IdUsuario, @IdProyecto, @IdRegion", param, param2, param3).To<List<OptionTree>>().ToList();
            }
        }

        public static List<UsuarioDto> find(int idUsuario)
        {
            List<UsuarioDto> lstUsuario = new List<UsuarioDto>();

            using (var db = new AsistenciaEntities())
            {
                var parameter = new SqlParameter("@IdUsuario", idUsuario);
                return db.Database.SqlQuery<spu_Asistencias_Result>("exec spu_Usuario @IdUsuario", parameter).To<List<UsuarioDto>>().ToList();
            }
        }

        public static List<spu_Usuario_Lista_Result> findlstUsuario(string idUsuario)
        {
            List<UsuarioDto> lstUsuario = new List<UsuarioDto>();

            using (var db = new AsistenciaEntities())
            {
                var parameter = new SqlParameter("@usuarios", idUsuario);
                return db.Database.SqlQuery<spu_Usuario_Lista_Result>("exec spu_Usuario_Lista @usuarios", parameter).ToList();
            }
        }

        public static Cosmic.Response GetSubordinados(int idUsuario)
        {
            Cosmic.Response response = new Response();
            List<Subordinado> lstSubordinado = new List<Subordinado>();

            try
            {
                using (var db = new AsistenciaEntities())
                {
                    int idCliente = db.UsuarioCliente.FirstOrDefault(c => c.IdUsuario == idUsuario).IdCliente;

                    var obj = db.UsuarioRegion.FirstOrDefault(r => r.IdUsuario == idUsuario);
                    var idRegion = obj != null ? obj.IdRegion : 0;
                    var lst = db.usp_UsuarioCliente_Obtiene(idCliente, idRegion).ToList();

                    if (lst != null && lst.Count > 0)
                    {
                        lstSubordinado = lst.To<List<Subordinado>>();
                    }
                }

                response.Data = lstSubordinado;
                response.Message = "Usuarios descargados correctamente";
                response.Success = true;

            }
            catch (Exception ex)
            {
                response.excepcion = ex;
                response.Success = false;
                response.Message = "No hay usuarios para descargar con este perfil, intente con otra cuenta.";
            }


            return response;
        }
    }
}
