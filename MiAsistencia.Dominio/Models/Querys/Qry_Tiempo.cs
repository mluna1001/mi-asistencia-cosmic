﻿using Cosmic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Dominio.Models.Querys
{
    public class Qry_Tiempo
    {
        public static List<Tiempo> rangoTiempo(DateTime fechainicial, DateTime fechafinal)
        {
            using (var db = new AsistenciaEntities())
            {
                var lsttiempo = db.Tiempo.Where(f => f.Fecha >= fechainicial && f.Fecha <= fechafinal && f.Fecha <= DateTime.Now).ToList();
                return lsttiempo;
            }
        }

        public static string rangoidsTiempo(DateTime fechainicial, DateTime fechafinal)
        {
            string idsTiempo = "";
            using (var db = new AsistenciaEntities())
            {
                idsTiempo = db.Tiempo.Where(s => s.Fecha >= fechainicial && s.Fecha <= fechafinal).Select(s => s.IdTiempo).ToList().intToString();
            }
            return idsTiempo;
        }
    }
}
