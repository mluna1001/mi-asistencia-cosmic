﻿using MiAsistencia.Dominio.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cosmic;
using System.Web;

namespace MiAsistencia.Dominio.Models.Querys
{
    public static class Qry_Asistencia
    {
        public static Asistencia find(int idAsistencia)
        {
            using (var db = new AsistenciaEntities())
            {
                return db.Asistencia.Find(idAsistencia);
            }
        }

        public static List<Asistencia> read(int idUsuario)
        {
            using (var db = new AsistenciaEntities())
            {
                return db.Asistencia.Where(s => s.IdUsuario == idUsuario).ToList();
            }
        }

        public static Cosmic.Response SaveCheck(Asistencia asistencia)
        {
            Cosmic.Response response = new Cosmic.Response();
            int idTiempo = 0;

            if (asistencia.IdTipoAsistencia == null || asistencia.IdTipoAsistencia == 0)
            {
                asistencia.IdTipoAsistencia = asistencia.Estatus ? 1 : 4;
            }
            
            if (asistencia != null)
            {
                using (var db = new AsistenciaEntities())
                {
                    idTiempo = db.Tiempo.First(x => x.Fecha == asistencia.Fecha.Date).IdTiempo;

                    using (var transaccion = db.Database.BeginTransaction())
                    {
                        try
                        {
                            asistencia.IdTiempo = idTiempo;
                            asistencia.FechaAlta = DateTime.Now;
                            db.Entry(asistencia).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                            transaccion.Commit();
                            response.Success = true;
                            response.Message = "Asistencia guardada correctamente: " + (asistencia.Estatus ? "Entrada" : "Salida");
                            asistencia.Fecha = ValidaFecha(asistencia.Fecha);
                            response.Data = asistencia.To<Asistencia>();
                        }
                        catch (Exception ex)
                        {
                            transaccion.Rollback();
                            response.excepcion = ex;
                            response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                            response.Success = false;
                            Qry_Log.InsertLog(ex, asistencia.IdUsuario, asistencia.IdTienda, "SaveCheck");
                        }
                    }
                }
            }

            if (response.excepcion != null)
            {
                var mappedPath = HttpContext.Current.Request.Url.ToString();
                Qry_Log.InsertLog(response.excepcion, asistencia.IdUsuario, asistencia.IdTienda, mappedPath);
            }

            return response;
        }

        public static Cosmic.Response SaveSignature(Recepcion recepcion)
        {
            Cosmic.Response response = new Response();
            //int idTiempo = 0;

            if (recepcion != null)
            {
                using (var db = new AsistenciaEntities())
                {
                    //idTiempo = db.Tiempo.First(x => x.Fecha == recepcion.FechaRecibe.Date).IdTiempo;

                    using (var transaccion = db.Database.BeginTransaction())
                    {
                        try
                        {
                            //recepcion.IdTiempo = idTiempo;
                            recepcion.FechaRecibe = DateTime.Now;
                            db.Entry(recepcion).State = System.Data.Entity.EntityState.Added;
                            db.SaveChanges();
                            transaccion.Commit();
                            response.Success = true;
                            response.Message = "Firma de recepción guardada correctamente.";
                            recepcion.FechaRecibe = ValidaFecha(recepcion.FechaRecibe);
                            response.Data = recepcion.To<Asistencia>();
                        }
                        catch (Exception ex)
                        {
                            transaccion.Rollback();
                            response.excepcion = ex;
                            response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                            response.Success = false;
                        }
                    }
                }
            }

            return response;
        }

        public static Cosmic.Response ReadCheckByUser(Asistencia objAsistencia)
        {
            Cosmic.Response response = new Response();

            using (var db = new AsistenciaEntities())
            {
                var asistUser = db.spu_Asistencia_ObtienePorUsuario(objAsistencia.IdUsuario).Select(a => new spu_Asistencia_ObtienePorUsuario_Result
                {
                    IdAsistencia = a.IdAsistencia,
                    IdTienda = a.IdTienda,
                    IdUsuario = a.IdUsuario,
                    Latitud = a.Latitud,
                    Longitud = a.Longitud,
                    NoEmpleado = a.NoEmpleado,
                    Nombre = a.Nombre,
                    Tienda = a.Tienda,
                    Fecha = a.Fecha.ToLocalTime(),
                    Estatus = a.Estatus
                }).ToList();
                if (asistUser != null)
                {
                    response.Message = "Asistencia del día de hoy";
                    response.Data = asistUser;
                    response.Success = true;
                }
                else
                {
                    response.Message = "No hay asistencias realizadas el día de hoy para este usuario.";
                }
                response.Success = true;
            }

            return response;
        }

        private static DateTime ValidaFecha(DateTime fecha)
        {
            DateTime fechaTZ = new DateTime();
            if (fecha != null)
            {

            }

            return fechaTZ;
        }

        public static Cosmic.Response SaveAnyCheck(List<Asistencia> lstasistencias)
        {
            Cosmic.Response response = new Cosmic.Response();

            if (lstasistencias != null || lstasistencias.Count > 0)
            {
                using (var db = new AsistenciaEntities())
                {
                    using (var transaccion = db.Database.BeginTransaction())
                    {
                        try
                        {
                            var listaInsert = lstasistencias.Select(l =>
                            {
                                l.FechaAlta = DateTime.Now;
                                l.IdTiempo = db.Tiempo.FirstOrDefault(t => t.Fecha == l.Fecha.Date).IdTiempo;
                                if (l.IdTipoAsistencia == null || l.IdTipoAsistencia == 0)
                                {
                                    l.IdTipoAsistencia = l.Estatus ? 1 : 4; 
                                }
                                return l;
                            }).ToList();
                            db.Asistencia.AddRange(listaInsert);
                            //foreach (var item in lstasistencias)
                            //{
                            //    item.FechaAlta = DateTime.Now;
                            //    var fecha = item.Fecha.Date;
                            //    var idtiempo = db.Tiempo.First(t => t.Fecha == fecha).IdTiempo;
                            //    db.Asistencia.Add(item);
                            //}
                            db.SaveChanges();
                            transaccion.Commit();
                            response.Success = true;
                            response.Message = "Asistencias fuera de linea guardadas correctamente.";
                        }
                        catch (Exception ex)
                        {
                            transaccion.Rollback();
                            response.excepcion = ex;
                            response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                            response.Success = false;
                        }
                    }
                }
            }

            if (response.excepcion != null)
            {
                var mappedPath = HttpContext.Current.Request.Url.ToString();
                Qry_Log.InsertLog(response.excepcion, lstasistencias[0].IdUsuario, null, mappedPath);
            }

            return response;
        }

        public static List<spu_Asistencias_Result> readAsistencia(string usuarios, string tiempol)
        {
            using (var db = new AsistenciaEntities())
            {
                Qry_Log.InsertLog(new Exception(), 1, 1, " entro aqui");
                var parameter = new SqlParameter("@usuarios", usuarios);
                var parameter2 = new SqlParameter("@IdsTiempo", tiempol);
                return db.Database.SqlQuery<spu_Asistencias_Result>("exec spu_Asistencias @usuarios, @IdsTiempo", parameter, parameter2).ToList();
            }
        }
    }
}
