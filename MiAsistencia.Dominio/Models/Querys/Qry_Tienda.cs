﻿using Cosmic;
using MiAsistencia.Comun.Models.ORM;
using MiAsistencia.Dominio.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiAsistencia.Dominio.Models.Querys
{
    public class Qry_Tienda
    {
        public static Response GetTiendas(int IdUsuario, decimal latitud, decimal longitud)
        {
            Response response = new Response();
            UsuarioDto usuario = new UsuarioDto();
            List<Tienda> lstTiendas = new List<Tienda>();

            try
            {
                usuario = Qry_Usuario.GetUsuario(IdUsuario);

                if (usuario != null)
                {
                    using (var db = new AsistenciaEntities())
                    {
                        lstTiendas = db.spu_Tienda_ObtieneTiendaCliente(usuario.UsuarioCliente.IdCliente.ToString()).To<List<Tienda>>();
                    }

                    if (lstTiendas.Count == 0)
                    {
                        response.Message = "No hay tiendas que mostrar para este usuario";
                        response.Success = false;
                    }
                    else
                    {
                        var maxcx = longitud + (decimal)0.07;
                        var mincx = longitud - (decimal)0.07;
                        var maxcy = latitud + (decimal)0.07;
                        var mincy = latitud - (decimal)0.07;

                        lstTiendas = lstTiendas.Where(s => s.Longitud < maxcx && s.Longitud > mincx && s.Latitud < maxcy && s.Latitud > mincy).ToList();

                        response.Data = lstTiendas;
                        response.Message = "Las tiendas del usuario se obtienen correctamente";
                        response.Success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                var mappedPath = HttpContext.Current.Request.Url.ToString();
                Qry_Log.InsertLog(ex, IdUsuario, null, mappedPath);
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                response.Success = false;
                response.excepcion = ex;
            }

            return response;
        }

        public static Response GetAllTiendas(int IdUsuario)
        {
            Response response = new Response();
            UsuarioDto usuario = new UsuarioDto();
            List<Tienda> lstTiendas = new List<Tienda>();

            try
            {
                usuario = Qry_Usuario.GetUsuario(IdUsuario);

                if (usuario != null)
                {
                    using (var db = new AsistenciaEntities())
                    {
                        lstTiendas = db.spu_Tienda_ObtieneTiendaUsuario(IdUsuario).To<List<Tienda>>();
                        //lstTiendas = db.spu_Tienda_ObtieneTiendaCliente(usuario.UsuarioCliente.IdCliente.ToString()).To<List<Tienda>>();
                    }

                    response.Data = lstTiendas;
                    response.Message = "Las tiendas del usuario se obtienen correctamente";
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                var mappedPath = HttpContext.Current.Request.Url.ToString();
                Qry_Log.InsertLog(ex, IdUsuario, null, mappedPath);
                response.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                response.Success = false;
                response.excepcion = ex;
            }

            return response;
        }

        public static Response GetCompanies()
        {
            var response = new Response();

            using (var db = new AsistenciaEntities())
            {
                try
                {
                    var lst = db.RazonSocial.To<List<RazonSocial>>();
                    response.Data = lst;
                    response.Success = true;
                    response.Message = "Compañias obtenidas correctamente.";
                }
                catch (Exception ex)
                {
                    response.excepcion = ex;
                    response.Success = false;
                    response.Message = "No pueden obtenerse las empresas, intentalo más tarde";
                }
            }

            return response;
        }
    }
}
