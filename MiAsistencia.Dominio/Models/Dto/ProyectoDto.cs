﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Dominio.Models.Dto
{
    public class ProyectoDto
    {
        public int IdProyecto { get; set; }
        public int IdCliente { get; set; }
        public string Descripcion { get; set; }
        public bool disabled { get; set; }
    }
}
