﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Dominio.Models.Dto
{
    public class AsistenciaReportDto
    {
        public Nullable<int> idasistencia { get; set; }
        public int IdTienda { get; set; }
        public string NombreTienda { get; set; }
        public string Formato { get; set; }
        public string CP { get; set; }
        public string Cadena { get; set; }
        public Nullable<decimal> TiendaLat { get; set; }
        public Nullable<decimal> TiendaLong { get; set; }
        public Nullable<System.DateTime> FechaAlta { get; set; }
        public string Determinante { get; set; }
        public int idusuario { get; set; }
        public string NombreUsuario { get; set; }
        public bool Estatus { get; set; }
        public Nullable<int> dia { get; set; }
        public int anio { get; set; }
        public int mesdelanio { get; set; }
        public int semanadelanio { get; set; }
        public int diasemana { get; set; }
        public string Hora { get; set; }
        public System.DateTime fecha { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
    }

    public class FilterData
    {
        public List<RegionDto> regionDto { get; set; }
        public List<OptionTree> userTree { get; set; }

    }

    public class OptionTree
    {
        public OptionTree()
        {
            children = new List<OptionTree>();
        }
        public int id { get; set; }
        public string label { get; set; }
        public int level { get; set; }
        public int? idParent { get; set; }
        public int idRegion { get; set; }
        public int idProyecto { get; set; }
        public List<OptionTree> children { get; set; }
    }

    public class CheckUserList
    {
        public int Anio { get; set; }
        public int Mes { get; set; }
        public int Semana { get; set; }
        public int? Dia { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public DateTime Fecha { get; set; }
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public int? NumEmpleado { get; set; }
        public int TaskID { get; set; }
        public string TaskName { get; set; }
        public string Progress { get; set; }
        public List<CheckUserList> Children { get; set; }
        public bool Estatus { get; set; }
        public Nullable<int> IdAsistenciaEntrada { get; set; }
        public Nullable<int> IdAsistenciaSalida { get; set; }
        public bool IsParent { get; set; }
        public int IdTienda { get; set; }
        public string Cadena { get; set; }
        public string Formato { get; set; }
        public string CodigoPostal { get; set; }
        public string NombreTienda { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public decimal LatitudEntrada { get; set; }
        public decimal LongitudEntrada { get; set; }
        public decimal LatitudSalida { get; set; }
        public decimal LongitudSalida { get; set; }
        public DateTime FechaAlta { get; set; }
        public string InfoGoogle { get; set; }
        public string Determinante { get; set; }
        public string DesTipoAsistencia { get; set; }
        public int IdTipoAsistencia { get; set; }
        public TimeSpan estanciaDia { get; set; }
        public TimeSpan estanciaTienda { get; set; }
    }

    public class SchedulePerson
    {

        public int Id { get; set; }
        public string Subject { get; set; }
        public string OwnerId { get; set; }
        public string NombreUsuario { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool AllDay { get; set; }
        public bool Recurrence { get; set; }
        public string RecurrenceRule { get; set; }
        public Nullable<int> IdAsistenciaEntrada { get; set; }
        public Nullable<int> IdAsistenciaSalida { get; set; }
        public int IdTienda { get; set; }
        public string Cadena { get; set; }
        public string Formato { get; set; }
        public string CodigoPostal { get; set; }
        public string NombreTienda { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }


        public decimal LatitudEntrada { get; set; }
        public decimal LongitudEntrada { get; set; }
        public decimal LatitudSalida { get; set; }
        public decimal LongitudSalida { get; set; }


        public DateTime FechaAlta { get; set; }
        public string InfoGoogle { get; set; }
        public string Determinante { get; set; }
        public string StartTimeTxt { get; set; }
        public string EndTimeTxt { get; set; }
        public string Estatustxt { get; set; }

        public string Entrada { get; set; }
        public string Salida { get; set; }
        public int IdTipoAsistencia { get; set; }
        public string DesTipoAsistencia { get; set; }
    }

    public class ScheduleRooms
    {
        public string Text;
        public string Id;
        public string Color;
        public int? NumEmpleado;
        public string NombreUsuario { get; set; }
    }

    public class DataAsistencia
    {
        public DataAsistencia()
        {
            this.Asistencia = new HashSet<DateAsistencia>();
        }
        public int IdUsuario { get; set; }
        public int? NumEmpleado { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public int IdRegion { get; set; }
        public string Region { get; set; }
        public string Puesto { get; set; }
        public int Id { get; set; }
        public int DiasAsistidos { get; set; }
        public int? IdParent { get; set; }
        public int IdProyecto { get; set; }
        public string Proyecto { get; set; }
        public string Supervisor { get; set; }
        public ICollection<DateAsistencia> Asistencia { get; set; }
    }

    public class DateAsistencia
    {
        public int IdDia { get; set; }
        public string Dia { get; set; }
        public string Asistio { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Cadena { get; set; }
        public string Formato { get; set; }
        public int IdTienda { get; set; }
        public string NombreTienda { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }

        public decimal LatitudEntrada { get; set; }
        public decimal LatitudSalida { get; set; }

        public decimal LongitudEntrada { get; set; }
        public decimal LongitudSalida { get; set; }
        public int IdTipoAsistencia { get; set; }
        public string DesTipoAsistencia { get; set; }
    }

    public class Header
    {
        public string text { get; set; }
        public Nullable<int> dia { get; set; }
        public string value { get; set; }
        public string align { get; set; }
        public bool showc { get; set; }
        public int numdia { get; set; }
        public bool sortable { get; set; }
        
    }

    public class Reporte
    {
        public Reporte()
        {
            this.dataAsistencia = new HashSet<DataAsistencia>();
            this.Header = new HashSet<Header>();
            this.checkAsistencia = new HashSet<CheckUserList>();
        }
        public string fecha { get; set; }
        public ICollection<DataAsistencia> dataAsistencia { get; set; }
        public ICollection<Header> Header { get; set; }
        public ICollection<CheckUserList> checkAsistencia { get; set; }
    }

    public class Pagination
    {
        public int page { get; set; }
        public int rowsPerPage { get; set; }
        public string sortBy { get; set; }
    }
}
