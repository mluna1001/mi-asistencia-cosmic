﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Dominio.Models.Dto
{
    public class Programas
    {
        public Guid IdPrograma { get; set; }
        public string Programa { get; set; }
    }
}
