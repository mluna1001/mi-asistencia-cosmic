﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Dominio.Models.Dto
{
    public class UsuarioDto
    {
        public int IdUsuario { get; set; }
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Alias { get; set; }
        public int IdParent { get; set; }
        public UsuarioCliente UsuarioCliente { get; set; }
        public UsuarioProyecto UsuarioProyecto { get; set; }
        public UsuarioRegion UsuarioRegion { get; set; }
        public UsuarioLunch UsuarioLunch { get; set; }
    }
}
