﻿//using Plugin.Geofence.Abstractions;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MiAsistencia.XF.Helpers
{
    public class GeoTools
    {        
        public static bool IsLocationAvailable()
        {
            if (!CrossGeolocator.IsSupported)
                return false;

            return CrossGeolocator.Current.IsGeolocationAvailable;
        }

        public static async Task<Position> GetPositionAsync()
        {
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 150;

            var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10));

            return position;
        }

        public static async Task<Position> GetCachedLocation()
        {
            var cached = await CrossGeolocator.Current.GetLastKnownLocationAsync();

            return cached;
        }

        public static async Task StartListening()
        {
            await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(10), 200, true, new Plugin.Geolocator.Abstractions.ListenerSettings
            {
                ActivityType = ActivityType.Fitness,
                AllowBackgroundUpdates = true,
                DeferLocationUpdates = true,
                DeferralDistanceMeters = 100,
                ListenForSignificantChanges = true,
                PauseLocationUpdatesAutomatically = false
            });
            CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
        }

        private static void Current_PositionChanged(object sender, PositionEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var p = e.Position;
                var t = p.Timestamp.ToString();
                var h = p.Heading.ToString();
                var s = p.Speed.ToString();
                var a = p.Accuracy.ToString();
                var al = p.Altitude.ToString();
                var ac = p.AltitudeAccuracy.ToString();
            });
        }
    }
}
