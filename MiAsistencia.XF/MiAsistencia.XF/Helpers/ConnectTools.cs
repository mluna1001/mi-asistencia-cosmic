﻿namespace MiAsistencia.XF.Helpers
{
    using Cosmic;
    using Plugin.Connectivity;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;

    public class ConnectTools
    {
        public static async Task<Response> CheckConnection(string ruta)
        {
            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    return new Response
                    {
                        Success = false,
                        Message = "Favor de encender tu internet"
                    };
                }
                var isr = await IsRemoteReachable();
                return isr;
            }
            catch (Exception ex)
            {
                return new Response
                {
                    Success = false,
                    Message = ex.ToString()
                };
            }
        }

        public static async Task<Response> IsRemoteReachable(string ruta = "google.com")
        {
            bool isreacheable = false;
            if (ruta.Contains("localhost"))
            {
                int port = int.Parse(ruta.Split(':')[2].Split('/')[0]);

                isreacheable = await CrossConnectivity.Current.IsRemoteReachable("localhost", port);
            }
            else
                isreacheable = await CrossConnectivity.Current.IsRemoteReachable(ruta);
            if (!isreacheable)
            {
                return new Response
                {
                    Success = false,
                    Message = (ruta == "google.com") ?
                    "No hay conexión a internet." :
                    "No hay acceso a esta ruta. " + ruta
                };
            }
            return new Response { Success = true };
        }
    }
}
