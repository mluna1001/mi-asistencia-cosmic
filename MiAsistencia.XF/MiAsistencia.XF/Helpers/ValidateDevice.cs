﻿using MiAsistencia.XF.LiteConnection.ORM;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MiAsistencia.XF.Helpers
{
    public class ValidateDevice
    {
        public static void Validate(string page)
        {
            if (Device.RuntimePlatform != Device.UWP)
            {
                if (!CheckAutomaticTime.IsAutomaticTime())
                {
                    Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.HoraAutomaticaDeshabilitada, "El dispositivo tiene la hora automática deshabilitada", page);
                    Device.BeginInvokeOnMainThread(new Action(async () =>
                    {
                        if (!await App.Current.MainPage.DisplayAlert("Mi Asistencia", "Tu fecha y hora no están en automático, por favor configura esto en tu dispositivo.", null, "Aceptar"))
                        {
                            new CloseApplication().CloseApp();
                        }
                    }));
                }
                else if (!CheckAutomaticTime.IsAutomaticZone())
                {
                    Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.HoraAutomaticaDeshabilitada, "El dispositivo tiene la zona horaria automática deshabilitada", page);
                    Device.BeginInvokeOnMainThread(new Action(async () =>
                    {
                        if (!await App.Current.MainPage.DisplayAlert("Mi Asistencia", "Tu zona horaria no está en automático, por favor configura esto en tu dispositivo.", null, "Aceptar"))
                        {
                            new CloseApplication().CloseApp();
                        }
                    }));
                }
            }
        }
    }
}
