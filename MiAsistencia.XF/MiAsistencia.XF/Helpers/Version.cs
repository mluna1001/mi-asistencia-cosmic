﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Helpers
{
    public class Version
    {
        private const string _AppVersion = "3.1";
        private const string _InternalVersion = "1.3";

        public string AppVersion
        {
            get { return _AppVersion; }
        }

        public string InternalVersion
        {
            get { return _InternalVersion; }
        }
    }
}
