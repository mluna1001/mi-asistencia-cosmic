﻿using MiAsistencia.XF.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MiAsistencia.XF.Helpers
{
    public class CheckAutomaticTime
    {
        public static bool IsAutomaticTime()
        {
            var automatic = DependencyService.Get<ICheckTime>();
            bool bandera = automatic.IsAutomaticTime();
            return bandera;
        }

        public static bool IsAutomaticZone()
        {
            var automatic = DependencyService.Get<ICheckTime>();
            bool bandera = automatic.IsAutomaticZone();
            return bandera;
        }
    }
}
