﻿using MiAsistencia.XF.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MiAsistencia.XF.Helpers
{
    public class CloseApplication
    {
        public void CloseApp()
        {
            var close = DependencyService.Get<ICloseApplication>();
            close?.CloseApp();
        }
    }
}
