﻿using MiAsistencia.XF.Interfaces;
using MiAsistencia.XF.Models;
using Microsoft.AppCenter.Crashes;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MiAsistencia.XF.Helpers
{
    public class ReadFile
    {
        public RespaldoBDUsuarioDto ReadDataBase()
        {
            var db = new RespaldoBDUsuarioDto();
            try
            {
                var rf = DependencyService.Get<IReadFile>();

                if (rf != null)
                {
                    db = rf.FileRead();
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
            }

            return db;
        }
    }
}
