﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.LiteConnection.ORM
{
    public class TiendaAsistencia
    {
        [PrimaryKey]
        public int IdTienda { get; set; }
        public int IdCadena { get; set; }
        public int IdFormato { get; set; }
        public string NombreTienda { get; set; }
        public string Direccion { get; set; }
        [MaxLength(5)]
        public string Determinante { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public float Radio { get; set; }
        public bool Verificada { get; set; }
    }

    public class TiendaAsistenciaDto
    {
        public int IdAsistencia { get; set; }
        public int IdUsuario { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public bool Estatus { get; set; }
        public int IdTienda { get; set; }
        public string NombreTienda { get; set; }
        public string nombrefoto { get; set; }
        public string Ubicacion { get; set; }
        public string TipoCheck { get; set; }
    }
}
