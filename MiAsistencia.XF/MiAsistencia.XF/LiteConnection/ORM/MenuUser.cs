﻿namespace MiAsistencia.XF.LiteConnection.ORM
{
    using SQLite;

    public class MenuUser
    {
        [PrimaryKey]
        [AutoIncrement]
        public int IdMenu { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string PageName { get; set; }
    }
}
