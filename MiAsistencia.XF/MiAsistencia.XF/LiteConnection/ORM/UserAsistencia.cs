﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.LiteConnection.ORM
{
    public class UserAsistencia
    {
        [PrimaryKey]
        public int IdUsuario { get; set; }
        public string Alias { get; set; }
        public string Nombre { get; set; }
        public byte[] Password { get; set; }
        public int IdProyecto { get; set; }
        public int IdPerfil { get; set; }
        public int IdRegion { get; set; }
        public string AppCenterToken { get; set; }
        public int BorraBase { get; set; }
        public bool LunchTime { get; set; }
    }
}
