﻿namespace MiAsistencia.XF.LiteConnection.ORM
{
    using SQLite;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Recepcion
    {
        [PrimaryKey, AutoIncrement]
        public int IdRecepcion { get; set; }
        public int IdUsuario { get; set; }
        public string NombreRecibe { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public DateTime FechaRecibe { get; set; }
        public byte[] Firma { get; set; }
        public bool Sincronizado { get; set; }
    }
}
