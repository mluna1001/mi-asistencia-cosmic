﻿namespace MiAsistencia.XF.LiteConnection.ORM
{
    using SQLite;
    using System;

    public class Bitacora
    {
        [PrimaryKey, AutoIncrement]
        public int IdBitacora { get; set; }
        public int IdCatBitacora { get; set; }
        public string Descripcion { get; set; }
        public string Pagina { get; set; }
        public int? IdTienda { get; set; }
        public int? IdUsuario { get; set; }
        public DateTime FechaAccion { get; set; }
        public bool Sincronizado { get; set; }

        public static void SaveBitacora(Bitacora bitacora)
        {
            App.DBasistencia.Bitacora.Add(bitacora);
            App.DBasistencia.SaveChanges();
        }

        public static void SaveBitacora(int idCatBitacora, string descripcion, string pagina)
        {
            Bitacora bitacora = new Bitacora
            {
                Descripcion = descripcion,
                IdTienda = App.CurrentIdTienda,
                FechaAccion = DateTime.Now,
                Pagina = pagina,
                IdCatBitacora = idCatBitacora,
                IdUsuario = App.CurrentUserAsistencia?.IdUsuario,
                Sincronizado = false
            };

            App.DBasistencia.Bitacora.Add(bitacora);
            App.DBasistencia.SaveChanges();
        }

        public static void CleanSincronizedBinnacle()
        {
            App.DBasistencia.Execute("DELETE FROM Bitacora WHERE Sincronizado = 1");
            App.DBasistencia.SaveChanges();
        }

        public enum AccionBitacora
        {
            IniciarSesionConCredenciales = 1,
            IniciarSesionRecordado,
            SeleccionDeEntradaSalida,
            RealizoEntrada,
            RealizoSalida,
            NoRealizoentrada,
            NoRealizoSalida,
            IngresoAPantallaDeEntrada,
            IngresoAPantallaDeSalida,
            IntentoRealizarNuevamenteUnaEntrada,
            IntentoRealizarUnaSalidaSinEntrada,
            EntradaSubidaAlServidor,
            SalidaSubidaAlServidor,
            EntradaGuardadaLocalmente,
            SalidaGuardadaLocalmente,
            UbicacionDetectadaDeEntrada,
            UbicacionDetectadaDeSalida,
            CargaManualDeEntradaYSalida,
            HoraAutomaticaDeshabilitada,
            PrimerPreguntaEliminarBD,
            SegundaPreguntaEliminarBD,
            TercerPreguntaEliminarBD,
            IMEIAutenticacion,
            RegistroIMEIDispositivo,
            RegistroAppsIsntaladas,
            CerrarSesionApp,
            CodigoErroneo,
            IntentoRealizarNuevamenteUnaSalida,
            TiendasOfflineSubidasAServidor,
            IniciarSesionLocalConCredenciales,
            SeFirmoRecibido,
            IngresoAPantallaDeSalidaAlmuerzo,
            IngresoAPantallaDeEntradaAlmuerzo,
            IntentoRealizarUnaSalidaAlmuerzoSinEntrada,
            IntentoRealizarUnaEntradaAlmuerzoSinEntrada,
            IntentoRealizarNuevamenteUnaSalidaAlmuerzo,
            IntentoRealizarNuevamenteUnaEntradaAlmuerzo,

        }
    }
}
