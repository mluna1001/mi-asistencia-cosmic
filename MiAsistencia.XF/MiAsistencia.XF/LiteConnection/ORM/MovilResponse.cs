﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.LiteConnection.ORM
{
    public class MovilResponse
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public object Objeto { get; set; }
    }
}
