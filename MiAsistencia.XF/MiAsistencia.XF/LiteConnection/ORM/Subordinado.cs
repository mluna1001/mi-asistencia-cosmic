﻿using SQLite;

namespace MiAsistencia.XF.LiteConnection.ORM
{
    public class Subordinado
    {
        [PrimaryKey]
        public int IdUsuario { get; set; }
        public int IdParent { get; set; }
        public string Alias { get; set; }
        public string Nombre { get; set; }
        public string NoEmpleado { get; set; }
        public bool LunchTime { get; set; }
    }
}
