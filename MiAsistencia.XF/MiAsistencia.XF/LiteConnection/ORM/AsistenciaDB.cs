﻿namespace MiAsistencia.XF.LiteConnection.ORM
{
    using MiAsistencia.XF.Interfaces;
    using MiAsistencia.XF.Services;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Xamarin.Forms;

    public class AsistenciaDB : DataBase
    {
        public AsistenciaDB(string databasePath, bool storeDateTimeAsTicks = true) : base(databasePath, storeDateTimeAsTicks)
        {
            CreateTables();
        }

        private static AsistenciaDB instance;
        public static AsistenciaDB GetInstance()
        {
            if (instance == null)
            {
                var srv = DependencyService.Get<IGetDBService>();
                if (srv == null) throw new NullReferenceException("La dependencia de servicio es null");
                var db = srv.GetDatabase();
                if (db.Status)
                    instance = (AsistenciaDB)db.Objeto;

            }
            return instance;
        }

        public void CreateTables()
        {
            userAsistencia = DBSet<UserAsistencia>();
            menuUser = DBSet<MenuUser>();
            Asistencia = DBSet<Asistencia>();
            TiendaAsistencia = DBSet<TiendaAsistencia>();
            Bitacora = DBSet<Bitacora>();
            Subordinado = DBSet<Subordinado>();
            Recepcion = DBSet<Recepcion>();
        }

        public void DropTables()
        {
            DropTable<UserAsistencia>();
            DropTable<MenuUser>();
            DropTable<Asistencia>();
            DropTable<TiendaAsistencia>();
            DropTable<Subordinado>();
            DropTable<Recepcion>();
        }

        #region Tablas
        public Table<UserAsistencia> userAsistencia { get; set; }
        public Table<MenuUser> menuUser { get; set; }
        public Table<Asistencia> Asistencia { get; set; }
        public Table<TiendaAsistencia> TiendaAsistencia { get; set; }
        public Table<Bitacora> Bitacora { get; set; }
        public Table<Subordinado> Subordinado { get; set; }
        public Table<Recepcion> Recepcion { get; set; }
        #endregion
    }
}
