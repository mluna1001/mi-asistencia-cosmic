﻿using MiAsistencia.XF.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MiAsistencia.XF.Services.API
{
    public class ApiUrl
    {
        public static string tokenApp { get; set; } = "1E9E8237-7819-4931-B73F-04B51F6C280D";
        public static string BaseLogin { get; set; }
        public static string BaseApi { get; set; }
        public static string slogin { get { return $"{BaseLogin}/loginapi/isuser/"; } }

        /// <summary>
        /// $"{BaseUrl}/userapi/loginUser/"
        /// </summary>
        public static string slogin2 { get { return $"{BaseLogin}/userapi/loginUser/"; } }
        //public static string slogin2 { get { return $"{BaseApi}/Home/Get/"; } }
        public static string sUserData { get { return $"{BaseApi}/Usuario/GetObjectUsuario"; } }
        public static string sGetStores { get { return $"{BaseApi}/Tienda/GetStores"; } }
        public static string sGetAllStores { get { return $"{BaseApi}/Tienda/GetAsignedStores"; } }
        public static string sSaveCheck { get { return $"{BaseApi}/Asistencia/SaveCheck"; } }
        public static string sSaveAllChecks { get { return $"{BaseApi}/Asistencia/SaveAllChecks"; } }
        public static string sSaveBinnacle { get { return $"{BaseApi}/Bitacora/SaveBinnacle"; } }
        public static string sDownloadSubordinate { get { return $"{BaseApi}/Subordinado/GetSubordinate"; } }
        public static string sSaveDatabase { get { return $"{BaseApi}/Database/SaveDatabase"; } }
        public static string sReadCheck { get { return $"{BaseApi}/Asistencia/ReadCheck"; } }
        public static string sSaveSignature { get { return $"{BaseApi}/Asistencia/SaveSignature"; } }
        public static string sRegisterUser { get { return $"{BaseApi}/Usuario/Register"; } }
        public static string sExistsUser { get { return $"{BaseApi}/Usuario/ExistsUser"; } }
        public static string sGetCompanies { get { return $"{BaseApi}/Tienda/GetCompanies"; } }

        public static async void SetUrls()
        {
            string urllogin = "http://localhost:28359/";
            //string urlapi = "http://localhost:24105/";
            string urlapi = "http://148.72.152.145/plesk-site-preview/cosmiccheck.com";
            //string urlapi = "http://apistenciastage.condor1772.startdedicated.com";

            BaseLogin = "http://login.cosmic.mx/";
            BaseApi = "http://148.72.152.145/plesk-site-preview/cosmiccheck.com";
            //BaseApi = "http://apistenciastage.condor1772.startdedicated.com";

#if DEBUG
            var isr = await ConnectTools.IsRemoteReachable(urllogin);
            if (Device.RuntimePlatform == Device.UWP && isr.Success)
                BaseLogin = urllogin;

            var isr2 = await ConnectTools.IsRemoteReachable(urlapi);
            if (Device.RuntimePlatform == Device.UWP && isr2.Success)
                BaseApi = urlapi;
#endif
        }

    }
}
