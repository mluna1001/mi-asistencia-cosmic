﻿namespace MiAsistencia.XF.Services.API
{
    using Helpers;
    using MiAsistencia.XF.Common;
    using Microsoft.AppCenter.Crashes;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;

    public class ApiServices
    {
        public static async Task<T> Post<T, K>(string url, K auditoria, Action<string> noconnected)
        {
            var conn = await ConnectTools.CheckConnection(url);
            if (conn.Success)
            {
                try
                {
                    var client = new HttpClient();
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(auditoria);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url)
                    {
                        Content = new StringContent(json, Encoding.UTF8, "application/json")
                    };
                    var response = await client.SendAsync(request);
                    if (response != null && response.IsSuccessStatusCode)
                    {
                        var responsestring = await response.Content.ReadAsStringAsync();
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responsestring);
                    }
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData());
                    noconnected?.Invoke(ex.Message);
                }
            }
            else
            {
                noconnected?.Invoke(conn.Message);
            }
            return default(T);
        }

        public static async Task<T> Post<T>(string url, List<Param> formdata, Action noconnected)
        {
            var conn = await ConnectTools.CheckConnection(url);

            if (conn.Success)
            {
                try
                {
                    var client = new HttpClient();
                    MultipartFormDataContent content = new MultipartFormDataContent();
                    if (formdata != null)
                    {
                        foreach (var item in formdata)
                        {
                            switch (item.Type)
                            {
                                case ParamType.File:
                                    var stream = new MemoryStream(item.Bytes);
                                    content.Add(new StreamContent(stream), item.Name, item.FileName);
                                    break;
                                case ParamType.String:
                                    content.Add(new StringContent(item.Element.ToString()), item.Name);
                                    break;
                                case ParamType.Obj:
                                    var cliente = new HttpClient();
                                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(item.Element);
                                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url)
                                    {
                                        Content = new StringContent(json, Encoding.UTF8, "application/json")
                                    };
                                    content.Add(request.Content, item.Name);
                                    break;
                            }
                        }
                    }
                    var response = await client.PostAsync(url, content);
                    var responsestring = await response.Content.ReadAsStringAsync();
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(responsestring);
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData());
                }
            }
            else
            {
                noconnected?.Invoke();
            }
            return default(T);
        }

        public async Task<TokenResponse> GetToken(string urlBase, string username, string password)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var response = await client.PostAsync("Token",
                    new StringContent(string.Format(
                    "grant_type=password&username={0}&password={1}",
                    username, password),
                    Encoding.UTF8, "application/x-www-form-urlencoded"));
                var resultJSON = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<TokenResponse>(
                    resultJSON);
                return result;
            }
            catch
            {
                return null;
            }
        }
    }

    public class Param
    {
        public object Element { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public ParamType Type { get; set; }

        public byte[] Bytes
        {
            get
            {
                if (Type == ParamType.File)
                {
                    if (Element is byte[])
                    {
                        return Element as byte[];
                    }
                    else if (Element is Stream)
                    {
                        var streamcontent = Element as Stream;
                        return ReadFully(streamcontent);
                    }
                }
                return null;
            }
        }

        public string ContentType { get; set; }

        public Param(string name, object value, ParamType type = ParamType.String, string filename = "file.jpg", string contenttype = "application/json")
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("El parametro NAME es null o esta vacio");
            }
            Element = value ?? throw new Exception("El parametro VALUE es null");
            Name = name;
            FileName = filename;
            Type = type;
            ContentType = contenttype;
        }

        private byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }

    public enum ParamType
    {
        File, String, Obj
    }
}
