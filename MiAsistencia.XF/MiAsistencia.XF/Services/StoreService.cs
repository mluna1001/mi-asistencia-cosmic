﻿using Cosmic;
using MiAsistencia.XF.LiteConnection.ORM;
using MiAsistencia.XF.Services.API;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.XF.Services
{
    public class StoreService
    {
        static private double Distancia(decimal lat1, decimal long1, decimal lat2, decimal long2)
        {
            decimal theta = long1 - long2;
            double distancia = Math.Sin(Radianes((double)lat1)) * Math.Sin(Radianes((double)lat2)) + Math.Cos(Radianes((double)lat1)) * Math.Cos(Radianes((double)lat2)) * Math.Cos(Radianes((double)theta));
            distancia = (Grados(Math.Acos(distancia))) * 60 * 1.1515 * 1.609344;
            return distancia;
        }

        static private double Radianes(double grados)
        {
            return grados * Math.PI / 180;
        }

        static private double Grados(double radianes)
        {
            return radianes * 180 / Math.PI;
        }

        public static List<TiendaAsistencia> GetOfflineStores(decimal latitud, decimal longitud)
        {
            List<TiendaAsistencia> tiendaAsistencias = new List<TiendaAsistencia>();

            var catatienda = App.DBasistencia.TiendaAsistencia.ToList();

            foreach (var item in catatienda)
            {
                double distancia = Distancia(latitud, longitud, item.Latitud, item.Longitud);
                double distanciaMetros = distancia * 1000;
                if (distanciaMetros < 600)
                {
                    tiendaAsistencias.Add(
                        new TiendaAsistencia()
                        {
                            IdTienda = item.IdTienda,
                            NombreTienda = item.NombreTienda,
                            Latitud = item.Latitud,
                            Longitud = item.Longitud
                        }
                    );
                }
            }

            return tiendaAsistencias;
        }

        public static List<TiendaAsistencia> GetOfflineStores()
        {
            return App.DBasistencia.TiendaAsistencia.ToList();
        }

        public static List<TiendaAsistencia> GetTiendasRadio(List<TiendaAsistencia> tiendaAsistencias, decimal latitud, decimal longitud)
        {
            var newTiendas = new List<TiendaAsistencia>();

            foreach (var item in tiendaAsistencias)
            {
                double distancia = Distancia(latitud, longitud, item.Latitud, item.Longitud);
                double distanciaMetros = distancia * 1000;
                if (distanciaMetros < 600)
                {
                    newTiendas.Add(
                        new TiendaAsistencia()
                        {
                            IdTienda = item.IdTienda,
                            NombreTienda = item.NombreTienda,
                            Latitud = item.Latitud,
                            Longitud = item.Longitud
                        }
                    );
                }
            }

            return newTiendas;
        }

        private static async Task<Response> UploadServer(Asistencia asistencia)
        {
            Dictionary<string, string> ora = new Dictionary<string, string>();

            var resptiendas = await ApiServices.Post<Response, Asistencia>(ApiUrl.sSaveCheck, asistencia, null);
            if (resptiendas != null)
            {
                if (resptiendas.Success)
                {
                    asistencia.Sincronizado = true;
                    App.DBasistencia.Entry(asistencia).State = Services.DataBase.EntryState.Modify;
                    App.DBasistencia.SaveChanges();
                }
                return resptiendas;
            }
            return new Response { Success = false };
        }

        public static async void SaveStore(int idusuario)
        {
            App.DBasistencia.Execute("DELETE FROM TiendaAsistencia");
            App.DBasistencia.SaveChanges();

            Common.SendDataStore sendData = new Common.SendDataStore
            {
                Latitud = 0,
                Longitud = 0,
                IdUsuario = App.CurrentUserAsistencia.IdUsuario
            };

            var resptiendas = await ApiServices.Post<Response, Common.SendDataStore>(ApiUrl.sGetAllStores, sendData, null);

            if (resptiendas != null)
            {
                foreach (var item in resptiendas.Data.To<List<Common.Tienda>>())
                {
                    TiendaAsistencia tienda = new TiendaAsistencia
                    {
                        IdTienda = item.IdTienda,
                        NombreTienda = item.NombreTienda,
                        Latitud = item.Latitud,
                        Longitud = item.Longitud
                    };

                    App.DBasistencia.TiendaAsistencia.Add(tienda);
                    App.DBasistencia.SaveChanges();
                }
            }
        }
    }
}
