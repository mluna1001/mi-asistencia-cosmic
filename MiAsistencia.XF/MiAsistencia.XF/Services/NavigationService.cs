﻿namespace MiAsistencia.XF.Services
{
    using LiteConnection.ORM;
    using System;
    using System.Threading.Tasks;
    using ViewModels;
    using Views;
    using Views.Pages;
    using Xamarin.Forms;

    public class NavigationService
    {
        public async void Navigate(string PageName)
        {
            App.Master.IsPresented = false;

            Type page = Type.GetType("MiAsistencia.XF.Views.Pages." + PageName + ",MiAsistencia.XF");
            Page pageinstance = null;

            try
            {
                pageinstance = (Page)Activator.CreateInstance(page);
                
            }
            catch (Exception ex)
            {
                if (PageName.Equals("CloseSessionPage"))
                {
                    var res = await App.Current.MainPage.DisplayAlert("Mi Asistencia", "¿Estás seguro que deseas cerrar sesión en la aplicación?", "No", "Si");

                    if (!res)
                    {
                        Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.CerrarSesionApp, "Ha cerrado sesión en la aplícación", "MasterPage");
                        Helpers.Settings.IsRemembered = "false";
                        Helpers.Settings.Alias = string.Empty;
                        MainViewModel.GetInstance().Login = new LoginViewModel();
                        App.Current.MainPage = new NavigationPage(new StandarLogin());
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
                else if (PageName.Equals("DeleteDatabasePage"))
                {
                    bool answer = await MsgResponse("¿Estás seguro de eliminar la base de datos?", "Si", "No");
                    if (answer)
                    {
                        Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.PrimerPreguntaEliminarBD, $"DBDelete - {(answer ? "Si" : "No")}", "HomePage");

                        bool answer2 = await MsgResponse("Al eliminar la base se elimina toda tu información. ¿Deseas eliminarla?", "No", "Si");

                        if (!answer2)
                        {
                            Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.SegundaPreguntaEliminarBD, $"DBDelete - {(!answer2 ? "Si" : "No")}", "HomePage");

                            // Revisar pendientes al subir asistencias
                            var asistencias = App.DBasistencia.Asistencia.Where(a => !a.Sincronizado).ToList();

                            if (asistencias.Count > 0)
                            {
                                bool answer3 = await MsgResponse("Tienes asistencias sin subir, con esta acción se eliminarán permanentemente. ¿Estás seguro de eliminar la base de datos?", "Si", "No");
                                if (answer3)
                                {
                                    Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.TercerPreguntaEliminarBD, $"DBDelete - {(answer3 ? "Si" : "No")}", "HomePage");
                                }
                            }
                            App.DBasistencia.DropTables();
                            App.DBasistencia.CreateTables();
                            // Se redirige a la pantalla de Login como instancia nueva ALV
                            Helpers.Settings.IsRemembered = "false";
                            Helpers.Settings.Alias = string.Empty;
                            MainViewModel.GetInstance().Login = new LoginViewModel();
                            App.Current.MainPage = new NavigationPage(new StandarLogin());
                        }
                    }
                    return;
                }
                //else if (PageName.Equals("ReceptionPage"))
                //{
                //    Type vmodel = Type.GetType("MiAsistencia.XF.ViewModels." + PageName.Replace("Page", "ViewModel") + ",MiAsistencia.XF");
                //    MainViewModel.GetInstance().Reception = (ReceptionViewModel)Activator.CreateInstance(vmodel);
                //    pageinstance = (Page)Activator.CreateInstance(page);
                //    await App.Navigator.PushAsync(pageinstance);

                //}
                else
                {
                    await App.Current.MainPage.DisplayAlert("Mi Asistencia", "No podemos abrir este menu, contacta a tu supervisor", "Aceptar");
                    return;
                }
            }
            if (!PageName.Equals("MainPage"))
            {
                InstanceViewModel(PageName);
                await App.Navigator.PushAsync(pageinstance);
            }
            else
            {
                App.Current.MainPage = new MasterPage();
            }
        }

        private async void InstanceViewModel(string pageName)
        {
            Type page = Type.GetType("MiAsistencia.XF.ViewModels." + pageName.Replace("Page", "ViewModel") + ",MiAsistencia.XF");
            if (page != null)
            {
                switch (pageName)
                {
                    case "UploadOfflinePage":
                        MainViewModel.GetInstance().UploadOffline = (UploadOfflineViewModel)Activator.CreateInstance(page);
                        break;
                    case "AboutPage":
                        MainViewModel.GetInstance().About = (AboutViewModel)Activator.CreateInstance(page);
                        break;
                    case "DescargaPage":
                        MainViewModel.GetInstance().Descarga = (DescargaViewModel)Activator.CreateInstance(page);
                        break;
                    case "MainCodeCheckPage":
                        MainViewModel.GetInstance().MainCodeCheck = (MainCodeCheckViewModel)Activator.CreateInstance(page);
                        break;
                    case "CloseSessionPage":
                        //var res = await App.Current.MainPage.DisplayAlert("Mi Asistencia", "¿Estás seguro que deseas cerrar sesión en la aplicación?", "No", "Si");
                        var res = await MsgResponse("¿Estás seguro que deseas cerrar sesión en la aplicación?", "No", "Si");
                        if (!res)
                        {
                            Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.CerrarSesionApp, "Ha cerrado sesión en la aplícación", "MasterPage");
                            Helpers.Settings.IsRemembered = "false";
                            Helpers.Settings.Alias = string.Empty;
                            MainViewModel.GetInstance().Login = new LoginViewModel();
                            App.Current.MainPage = new NavigationPage(new StandarLogin());
                        }
                        break;
                    case "DeleteDatabasePage":
                        bool answer = await MsgResponse("¿Estás seguro de eliminar la base de datos?", "Si", "No");
                        if (answer)
                        {
                            Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.PrimerPreguntaEliminarBD, $"DBDelete - {(answer ? "Si" : "No")}", "HomePage");

                            bool answer2 = await MsgResponse("Al eliminar la base se elimina toda tu información. ¿Deseas eliminarla?", "No", "Si");

                            if (!answer2)
                            {
                                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.SegundaPreguntaEliminarBD, $"DBDelete - {(!answer2 ? "Si" : "No")}", "HomePage");

                                // Revisar pendientes al subir asistencias
                                var asistencias = App.DBasistencia.Asistencia.Where(a => !a.Sincronizado).ToList();

                                if (asistencias.Count > 0)
                                {
                                    bool answer3 = await MsgResponse("Tienes asistencias sin subir, con esta acción se eliminarán permanentemente. ¿Estás seguro de eliminar la base de datos?", "Si", "No");
                                    if (answer3)
                                    {
                                        Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.TercerPreguntaEliminarBD, $"DBDelete - {(answer3 ? "Si" : "No")}", "HomePage");
                                    }
                                }
                                App.DBasistencia.DropTables();
                                App.DBasistencia.CreateTables();
                                // Se redirige a la pantalla de Login como instancia nueva ALV
                                Helpers.Settings.IsRemembered = "false";
                                Helpers.Settings.Alias = string.Empty;
                                MainViewModel.GetInstance().Login = new LoginViewModel();
                                App.Current.MainPage = new NavigationPage(new StandarLogin());
                            }
                        }
                        break;
                    case "ReceptionPage":
                        MainViewModel.GetInstance().Reception = (ReceptionViewModel)Activator.CreateInstance(page);
                        break;
                    default:
                        break;
                }
            }
        }

        public async Task<bool> MsgResponse(string arg, string btn1, string btn2)
        {
            return await Application.Current.MainPage.DisplayAlert("Mi Asistencia", arg, btn1, btn2);
        }

        internal void SetMainPage(Page page)
        {
            App.Current.MainPage = page;
        }
    }
}
