﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Common
{
    public class Tienda
    {
        public int IdTienda { get; set; }
        public string NombreTienda { get; set; }
        public string Direccion { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public bool Verificada { get; set; }
    }

    public class SendDataStore
    {
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public int IdUsuario { get; set; }
    }

    public class AsistenciaReal
    {
        public int IdAsistencia { get; set; }
        public int IdUsuario { get; set; }
        public int IdTienda { get; set; }
        public bool Estatus { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime FechaAlta { get; set; }
    }

    public class SendDataDownloadUser
    {
        public int IdUsuario { get; set; }
    }
}
