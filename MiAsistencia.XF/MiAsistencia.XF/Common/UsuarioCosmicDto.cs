﻿namespace MiAsistencia.XF.Common
{
    using MiAsistencia.XF.Models;
    using System;
    using System.Collections.Generic;
    public class UsuarioCosmicDto
    {
        public UsuarioCosmicDto()
        {
            menuDtoL = new List<MenuDto>();
        }

        public usuario usuario { get; set; }
        public List<usuario> usuarioL { get; set; }
        public ProgramaUsuarioDt ProgramaUsuario { get; set; }
        public List<MenuDto> menuDtoL { get; set; }
        public List<ActionsDt> ActionsDt { get; set; }
        public PerfilDto PerfilDto { get; set; }
    }

    public partial class usuario
    {
        public int IdUsuario { get; set; }
        public int IdKey { get; set; }
        public string Nombre { get; set; }
        public int NoEmpleado { get; set; }
        public string Contrasena { get; set; }
        public string Alias { get; set; }
        public string Correo { get; set; }
        public int? IdPerfil { get; set; }
        public int Nivel { get; set; }
        public int? IdUsuarioCosmic { get; set; }
        public int? IdProyecto { get; set; }
        public int? IdRegion { get; set; }
        public bool Bloqueado { get; set; }
        public int? IdParent { get; set; }
        public string UrlFoto { get; set; }
        public int? IdCliente { get; set; }
        public Guid? IdPrograma { get; set; }
        public string Telefono { get; set; }
        public string RFC { get; set; }
        public string IMSS { get; set; }
        public bool? Eliminado { get; set; }
        public List<usuario> Users { get; set; }
        public int? IdUsuarioAlta { get; set; }
        public bool? Dev { get; set; }
    }

    public partial class Subordinado
    {
        public int IdUsuario { get; set; }
        public int IdParent { get; set; }
        public string Alias { get; set; }
        public string Nombre { get; set; }
        public string NoEmpleado { get; set; }
    }

    public partial class ProgramaUsuarioDt
    {
        public int IdProgramaUsuario { get; set; }
        public int IdUsuario { get; set; }
        public Guid IdPrograma { get; set; }
        public Guid? IdProgramaMovil { get; set; }
        public bool? Acceso { get; set; }
    }

    public class MenuDto
    {
        public int IdMenu { get; set; }
        public string URLMenu { get; set; }
        public string Menu { get; set; }
        public Guid? IdPrograma { get; set; }
        public string Icono { get; set; }
        public string Link { get; set; }
        public bool Eliminar { get; set; }
        public int? IdMenuParent { get; set; }
        public int? Orden { get; set; }
        public List<MenuDto> MenusHijos { get; set; }
    }

    public class PerfilDto
    {
        public int IdPerfil { get; set; }
        public string Perfil1 { get; set; }
        public int? Nivel { get; set; }
        public System.Guid IdPrograma { get; set; }
    }

    public class ActionsDt
    {
        public int idAction { get; set; }
        public string controlador { get; set; }
    }

    public class loginUser
    {
        public string usuario { get; set; }
        public string contrasena { get; set; }
        public Guid IdPrograma { get; set; }
    }

    public class UsuarioAlta
    {
        public int IdUsuarioAlta { get; set; }
        public string empleado { get; set; }
        public string paterno { get; set; }
        public string materno { get; set; }
        public string nombre { get; set; }
        public string categoria { get; set; }
        public string puesto { get; set; }
        public string jerarquia { get; set; }
        public string estado { get; set; }
        public string sueldo_integrado { get; set; }
        public string cedula_imss { get; set; }
        public string registro_patronal { get; set; }
        public string cuenta_bancaria { get; set; }
        public string sueldo_diario { get; set; }
        public string cuenta_individual { get; set; }
        public string rfc { get; set; }
        public string calle { get; set; }
        public string numero_calle { get; set; }
        public string colonia { get; set; }
        public string codigo_postal { get; set; }
        public string ciudad { get; set; }
        public string telefono_propio { get; set; }
        public string telefono_recado { get; set; }
        public string razon_social { get; set; }
        public string desc_nomina { get; set; }
        public string fecha_alta { get; set; }
        public string fecha_antiguedad { get; set; }
        public string fecha_reingreso { get; set; }
        public string fecha_baja { get; set; }
        public string plaza { get; set; }
        public string costos_n { get; set; }
        public string costos_d { get; set; }
        public string costos_s { get; set; }
        public string region_n { get; set; }
        public string region_d { get; set; }
        public string distribucion_n { get; set; }
        public string distribucion_d { get; set; }
        public string cadena_d { get; set; }
        public string fecha_nacimiento { get; set; }
        public string Fecha_baja_real { get; set; }
        public string Nombre_Ultimo_Jefe { get; set; }
        public string Fecha_alta_real { get; set; }
        public string Aquien_cubre { get; set; }
        public string estado_civil { get; set; }
        public string estado_civil_cia { get; set; }
        public string Folio_Aquien_cubre { get; set; }
        public string Grado_Estudio { get; set; }
        public string sexo { get; set; }
        public string sexo_cia { get; set; }
        public string Producto { get; set; }
        public string Promocion { get; set; }
        public string factor_integracion { get; set; }
        public string Dias_a_laborar { get; set; }
        public string Email { get; set; }
        public string Clabe { get; set; }
        public string credito_infonavit { get; set; }
        public string tipo_descuento { get; set; }
        public string factor_descuento { get; set; }
        public string originario { get; set; }
        public string situacion_familiar { get; set; }
        public string numero_de_hijos { get; set; }
        public string Banco { get; set; }
        public string zona { get; set; }
        public string desc_tipo_plaza { get; set; }
        public Nullable<decimal> costos_ndec { get; set; }
        public Nullable<bool> Eliminado { get; set; }
        public List<Programas> Programas { get; set; }
    }
}
