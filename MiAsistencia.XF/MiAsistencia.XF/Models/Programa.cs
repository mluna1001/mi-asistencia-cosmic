﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Models
{
    public class Programas
    {
        public Guid IdPrograma { get; set; }
        public string Programa { get; set; }
    }
}
