﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Models
{
    public class RazonSocial
    {
        public int IdRazonSocial { get; set; }
        public string Descripcion { get; set; }
        public bool Vigencia { get; set; }
    }
}
