﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Models
{
    public class RespaldoBDUsuarioDto
    {
        public int IdUsuario { get; set; }
        public byte[] BasedeDatos { get; set; }
        public string Nombre { get; set; }
        public string RazonRespaldo { get; set; }
    }
}
