﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Models
{
    public class Item
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }
}
