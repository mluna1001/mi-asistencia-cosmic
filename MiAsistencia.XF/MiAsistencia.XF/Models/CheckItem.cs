﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Models
{
    public class CheckItem
    {
        public int ItemId { get; set; }
        public string ItemType { get; set; }
        public string Icon { get; set; }
        public int? TypeCheckId { get; set; }
    }
}
