﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Models
{
    public class TypeCheck
    {
        public enum TipoAsistencia
        {
            Entrada = 1,
            SalidaAlmuerzo = 2,
            EntradaAlmuerzo = 3,
            Salida = 4
        }
    }
}
