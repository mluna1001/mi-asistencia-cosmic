﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Interfaces
{
    public interface IDevice
    {
        string GetIdentifier();

        List<Tuple<string, string>> GetInstalledApps();
    }
}
