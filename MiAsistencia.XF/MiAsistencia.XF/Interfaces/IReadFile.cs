﻿namespace MiAsistencia.XF.Interfaces
{
    using Models;

    public interface IReadFile
    {
        RespaldoBDUsuarioDto FileRead();
    }
}
