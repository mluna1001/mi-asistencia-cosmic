﻿using MiAsistencia.XF.LiteConnection.ORM;
using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Interfaces
{
    public interface IGeocerca
    {
        void IniciaGeofences();
        void AddGeofences(List<TiendaAsistencia> tiendaAsistencias, EnergyConfig energyConfig);
        void RemoveGeofences();
        //void GetInstance();
    }
    public enum EnergyConfig
    {
        Low = 0,
        Medium = 1,
        Accurate = 2
    }
}
