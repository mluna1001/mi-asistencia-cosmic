﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Interfaces
{
    public interface ICheckTime
    {
        bool IsAutomaticTime();
        bool IsAutomaticZone();
    }
}
