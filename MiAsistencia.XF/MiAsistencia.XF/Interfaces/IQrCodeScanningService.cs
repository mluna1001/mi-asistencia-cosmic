﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.XF.Interfaces
{
    public interface IQrCodeScanningService
    {
        Task<string> ScanAsync();
    }
}
