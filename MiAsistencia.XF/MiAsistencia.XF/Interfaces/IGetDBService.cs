﻿using MiAsistencia.XF.LiteConnection.ORM;
using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Interfaces
{
    public interface IGetDBService
    {
        MovilResponse GetDatabase();
    }
}
