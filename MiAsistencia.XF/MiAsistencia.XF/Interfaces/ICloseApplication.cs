﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MiAsistencia.XF.Interfaces
{
    public interface ICloseApplication
    {
        void CloseApp();
    }
}
