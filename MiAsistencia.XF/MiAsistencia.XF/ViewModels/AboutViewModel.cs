﻿using Cosmic;
using GalaSoft.MvvmLight.Command;
using MiAsistencia.XF.Helpers;
using MiAsistencia.XF.LiteConnection.ORM;
using MiAsistencia.XF.Models;
using MiAsistencia.XF.Services.API;
using Plugin.Messaging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MiAsistencia.XF.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        #region Properties
        public string Version
        {
            get { return "Versión: " + new Helpers.Version().AppVersion; }
        }

        public string Internal
        {
            get { return "Versión interna: " + new Helpers.Version().InternalVersion; }
        }

        public string Usuario
        {
            get { return App.CurrentUserAsistencia.Alias; }
        }

        public string NombreUsuario
        {
            get { return App.CurrentUserAsistencia.Nombre; }
        }
        #endregion

        public AboutViewModel()
        {
            //MsgResponse("Ingresó a pantalla de Acerca de");
        }

        public async void MsgResponse(string arg)
        {
            await Application.Current.MainPage.DisplayAlert("Mi Asistencia", arg, "Aceptar");
        }

        #region Singleton
        private static AboutViewModel instance;
        public static AboutViewModel GetInstance()
        {
            if (instance == null)
            {
                return new AboutViewModel();
            }
            return instance;
        }
        #endregion

        #region Commands

        public ICommand CallMesaControlCommand
        {
            get { return new RelayCommand(Call); }
        }

        public void Call()
        {
            var phone = CrossMessaging.Current.PhoneDialer;
            if (phone.CanMakePhoneCall)
            {
                phone.MakePhoneCall("8002889353");
            }
        }

        public ICommand SaveDatabaseCommand
        {
            get { return new RelayCommand(SaveDatabase); }
        }

        public async void SaveDatabase()
        {
            App.DBasistencia.Close();
            var basedatos = new ReadFile();
            RespaldoBDUsuarioDto baseDatos = new RespaldoBDUsuarioDto
            {
                IdUsuario = App.CurrentUserAsistencia.IdUsuario,
                RazonRespaldo = "Solicitud de guardado"
            };

            var db = basedatos.ReadDataBase();

            baseDatos.BasedeDatos = db.BasedeDatos;
            baseDatos.Nombre = db.Nombre;

            var respuesta = await RespaldaBDServidor(baseDatos);

            if (respuesta.Success)
            {
                MsgResponse("La base de datos ha sido cargada al servidor correctamente.");
            }
            else
            {
                MsgResponse(respuesta.Message);
            }
            App.DBasistencia = AsistenciaDB.GetInstance();
        }

        public async Task<Response> RespaldaBDServidor(RespaldoBDUsuarioDto respaldoBDUsuarioDto)
        {
            bool noconnected = true;

            byte[] file = respaldoBDUsuarioDto.BasedeDatos;
            respaldoBDUsuarioDto.BasedeDatos = null;

            List<Param> parameters = new List<Param>
                {
                    new Param("baseDatosStr", respaldoBDUsuarioDto, ParamType.Obj),
                    new Param("file", file, ParamType.File)
                };

            var response = await ApiServices.Post<Response>(ApiUrl.sSaveDatabase, parameters, ()=> noconnected = false);
            
            if (response != null)
            {
                if (response.Success)
                {
                    return response;
                }
                else
                    return new Response { Success = false };
            }
            else
                return new Response { Success = false };
        }

        #endregion
    }
}
