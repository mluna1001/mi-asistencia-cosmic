﻿namespace MiAsistencia.XF.ViewModels
{
    using Cosmic;
    using GalaSoft.MvvmLight.Command;
    using MiAsistencia.XF.Common;
    using MiAsistencia.XF.Helpers;
    using MiAsistencia.XF.Interfaces;
    using MiAsistencia.XF.LiteConnection.ORM;
    using MiAsistencia.XF.Models;
    using MiAsistencia.XF.Services;
    using MiAsistencia.XF.Services.API;
    using Microsoft.AppCenter.Crashes;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class CodeCheckViewModel : BaseViewModel
    {
        #region Attributes
        bool _IsVisibleUbication;
        bool _IsAIRunning;
        bool _IsAIVisible;
        bool _IsDisabled;
        bool _IsVisibleSaveButton;
        bool _IsVisibleUserData;
        string _TitlePage;
        string _StatusUbication;
        string message = string.Empty;
        decimal _Latitud = 0;
        decimal _Longitud = 0;
        string _NoEmpleado;
        string _Nombre;
        string _HoEntrada;
        string _HoSalida;
        string _TiendaNombre;
        CheckItemViewModel _Item;
        Tienda _StoreSelected;
        ObservableCollection<Tienda> _ListaTiendas;
        List<Asistencia> _ListAsistencias;
        #endregion

        #region Properties
        public int IdUsuarioCheck { get; set; }

        public bool IsVisibleUbication
        {
            get { return _IsVisibleUbication; }
            set { SetProperty(ref _IsVisibleUbication, value); }
        }

        public bool IsAIRunning
        {
            get { return _IsAIRunning; }
            set { SetProperty(ref _IsAIRunning, value); }
        }

        public bool IsAIVisible
        {
            get { return _IsAIVisible; }
            set { SetProperty(ref _IsAIVisible, value); }
        }

        public bool IsDisabled
        {
            get { return true; }
            set { SetProperty(ref _IsDisabled, value); }
        }

        public bool IsVisibleSaveButton
        {
            get { return _IsVisibleSaveButton; }
            set { SetProperty(ref _IsVisibleSaveButton, value); }
        }

        public bool IsStorePickerEnabled { get; set; }

        public string TitlePage
        {
            get { return _TitlePage; }
            set { SetProperty(ref _TitlePage, value); }
        }

        public string StatusUbication
        {
            get { return _StatusUbication; }
            set { SetProperty(ref _StatusUbication, value); }
        }

        public string TiendaNombre
        {
            get { return _TiendaNombre; }
            set { SetProperty(ref _TiendaNombre, value); }
        }

        public ObservableCollection<Tienda> ListaTiendas
        {
            get { return _ListaTiendas; }
            set { SetProperty(ref _ListaTiendas, value); }
        }

        public Tienda StoreSelected
        {
            get { return _StoreSelected; }
            set { SetProperty(ref _StoreSelected, value); }
        }

        public string NoEmpleado
        {
            get { return _NoEmpleado; }
            set { SetProperty(ref _NoEmpleado, value); }
        }

        public string Nombre
        {
            get { return _Nombre; }
            set { SetProperty(ref _Nombre, value); }
        }

        public string HoEntrada
        {
            get { return _HoEntrada; }
            set { SetProperty(ref _HoEntrada, value); }
        }

        public string HoSalida
        {
            get { return _HoSalida; }
            set { SetProperty(ref _HoSalida, value); }
        }

        public List<Asistencia> ListAsistencias
        {
            get { return _ListAsistencias; }
            set { SetProperty(ref _ListAsistencias, value); }
        }

        #endregion

        #region Constructors
        public CodeCheckViewModel(CheckItemViewModel item)
        {
            _ListAsistencias = new List<Asistencia>();
            ValidateDevice.Validate("CodeCheckPage");
            try
            {
                this._Item = item;
                this.TitlePage = item.ItemType;
                this.StatusUbication = "Estamos buscando tu ubicación...";
                this.IsAIRunning = true;
                this.IsAIVisible = true;
                this.IsVisibleSaveButton = false;
                if (_Item.ItemId == 1)
                    Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IngresoAPantallaDeEntrada, "Ingresó a pantalla de entrada vía lector", "CheckPage");
                else if (_Item.ItemId == 2)
                    Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IngresoAPantallaDeEntrada, "Ingresó a pantalla de salida a almuerzo vía lector", "CheckPage");
                else if (_Item.ItemId == 3)
                    Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IngresoAPantallaDeEntrada, "Ingresó a pantalla de entrada a almuerzo vía lector", "CheckPage");
                else
                    Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IngresoAPantallaDeSalida, "Ingresó a pantalla de salida vía lector", "CheckPage");

                GetPosition();
                UploadBinnacle();
                //MsgResponse("Ingresó a pantalla de Check con QR para realizar " + item.ItemType);
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
            }
        }
        #endregion

        #region Methods
        private async Task UploadBinnacle()

        {
            var bitacoras = App.DBasistencia.Bitacora.Where(x => !x.Sincronizado).Take(10).ToList();

            var respBitacora = await ApiServices.Post<Response, List<Bitacora>>(ApiUrl.sSaveBinnacle, bitacoras, null);
            if (respBitacora != null)
            {
                if (respBitacora.Success)
                {
                    foreach (var item in bitacoras)
                    {
                        item.Sincronizado = true;
                        App.DBasistencia.Entry(item).State = Services.DataBase.EntryState.Modify;
                        App.DBasistencia.SaveChanges();
                    }
                }
            }
        }

        private async void GetPosition()
        {
            if (GeoTools.IsLocationAvailable())
            {
                try
                {
                    var position = await GeoTools.GetPositionAsync();
                    if (position != null)
                    {
                        _Latitud = (decimal)position.Latitude;
                        _Longitud = (decimal)position.Longitude;
                        GetStores();
                    }
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData());
                    MsgResponse("En estos momentos no podemos obtener información, verifica tu conexión a internet");
                    //await App.Current.MainPage.Navigation.PopAsync();
                }
            }
        }

        public async void MsgResponse(string arg)
        {
            await Application.Current.MainPage.DisplayAlert("Mi Asistencia", arg, "Aceptar");
        }

        private async void GetStores()
        {
            this.StatusUbication = "¡Ya encontramos tu ubicación!";
            //var listTiendas = StoreService.GetOfflineStores(_Latitud, _Longitud);
            var listTiendas = StoreService.GetOfflineStores();
            ListaTiendas = listTiendas.To<ObservableCollection<Tienda>>();
            this.IsAIRunning = false;
            this.IsAIVisible = false;
            this.IsVisibleUbication = true;
            try
            {
                this.StoreSelected = ListaTiendas[0];
            }
            catch (Exception ex)
            {
                MsgResponse("No hay tiendas disponibles. Por favor descargue catálogos nuevamente. Si aún no se muestra ninguna, contacte a su supervisor.");
            }

            if (ListaTiendas.Count <= 1)
                this.IsStorePickerEnabled = false; 
        }

        private static async Task<Response> UploadServer(Asistencia asistencia)
        {
            Dictionary<string, string> ora = new Dictionary<string, string>();

            var resptiendas = await ApiServices.Post<Response, Asistencia>(ApiUrl.sSaveCheck, asistencia, null);
            if (resptiendas != null)
            {
                if (resptiendas.Success)
                {
                    asistencia.Sincronizado = true;
                    App.DBasistencia.Entry(asistencia).State = Services.DataBase.EntryState.Modify;
                    App.DBasistencia.SaveChanges();
                }
                return resptiendas;
            }
            return new Response { Success = false };
        }

        private void SaveStore()
        {
            if (!App.DBasistencia.TiendaAsistencia.Exists(x => x.IdTienda == this.StoreSelected.IdTienda))
            {
                TiendaAsistencia tienda = new TiendaAsistencia
                {
                    IdTienda = this.StoreSelected.IdTienda,
                    NombreTienda = this.StoreSelected.NombreTienda,
                    Latitud = this.StoreSelected.Latitud,
                    Longitud = this.StoreSelected.Longitud
                };

                App.DBasistencia.TiendaAsistencia.Add(tienda);
                App.DBasistencia.SaveChanges();
            }
        }

        public async void GetAsistencias(int IdUsuario)
        {
            this.StatusUbication = "Leyendo datos del colaborador...";
            this.IsAIRunning = true;
            this.IsAIVisible = true;
            this.IsVisibleUbication = false;

            var asistencia = new Asistencia { IdUsuario = IdUsuario };

            var respAsistencia = await ApiServices.Post<Response, Asistencia>(ApiUrl.sReadCheck, asistencia, null);

            if (respAsistencia != null)
            {
                if (respAsistencia.Success)
                {
                    _ListAsistencias = new List<Asistencia>();
                    this.ListAsistencias = respAsistencia.Data.To<List<Asistencia>>();

                    if (this.ListAsistencias.Count > 0)
                    {
                        SaveAsistencia(this.ListAsistencias);
                    }

                    this.IsAIRunning = false;
                    this.IsAIVisible = false;
                    this.IsVisibleUbication = true;
                    this.IsVisibleSaveButton = true;
                    this.HoEntrada = (ListAsistencias.Count > 0 && ListAsistencias != null) ? ListAsistencias[0].Fecha.ToString() : "Pendiente";
                    this.HoSalida = (ListAsistencias.Count > 0 && ListAsistencias != null) ? ListAsistencias.Count > 1 ? ListAsistencias[1].Fecha.ToString() : "Pendiente" : "Pendiente";
                    this.TiendaNombre = (ListAsistencias.Count > 0 && ListAsistencias != null) ? ListAsistencias[0].Tienda : this.ListaTiendas[0].NombreTienda;

                    
                }
                else
                {
                    _ListAsistencias = new List<Asistencia>();
                }
            }
            else
            {
                var fechaActual = DateTime.Now.Date;

                _ListAsistencias = (from a in App.DBasistencia.Asistencia
                                   where a.IdUsuario == IdUsuario
                                    && a.Fecha > fechaActual
                                   select a).ToList();

                this.IsAIRunning = false;
                this.IsAIVisible = false;
                this.IsVisibleUbication = true;
                this.IsVisibleSaveButton = true;

                this.HoEntrada = (ListAsistencias.Count > 0 && ListAsistencias != null) ? ListAsistencias[0].Fecha.ToString() : "Pendiente";
                this.HoSalida = (ListAsistencias.Count > 0 && ListAsistencias != null) ? ListAsistencias.Count > 1 ? ListAsistencias[1].Fecha.ToString() : "Pendiente" : "Pendiente";
            }
            //return res;
            this.StatusUbication = "¡Datos obtenidos!";
        }

        private async void SaveAsistencia(List<Asistencia> listAsistencias)
        {
            var actual = DateTime.Now.Date;

            if (!App.DBasistencia.Asistencia.Any(a => a.IdUsuario == ListAsistencias[0].IdUsuario && a.Fecha >= actual))
            {
                foreach (var item in ListAsistencias)
                {
                    App.DBasistencia.Add(item);
                    App.DBasistencia.SaveChanges();
                }
            }
        }
        #endregion

        #region Commands
        public ICommand SaveCommand
        {
            get { return new RelayCommand(Save); }
        }

        public async void Save()
        {
            try
            {
                this.IsAIRunning = true;
                this.IsAIVisible = true;
                this.IsVisibleUbication = false;
                ValidateDevice.Validate("CodeCheckPage");

                App.CurrentIdTienda = StoreSelected.IdTienda;

                if (StoreSelected != null)
                {
                    Asistencia asistencia = new Asistencia
                    {
                        IdUsuario = this.IdUsuarioCheck,
                        IdTienda = StoreSelected.IdTienda,
                        Latitud = _Latitud,
                        Longitud = _Longitud,
                        Estatus = (_Item.ItemId == (int)TypeCheck.TipoAsistencia.Entrada || _Item.ItemId == (int)TypeCheck.TipoAsistencia.EntradaAlmuerzo)? true : false,
                        Fecha = DateTime.Now,
                        Sincronizado = false
                    };

                    var date = asistencia.Fecha.Date;

                    if (_Item.TypeCheckId == 2 || _Item.TypeCheckId == 3) // Este bloque en if es para registrar tiempos de almuerzo
                    {
                        if (!asistencia.Estatus)
                        {
                            var entrada = App.DBasistencia.Asistencia.FirstOrDefault
                                (et => et.IdUsuario == asistencia.IdUsuario && et.IdTienda == asistencia.IdTienda
                                && et.Fecha >= date);

                            if (entrada == null)
                            {
                                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IntentoRealizarUnaSalidaAlmuerzoSinEntrada, "Salida a almuerzo sin entrada previa", "CodeCheckPage");
                                message = $"El usuario { this.NoEmpleado } debe realizar una entrada vía lector primero para realizar una salida a almuerzo en \"{ StoreSelected.NombreTienda }\"";
                                MsgResponse(message);
                                this.IsAIRunning = false;
                                this.IsAIVisible = false;
                                this.IsVisibleUbication = true;
                                return;
                            }

                            var salidaS = App.DBasistencia.Asistencia.FirstOrDefault
                                (et => et.IdUsuario == asistencia.IdUsuario && et.IdTienda == asistencia.IdTienda
                                && et.Fecha >= date && et.Estatus == false && et.IdTipoAsistencia == (int)TypeCheck.TipoAsistencia.SalidaAlmuerzo);

                            if (salidaS != null)
                            {
                                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IntentoRealizarNuevamenteUnaSalidaAlmuerzo, "Intentó realizar salida a almuerzo nuevamente", "CodeCheckPage");
                                message = $"El usuario { this.NoEmpleado } no puede realizar una salida a almuerzo vía lector para \"{ StoreSelected.NombreTienda }\" debido a que ya ha realizado una previa en este día.";
                                MsgResponse(message);
                                this.IsAIRunning = false;
                                this.IsAIVisible = false;
                                this.IsVisibleUbication = true;
                                return;
                            }

                            message = "Se ha realizado una salida a almuerzo vía lector para el usuario: " + this.NoEmpleado + " en: ";
                            asistencia.IdTipoAsistencia = (int)TypeCheck.TipoAsistencia.SalidaAlmuerzo;
                        }
                        else
                        {
                            var entrada = App.DBasistencia.Asistencia.FirstOrDefault
                                (et => et.IdUsuario == asistencia.IdUsuario && et.IdTienda == asistencia.IdTienda
                                && et.Fecha >= date);

                            if (entrada != null)
                            {
                                var entradaA = App.DBasistencia.Asistencia.FirstOrDefault
                                (et => et.IdUsuario == asistencia.IdUsuario && et.IdTienda == asistencia.IdTienda
                                && et.Fecha >= date && et.IdTipoAsistencia == (int)TypeCheck.TipoAsistencia.EntradaAlmuerzo);

                                if (entradaA != null)
                                {
                                    Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IntentoRealizarNuevamenteUnaEntradaAlmuerzo, "Intentó entrada de regreso de almuerzo vía lector en la misma tienda", "CheckPage");
                                    message = $"El usuario { this.NoEmpleado } no puede realizar una entrada de regreso de almuerzo para \"{ StoreSelected.NombreTienda }\" debido a que ya ha realizado una previa en este día.";
                                    MsgResponse(message);
                                    this.IsAIRunning = false;
                                    this.IsAIVisible = false;
                                    this.IsVisibleUbication = true;
                                    return;
                                }

                                message = "Se ha realizado una entrada de regreso de almuerzo vía lector para el usuario: " + this.NoEmpleado + " en: ";
                                asistencia.IdTipoAsistencia = (int)TypeCheck.TipoAsistencia.EntradaAlmuerzo; 
                            }
                            else
                            {
                                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IntentoRealizarUnaEntradaAlmuerzoSinEntrada, "Entrada de regreso de almuerzo sin entrada previa", "CodeCheckPage");
                                message = $"El usuario { this.NoEmpleado } debe realizar una entrada vía lector primero para realizar una entrada de regreso de almuerzo en \"{ StoreSelected.NombreTienda }\"";
                                MsgResponse(message);
                                this.IsAIRunning = false;
                                this.IsAIVisible = false;
                                this.IsVisibleUbication = true;
                                return;
                            }
                        }
                    }
                    else // Este bloque en else es para registrar entrada y salida en toda la tienda
                    { 
                        if (!asistencia.Estatus)
                        {
                            var entrada = App.DBasistencia.Asistencia.FirstOrDefault
                                (et => et.IdUsuario == asistencia.IdUsuario && et.IdTienda == asistencia.IdTienda
                                && et.Fecha >= date && et.IdTipoAsistencia == (int)TypeCheck.TipoAsistencia.Entrada);

                            if (entrada == null)
                            {
                                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IntentoRealizarUnaSalidaSinEntrada, "Salida sin entrada previa", "CheckPage");
                                message = $"El usuario { this.NoEmpleado } debe realizar una entrada vía lector primero para realizar una salida en \"{ StoreSelected.NombreTienda }\"";
                                MsgResponse(message);
                                this.IsAIRunning = false;
                                this.IsAIVisible = false;
                                this.IsVisibleUbication = true;
                                return;
                            }

                            var salida = App.DBasistencia.Asistencia.FirstOrDefault
                                (et => et.IdUsuario == asistencia.IdUsuario && et.IdTienda == asistencia.IdTienda
                                && et.Fecha >= date && et.Estatus == false && et.IdTipoAsistencia == (int)TypeCheck.TipoAsistencia.Salida);

                            if (salida != null)
                            {
                                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IntentoRealizarNuevamenteUnaSalida, "Ingresó a pantalla de entrada", "CheckPage");
                                message = $"El usuario { this.NoEmpleado } no puede realizar una salida vía lector para \"{ StoreSelected.NombreTienda }\" debido a que ya ha realizado una previa en este día.";
                                MsgResponse(message);
                                this.IsAIRunning = false;
                                this.IsAIVisible = false;
                                this.IsVisibleUbication = true;
                                return;
                            }

                            message = "Se ha realizado una salida vía lector para el usuario: " + this.NoEmpleado + " en: ";
                            asistencia.IdTipoAsistencia = (int)TypeCheck.TipoAsistencia.Salida;
                        }
                        else
                        {
                            var entrada = App.DBasistencia.Asistencia.FirstOrDefault
                                (et => et.IdUsuario == asistencia.IdUsuario && et.IdTienda == asistencia.IdTienda
                                && et.Fecha >= date && et.IdTipoAsistencia == (int)TypeCheck.TipoAsistencia.Entrada);

                            if (entrada != null)
                            {
                                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IntentoRealizarNuevamenteUnaEntrada, "Entrada vía lector en la misma tienda", "CheckPage");
                                message = $"El usuario { this.NoEmpleado } no puede realizar una entrada para \"{ StoreSelected.NombreTienda }\" debido a que ya ha realizado una previa en este día.";
                                MsgResponse(message);
                                this.IsAIRunning = false;
                                this.IsAIVisible = false;
                                this.IsVisibleUbication = true;
                                return;
                            }

                            message = "Se ha realizado una entrada vía lector para el usuario: " + this.NoEmpleado + " en: ";
                            asistencia.IdTipoAsistencia = (int)TypeCheck.TipoAsistencia.Entrada;
                        }
                    }
                    App.DBasistencia.Asistencia.Add(asistencia);
                    App.DBasistencia.SaveChanges();

                    SaveStore();

                    var upload = await UploadServer(asistencia);

                    if (upload.Success)
                    {
                        var asis = upload.Data.To<AsistenciaReal>();
                        message = message + $"\"{ StoreSelected.NombreTienda }\" con fecha { asistencia.Fecha }. ";
                        message = message + "La información se ha subido al servidor.";
                        var tipoCheck = asistencia.Estatus ? (int)Bitacora.AccionBitacora.EntradaSubidaAlServidor : (int)Bitacora.AccionBitacora.SalidaSubidaAlServidor;
                        Bitacora.SaveBitacora(tipoCheck, $"{ (asistencia.Estatus ? "Entrada" : "Salida") } guardada al servidor para {StoreSelected.IdTienda} - {StoreSelected.NombreTienda}", "CheckPage");
                    }
                    else
                    {
                        message = message + $"\"{ StoreSelected.NombreTienda }\" con fecha { asistencia.Fecha }. ";
                        message = message + "La información se guardó en el dispositivo. Más adelante puede subirla manualmente al servidor.";
                        var tipoCheck = asistencia.Estatus ? (int)Bitacora.AccionBitacora.EntradaGuardadaLocalmente : (int)Bitacora.AccionBitacora.SalidaGuardadaLocalmente;
                        Bitacora.SaveBitacora(tipoCheck, $"{ (asistencia.Estatus ? "Entrada" : "Salida") } guardada localmente para {StoreSelected.IdTienda} - {StoreSelected.NombreTienda}", "CheckPage");
                    }

                    App.CurrentIdTienda = null;
                    MsgResponse(message);
                    await App.Navigator.PopAsync();
                }
                else
                {
                    MsgResponse("Por favor, seleccione una tienda");
                    this.IsAIRunning = false;
                    this.IsAIVisible = false;
                    this.IsVisibleUbication = true;
                }
            }
            catch (Exception ex)
            {
                this.IsDisabled = true;
                //MsgResponse(ex.Message);
                MsgResponse("En estos momentos no es posible subir la asistencia, intente más tarde");
                Crashes.TrackError(ex, App.TrackData());
            }
        }

        public ICommand ReadCodeCommand
        {
            get { return new RelayCommand(ReadCode); }
        }

        public async void ReadCode()
        {
            var FechaActual = DateTime.Now;

            if (Device.RuntimePlatform != Device.UWP)
            {
                try
                {
                    var scanner = DependencyService.Get<IQrCodeScanningService>();
                    var result = await scanner.ScanAsync();
                    if (result != null)
                    {
                        string codigo = result;
                        var p = App.DBasistencia.Subordinado.Where(s => s.NoEmpleado == result).GetFirst();
                        if (p != null)
                        {
                            this.IdUsuarioCheck = p.IdUsuario;
                            GetAsistencias(this.IdUsuarioCheck);

                            this.NoEmpleado = p.NoEmpleado;
                            this.Nombre = p.Nombre;
                            //this.HoEntrada = (ListAsistencias.Count > 0 && ListAsistencias != null) ? ListAsistencias[0].Fecha.ToString() : "Pendiente";
                            //this.HoSalida = (ListAsistencias.Count > 0 && ListAsistencias != null) ? ListAsistencias.Count > 1 ? ListAsistencias[1].Fecha.ToString() : "Pendiente" : "Pendiente";

                            if (!p.LunchTime && (_Item.TypeCheckId == (int)TypeCheck.TipoAsistencia.SalidaAlmuerzo || _Item.TypeCheckId == (int)TypeCheck.TipoAsistencia.EntradaAlmuerzo))
                            {
                                MsgResponse("Este usuario no registra entrada/salida a almuerzo.");
                                this.IsVisibleSaveButton = false;
                            }
                        }
                        else
                        {
                            Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.CodigoErroneo, "Código erroneo para lector", "CheckPage");
                            MsgResponse("Código no reconocido, intente nuevamente.");
                            this.IsVisibleSaveButton = false;
                            this.StatusUbication = "Leyendo datos del colaborador...";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData());
                    MsgResponse("Por favor intente más tarde.");
                }
            }
            else
            {
                string result = "10002022";

                var p = App.DBasistencia.Subordinado.Where(s => s.NoEmpleado.Equals(result)).GetFirst();
                if (p != null)
                {
                    this.IdUsuarioCheck = p.IdUsuario;
                    GetAsistencias(this.IdUsuarioCheck);
                    //this.IsVisibleSaveButton = true;
                    this.NoEmpleado = p.NoEmpleado;
                    this.Nombre = p.Nombre;
                    //this.HoEntrada = (ListAsistencias.Count > 0 && ListAsistencias != null) ? ListAsistencias[0].Fecha.ToString() : "Pendiente";
                    //this.HoSalida = (ListAsistencias.Count > 0 && ListAsistencias != null) ? ListAsistencias.Count > 1 ? ListAsistencias[1].Fecha.ToString() : "Pendiente" : "Pendiente";
                    if (!p.LunchTime && (_Item.TypeCheckId == (int)TypeCheck.TipoAsistencia.SalidaAlmuerzo || _Item.TypeCheckId == (int)TypeCheck.TipoAsistencia.EntradaAlmuerzo))
                    {
                        MsgResponse("Este usuario no registra entrada/salida a almuerzo.");
                        this.IsVisibleSaveButton = false;
                    }
                }
            }
        }
        #endregion
    }
}
