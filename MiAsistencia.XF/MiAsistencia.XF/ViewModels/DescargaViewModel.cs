﻿namespace MiAsistencia.XF.ViewModels
{
    using Cosmic;
    using GalaSoft.MvvmLight.Command;
    using MiAsistencia.XF.Common;
    using MiAsistencia.XF.Helpers;
    using MiAsistencia.XF.Services;
    using MiAsistencia.XF.Services.API;
    using Microsoft.AppCenter.Crashes;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class DescargaViewModel : BaseViewModel
    {
        #region Attributes
        bool _IsVisibleAI;
        bool _IsVisibleButtonList;
        bool _IsRunning;
        ObservableCollection<LiteConnection.ORM.Subordinado> _UsersList = new ObservableCollection<LiteConnection.ORM.Subordinado>();
        #endregion

        #region Properties
        public ObservableCollection<LiteConnection.ORM.Subordinado> UsersList
        {
            get { return _UsersList; }
            set { SetProperty(ref _UsersList, value); }
        }

        public bool IsVisibleAI
        {
            get { return _IsVisibleAI; }
            set { SetProperty(ref _IsVisibleAI, value); }
        }

        public bool IsVisibleButtonList
        {
            get { return _IsVisibleButtonList; }
            set { SetProperty(ref _IsVisibleButtonList, value); }
        }

        public bool IsRunning
        {
            get { return _IsRunning; }
            set { SetProperty(ref _IsRunning, value); }
        }

        #endregion

        #region Constructors
        public DescargaViewModel()
        {
            this.IsVisibleButtonList = true;
            this.IsVisibleAI = false;
            ShowSubordinates();
            ValidateDevice.Validate("StandardLogin");
            //MsgResponse("Ingresó a pantalla para subir asistencia guardada localmente");
        }
        #endregion

        #region Commands
        public ICommand DownloadCommand
        {
            get { return new RelayCommand(Download); }
        }

        public async void Download()
        {
            this.IsVisibleAI = true;
            this.IsRunning = true;
            this.IsVisibleButtonList = false;

            try
            {
                SendDataDownloadUser usuario = new SendDataDownloadUser { IdUsuario = App.CurrentUserAsistencia.IdUsuario };
                var user = await ApiServices.Post<Cosmic.Response, SendDataDownloadUser>(ApiUrl.sDownloadSubordinate, usuario, null);

                if (user != null)
                {
                    if (user.Success)
                    {
                        App.DBasistencia.Execute("DELETE FROM Subordinado");
                        App.DBasistencia.Execute("VACUUM");
                        App.DBasistencia.SaveChanges();

                        var data = user.Data.To<List<LiteConnection.ORM.Subordinado>>();

                        foreach (var item in data)
                        {
                            App.DBasistencia.Subordinado.Add(item);
                        }
                        App.DBasistencia.SaveChanges();
                    }
                    StoreService.SaveStore(App.CurrentUserAsistencia.IdUsuario);
                    this.IsVisibleAI = false;
                    this.IsRunning = false;
                    this.IsVisibleButtonList = true;
                    MsgResponse("Se han descargado todos los usuarios.");
                }
            }
            catch (Exception ex)
            {
                this.IsVisibleAI = false;
                this.IsRunning = false;
                this.IsVisibleButtonList = true;
                MsgResponse("No es posible descargar en estos momentos. Intente más tarde.");
                Crashes.TrackError(ex, App.TrackData());
            }

            ShowSubordinates();
        }

        private void ShowSubordinates()
        {
            UsersList = App.DBasistencia.Subordinado.ToList().To<ObservableCollection<LiteConnection.ORM.Subordinado>>();
        }
        #endregion

        #region Methods
        public async void MsgResponse(string arg)
        {
            await Application.Current.MainPage.DisplayAlert("Mi Asistencia", arg, "Aceptar");
        }
        #endregion
    }
}
