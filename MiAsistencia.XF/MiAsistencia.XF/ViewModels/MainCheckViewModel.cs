﻿namespace MiAsistencia.XF.ViewModels
{
    using Cosmic;
    using MiAsistencia.XF.LiteConnection.ORM;
    using MiAsistencia.XF.Models;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using Xamarin.Forms;

    public class MainCheckViewModel : BaseViewModel
    {
        #region Services

        #endregion

        #region Attributes
        ObservableCollection<CheckItemViewModel> _CheckItems;
        ObservableCollection<CheckItem> _ci;
        bool _IsEnabled;
        bool? _IsSelected;
        string _Titulo;
        #endregion

        #region Properties
        public ObservableCollection<CheckItemViewModel> CheckItems
        {
            get { return this._CheckItems; }
            set { SetProperty(ref this._CheckItems, value); }
        }

        public bool IsEnabled
        {
            get { return this._IsEnabled; }
            set { SetProperty(ref this._IsEnabled, value); }
        }

        public bool? IsSelected
        {
            get { return null; }
            //set { SetProperty(ref this._IsEnabled, value); }
        }
        public string Titulo
        {
            get { return _Titulo; }
            set { SetProperty(ref this._Titulo, value); }
        }
        #endregion

        #region Constructor
        public MainCheckViewModel()
        {
            instance = this;
            //MsgResponse("Ingresó a pantalla de selección de Entrada/Salida");
            this.Titulo = "Elige la acción a realizar";
            this.LoadCheckItems();
            Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.SeleccionDeEntradaSalida, "Pantalla entrada/salida", "MainCheckPage");
        }
        #endregion

        #region Singleton
        private static MainCheckViewModel instance;
        public static MainCheckViewModel GetInstance()
        {
            if (instance == null)
                return new MainCheckViewModel();
            return instance;
        }
        #endregion

        #region Methods
        private void LoadCheckItems()
        {
            _ci = new ObservableCollection<CheckItem>()
            {
                new CheckItem { ItemId = 1, ItemType = "Entrada", Icon = "checkin_logo.png" },
                new CheckItem { ItemId = 0, ItemType = "Salida", Icon = "checkout_logo.png" },
            };
            IsEnabled = true;
            CheckItems = _ci.To<ObservableCollection<CheckItemViewModel>>();
        }

        public async void MsgResponse(string arg)
        {
            await Application.Current.MainPage.DisplayAlert("Mi Asistencia", arg, "Aceptar");
        }

        #endregion
    }
}
