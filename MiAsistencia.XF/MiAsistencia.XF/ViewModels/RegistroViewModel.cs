﻿namespace MiAsistencia.XF.ViewModels
{
    using Common;
    using Cosmic;
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Services.API;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;

    public class RegistroViewModel : BaseViewModel
    {
        #region Attributes
        bool _IsLoaded;
        bool _Exists;
        bool _ArePrograms;
        bool _IsPassword;
        bool _IsRegistered;
        bool _IsEnabled;
        int _IdUsuarioCosmic;
        string _NoEmpleado;
        string _IMSS;
        string _Alias;
        string _Nombre;
        string _Correo;
        string _Contrasennia;
        string _Contrasennia2;
        RazonSocial _Empresa;
        List<Programas> _Programs;
        List<RazonSocial> _Empresas;
        #endregion

        #region Properties
        public bool IsLoaded
        {
            get { return _IsLoaded; }
            set { SetProperty(ref _IsLoaded, value); }
        }

        public bool Exists
        {
            get { return _Exists; }
            set { SetProperty(ref _Exists, value); }
        }

        public bool ArePrograms
        {
            get { return _ArePrograms; }
            set { SetProperty(ref _ArePrograms, value); }
        }

        public bool IsPassword
        {
            get { return _IsPassword; }
            set { SetProperty(ref _IsPassword, value); }
        }

        public bool IsRegistered
        {
            get { return _IsRegistered; }
            set { SetProperty(ref _IsRegistered, value); }
        }

        public bool IsEnabled
        {
            get { return _IsEnabled; }
            set { SetProperty(ref _IsEnabled, value); }
        }

        public int IdUsuarioCosmic
        {
            get { return _IdUsuarioCosmic; }
            set { SetProperty(ref _IdUsuarioCosmic, value); }
        }

        public string NoEmpleado
        {
            get { return _NoEmpleado; }
            set { SetProperty(ref _NoEmpleado, value); }
        }

        public string IMSS
        {
            get { return _IMSS; }
            set { SetProperty(ref _IMSS, value); }
        }

        public string Alias
        {
            get { return _Alias; }
            set { SetProperty(ref _Alias, value); }
        }

        public string Nombre
        {
            get { return _Nombre; }
            set { SetProperty(ref _Nombre, value); }
        }

        public string Correo
        {
            get { return _Correo; }
            set { SetProperty(ref _Correo, value); }
        }

        public string Contrasennia
        {
            get { return _Contrasennia; }
            set { SetProperty(ref _Contrasennia, value); }
        }

        public string Contrasennia2
        {
            get { return _Contrasennia2; }
            set { SetProperty(ref _Contrasennia2, value); }
        }

        public RazonSocial Empresa
        {
            get { return _Empresa; }
            set { SetProperty(ref _Empresa, value); }
        }

        public List<Programas> Programs
        {
            get { return _Programs; }
            set { SetProperty(ref _Programs, value); }
        }

        public List<RazonSocial> Empresas
        {
            get { return _Empresas; }
            set { SetProperty(ref _Empresas, value); }
        }
        #endregion

        #region Constructors
        public RegistroViewModel()
        {
            IsBusy = false;
            IsEnabled = true;
        }
        #endregion

        #region Methods
        bool ValidateFields(ref string mensaje)
        {
            bool bandera = true;

            if (Exists)
            {
                if (Empresa is null)
                {
                    bandera = false;
                    mensaje = "Seleccione la empresa.";
                    return bandera;
                }
            }

            if (string.IsNullOrEmpty(IMSS))
            {
                bandera = false;
                mensaje = "Ingrese un dato para el número de IMSS.";
                return bandera;
            }
            if (string.IsNullOrEmpty(NoEmpleado))
            {
                bandera = false;
                mensaje = "Ingrese un número de empleado.";
                return bandera;
            }
            if (string.IsNullOrEmpty(Nombre))
            {
                bandera = false;
                mensaje = "Ingrese un número de empleado.";
                return bandera;
            }
            if (string.IsNullOrEmpty(Alias))
            {
                bandera = false;
                mensaje = "Ingrese un número de empleado.";
                return bandera;
            }
            if (string.IsNullOrEmpty(Correo))
            {
                bandera = false;
                mensaje = "Ingrese un número de empleado.";
                return bandera;
            }
            if (this.IsPassword)
            {
                if (string.IsNullOrEmpty(Contrasennia))
                {
                    bandera = false;
                    mensaje = "Ingrese un número de empleado.";
                    return bandera;
                }
                if (string.IsNullOrEmpty(Contrasennia2))
                {
                    bandera = false;
                    mensaje = "Ingrese un número de empleado.";
                    return bandera;
                }
                if (!Contrasennia.Equals(Contrasennia2))
                {
                    bandera = false;
                    mensaje = "Las contraseñas no coinciden";
                    return bandera;
                } 
            }

            return bandera;
        }

        void CleanControls()
        {
            this.Alias = string.Empty;
            this.Contrasennia = string.Empty;
            this.Contrasennia2 = string.Empty;
            this.Correo = string.Empty;
            this.IMSS = string.Empty;
            this.Nombre = string.Empty;
        }

        public async void MsgResponse(string mensaje)
        {
            await App.Current.MainPage.DisplayAlert("Mi Asistencia", mensaje, "Aceptar");
        }

        private async void GetCompanies()
        {
            var result = await ApiServices.Post<Response>(ApiUrl.sGetCompanies, null, null);

            if (result != null)
            {
                this.Empresas = result.Data.To<List<RazonSocial>>();
            }
            else
                this.Empresas = new List<RazonSocial>();
        }
        #endregion

        public ICommand FindUserCommand
        {
            get
            {
                return new RelayCommand(Find);
            }
        }

        public ICommand RegisterCommand
        {
            get
            {
                return new RelayCommand(Register);
            }
        }

        public async void Find()
        {
            IsEnabled = false;
            IsBusy = true;
            if (!NoEmpleado.Equals("0"))
            {
                usuario model = new usuario
                {
                    NoEmpleado = int.Parse(this.NoEmpleado),
                };

                var register = await ApiServices.Post<Response, usuario>(ApiUrl.sExistsUser, model, null);

                if (register != null)
                {
                    UsuarioAlta user = register.Data.To<UsuarioAlta>();
                    if (user != null)
                    {
                        GetCompanies();
                        this.Alias = $"{ user.nombre.Substring(0, 2).ToLower() }{ user.paterno.Substring(0, 2).ToLower() }{ user.materno.Substring(0, 2).ToLower() } ";
                        this.IMSS = user.cedula_imss.Trim();
                        this.Correo = user.Email.Trim();
                        this.Nombre = $"{ user.paterno.Trim() } { user.materno.Trim() } {user.nombre.Trim() }";
                        if (user.Programas != null && user.Programas.Count > 0)
                        {
                            this.Programs = user.Programas;
                            this.IsPassword = false;
                            this.ArePrograms = true;
                            var program = Programs.FirstOrDefault(prop => prop.IdPrograma.ToString().Equals(ApiUrl.tokenApp.ToLower()));
                            if (program != null)
                            {
                                this.IsRegistered = false;
                                MsgResponse("Este usuario ya se encuentra registrado. No es necesario crear la cuenta.");
                            }
                            else
                                this.IsRegistered = true;
                        }
                        else
                        {
                            IsPassword = true;
                            ArePrograms = false;
                            this.IsRegistered = true;
                        }
                    }
                    this.Exists = true;
                    this.IsLoaded = true;
                    IsEnabled = true;
                }
            }
            else
            {
                CleanControls();
                IsLoaded = true;
                IsEnabled = true;
                IsPassword = true;
                ArePrograms = false;
                this.IsRegistered = true;
            }
            IsBusy = false;
        }

        public async void Register()
        {
            string mensaje = string.Empty;

            if (ValidateFields(ref mensaje))
            {
                IsBusy = true;
                IsLoaded = false;

                usuario model = new usuario
                {
                    Contrasena = this.Contrasennia,
                    Nombre = Nombre,
                    NoEmpleado = int.Parse(this.NoEmpleado),
                    Alias = Alias,
                    Correo = this.Correo,
                    IdPrograma = Guid.Parse(ApiUrl.tokenApp),
                    IMSS = "SD",
                    RFC = "SD",
                    Telefono = "SD",
                    Dev = false,
                    IdPerfil = (int)PublicRegister.Perfil.Empleado
                };

                switch (Empresa.IdRazonSocial)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        model.IdParent = (int)PublicRegister.Parent.BTL;
                        break;
                    default:
                        model.IdParent = 531;
                        break;
                }

                var register = await ApiServices.Post<Response, usuario>(ApiUrl.sRegisterUser, model, null);

                if (register != null)
                {
                    MsgResponse(register.Message);
                    await App.Current.MainPage.Navigation.PopAsync();
                }
                else
                {
                    MsgResponse("No se puede realizar el registro en estos momentos, intente más tarde");
                }

                IsBusy = false;
                IsLoaded = true; 
            }
            else
            {
                MsgResponse(!string.IsNullOrEmpty(mensaje) ? mensaje : "Te hacen falta campos por llenar");
            }
        }
    }
}
