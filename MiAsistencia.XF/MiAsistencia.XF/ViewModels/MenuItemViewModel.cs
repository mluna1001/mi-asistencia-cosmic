﻿namespace MiAsistencia.XF.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using MiAsistencia.XF.LiteConnection.ORM;
    using MiAsistencia.XF.Services;
    using System.Windows.Input;

    public class MenuItemViewModel
    {
        public MenuUser menuUser { get; set; }

        private NavigationService navigationService;
        public MenuItemViewModel()
        {
            navigationService = new NavigationService();
        }

        public ICommand NavigateCommand
        {
            get
            {
                return new RelayCommand(() =>

                    navigationService.Navigate(menuUser.PageName)
                );
            }
        }
    }
}
