﻿namespace MiAsistencia.XF.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Helpers;
    using LiteConnection.ORM;
    using Microsoft.AppCenter.Crashes;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Input;
    using Xamarin.Forms;
    using SignaturePad.Forms;
    using System.IO;
    using System.Threading.Tasks;
    using MiAsistencia.XF.Services.API;

    public class ReceptionViewModel : BaseViewModel
    {
        #region Attributes
        bool _IsVisibleUbication;
        bool _IsAIRunning;
        bool _IsAIVisible;
        bool _IsDisabled;
        decimal _Latitud = 0;
        decimal _Longitud = 0;
        string _Nombre;
        string _StatusUbication;
        string message = string.Empty;
        byte[] _Firma;

        #endregion

        #region Properties
        public bool IsVisibleUbication
        {
            get { return _IsVisibleUbication; }
            set { SetProperty(ref _IsVisibleUbication, value); }
        }

        public bool IsAIRunning
        {
            get { return _IsAIRunning; }
            set { SetProperty(ref _IsAIRunning, value); }
        }

        public bool IsAIVisible
        {
            get { return _IsAIVisible; }
            set { SetProperty(ref _IsAIVisible, value); }
        }

        public bool IsDisabled
        {
            get { return true; }
            set { SetProperty(ref _IsDisabled, value); }
        }

        public string Nombre
        {
            get { return _Nombre; }
            set { SetProperty(ref _Nombre, value) ; }
        }

        public string StatusUbication
        {
            get { return _StatusUbication; }
            set { SetProperty(ref _StatusUbication, value); }
        }

        public byte[] Firma
        {
            get { return _Firma; }
            set { SetProperty(ref _Firma, value ); }
        }

        public decimal Latitud
        {
            get { return _Latitud; }
            set { SetProperty(ref _Latitud, value); }
        }

        public decimal Longitud
        {
            get { return _Longitud; }
            set { SetProperty(ref _Longitud, value); }
        }

        #endregion

        #region Constructors
        public ReceptionViewModel()
        {
            this.StatusUbication = "Estamos buscando tu ubicación...";
            this.IsAIRunning = true;
            this.IsAIVisible = true;
            GetPosition();
        }
        #endregion

        #region Singleton

        public static ReceptionViewModel instance;

        public static ReceptionViewModel GetInstance()
        {
            if (instance == null)
            {
                return new ReceptionViewModel();
            }
            return instance;
        }

        #endregion

        #region Methods
        private async void GetPosition()
        {
            if (GeoTools.IsLocationAvailable())
            {
                try
                {
                    var position = await GeoTools.GetPositionAsync();
                    if (position != null)
                    {
                        _Latitud = (decimal)position.Latitude;
                        _Longitud = (decimal)position.Longitude;
                        this.StatusUbication = "¡Ya encontramos tu ubicación!";
                        this.IsAIRunning = false;
                        this.IsAIVisible = false;
                        this.IsVisibleUbication = true;
                    }
                }
                catch (Exception ex)
                {
                    MsgResponse("En estos momentos no podemos obtener información, verifica tu conexión a internet");
                    await App.Current.MainPage.Navigation.PopAsync();
                }
            }
        }

        public async void MsgResponse(string arg)
        {
            await Application.Current.MainPage.DisplayAlert("Mi Asistencia", arg, "Aceptar");
        }

        private async Task<Cosmic.Response> UploadServer(Recepcion asistencia)
        {
            Dictionary<string, string> ora = new Dictionary<string, string>();

            var resptiendas = await ApiServices.Post<Cosmic.Response, Recepcion>(ApiUrl.sSaveSignature, asistencia, null);
            if (resptiendas != null)
            {
                if (resptiendas.Success)
                {
                    asistencia.Sincronizado = true;
                    App.DBasistencia.Entry(asistencia).State = Services.DataBase.EntryState.Modify;
                    App.DBasistencia.SaveChanges();
                }
                return resptiendas;
            }
            return new Cosmic.Response { Success = false };
        }
        #endregion

        #region Commands
        public ICommand FirmarCommand
        {
            get { return new RelayCommand(Firmar); }
        }

        private async void Firmar()
        {
            try
            {
                this.IsAIRunning = true;
                this.IsAIVisible = true;
                this.IsVisibleUbication = false;
                byte[] foto;

                ValidateDevice.Validate("ReceptionPage");

                if (!string.IsNullOrEmpty(Nombre))
                {
                    Recepcion recepcion = new Recepcion
                    {
                        IdUsuario = App.CurrentUserAsistencia.IdUsuario,
                        NombreRecibe = Nombre,
                        Latitud = _Latitud,
                        Longitud = _Longitud,
                        FechaRecibe = DateTime.Now,
                        
                        Sincronizado = false
                    };

                    Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.SeFirmoRecibido, "", "ReceptionPage");
                    message = $"Se ha guardado la recepción realizada a \"{ Nombre }\" con fecha { recepcion.FechaRecibe }";

                    App.DBasistencia.Recepcion.Add(recepcion);
                    App.DBasistencia.SaveChanges();

                    this.IsAIRunning = false;
                    this.IsAIVisible = false;
                    this.IsVisibleUbication = true;

                    var upload = await UploadServer(recepcion);

                    if (upload.Success)
                    {
                        message = message + " La información se ha subido al servidor.";
                    }
                    else
                    {
                        message = message + " La información se ha guardado de forma local. Recuerda subirla manualmente después";
                    }

                    App.CurrentIdTienda = null;
                    MsgResponse(message);
                    await App.Navigator.PopAsync();
                }
                else
                {
                    MsgResponse("Por favor, ingrese el nombre de la persona que recibe");
                    this.IsAIRunning = false;
                    this.IsAIVisible = false;
                    this.IsVisibleUbication = true;
                }
            }
            catch (Exception ex)
            {
                this.IsDisabled = true;
                MsgResponse("En estos momentos no es posible guardar la recepción, intente más tarde");
                this.IsAIRunning = false;
                this.IsAIVisible = false;
                this.IsVisibleUbication = true;
                Crashes.TrackError(ex, App.TrackData());
            }
        }
        #endregion
    }
}
