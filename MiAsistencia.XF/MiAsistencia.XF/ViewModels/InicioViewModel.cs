﻿namespace MiAsistencia.XF.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using MiAsistencia.XF.Helpers;
    using MiAsistencia.XF.LiteConnection.ORM;
    using MiAsistencia.XF.Views.Pages;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class InicioViewModel : BaseViewModel
    {
        private string bienvenido;
        public string Bienvenido
        {
            get { return this.bienvenido; }
            set { SetProperty(ref this.bienvenido, value); }
        }

        public ICommand GoToCommand { get { return new RelayCommand(GoTo); } }

        public void GoTo()
        {
            Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.SeleccionDeEntradaSalida, "Ingresó a realizar entrada/salida", "MasterPage");
            var mainCheck = MainViewModel.GetInstance();
            mainCheck.MainCheck = new MainCheckViewModel();
            App.Navigator.PushAsync(new MainCheckPage());
        }

        public InicioViewModel()
        {
            instance = this;
            Bienvenido = "Hi";

            GetPosition();

            if (Device.RuntimePlatform != Device.UWP)
            {
                ValidateDevice.Validate("MainPage");
            }
        }

        #region Singleton
        private static InicioViewModel instance;
        public static InicioViewModel GetInstance()
        {
            if (instance == null)
                return new InicioViewModel();
            return instance;
        }
        #endregion

        private async void GetPosition()
        {
            if (GeoTools.IsLocationAvailable())
            {
                //var p = await GeoTools.GetPositionAsync();
            }
        }
    }
}
