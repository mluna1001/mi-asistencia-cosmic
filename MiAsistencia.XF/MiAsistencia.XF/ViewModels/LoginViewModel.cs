﻿namespace MiAsistencia.XF.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using MiAsistencia.XF.Common;
    using MiAsistencia.XF.Helpers;
    using MiAsistencia.XF.LiteConnection.ORM;
    using MiAsistencia.XF.Services;
    using MiAsistencia.XF.Services.API;
    using MiAsistencia.XF.Views;
    using MiAsistencia.XF.Views.Pages;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class LoginViewModel : BaseViewModel
    {
        #region Data
        private string alias;
        public string Alias
        {
            get { return this.alias; }
            set { SetProperty(ref this.alias, value); }
        }

        private string pass;
        public string Pass
        {
            get { return pass; }
            set { SetProperty(ref this.pass, value); }
        }

        private bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set { SetProperty(ref this.isEnabled, value); }
        }

        private bool isRemembered;
        public bool IsRemembered
        {
            get { return isRemembered; }
            set { SetProperty(ref this.isRemembered, value); }
        }


        public ICommand Logear
        {
            get { return new RelayCommand(UserLogin); }
        }

        public ICommand RegisterCommand
        {
            get { return new RelayCommand(Register); }
        }
        #endregion

        #region Singleton
        private static LoginViewModel instance;
        public static LoginViewModel GetInstance()
        {
            if (instance == null)
                return new LoginViewModel();
            return instance;
        }
        #endregion

        public LoginViewModel()
        {
            instance = this;
            IsRemembered = true;
            IsEnabled = true;
#if DEBUG
            this.Alias = "794569";
            this.Pass = "5Yt@tyR";
#endif
            ValidateDevice.Validate("StandardLogin");

        }

        #region Methods
        public async void UserLogin()
        {
            IsBusy = true;
            IsEnabled = false;

            var r = await ApiServices.Post<Cosmic.LoginResponse, Cosmic.UserLogin>(ApiUrl.slogin, new Cosmic.UserLogin
            {
                alias = this.Alias,
                password = this.Pass,
                dev = false,
                idPrograma = Guid.Parse(ApiUrl.tokenApp)
            }, null);
            if (r != null && r.Success)
            {
                var r2 = await ApiServices.Post<UsuarioCosmicDto, Cosmic.User>(ApiUrl.slogin2, r.User, null);
                if (r2 != null)
                {
                    App.CurrentUser = r2;
                    var obj = new UserAsistencia { Alias = r2.usuario.Alias, Nombre = r2.usuario.Nombre };
                    App.CurrentUserAsistencia = obj;

                    GuardaUsuarioLocal();
                    //MsgResponse("Acceso concedido.");
                }
                else
                {
                    MsgResponse("Acceso denegado. Verifica tus credenciales.");
                    return;
                }
            }
            else
            {
                UserAsistencia user = App.DBasistencia.userAsistencia.FirstOrDefault(x => x.Alias == this.Alias);
                if (user != null)
                {
                    BouncyCastleHashing bouncy = new BouncyCastleHashing();
                    bool IsAuthenticated = bouncy.ValidatePassword(this.pass, new byte[16], 10000, 20, user.Password);

                    if (IsAuthenticated)
                    {
                        Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IniciarSesionLocalConCredenciales, "Inició sesión de forma local con credenciales ingresadas", "LoginPage");
                        App.CurrentUserAsistencia = user;
                    }
                }
                else
                {
                    MsgResponse("Acceso denegado. " + r?.Message);
                    return;
                }
            }

            var mvm = MainViewModel.GetInstance();
            //mvm.Token = null;
            mvm.Inicios = new InicioViewModel();
            mvm.LoadMenu();
            App.Current.MainPage = new MasterPage();
        }

        private async void GuardaUsuarioLocal()
        {
            if (App.CurrentUser != null)
            {
                //Task<UsuarioAsistenciaDto> t = GetDataAsist(App.CurrentUser.usuario.IdUsuario);
                //t.Start();

                var usuarioAsis = await Task.Run(() => GetDataAsist(App.CurrentUser.usuario.IdUsuario));
                //var usuarioAsis = t.Result;

                UserAsistencia user = new UserAsistencia
                {
                    IdUsuario = App.CurrentUser.usuario.IdUsuario,
                    Alias = App.CurrentUser.usuario.NoEmpleado.ToString(),
                    Nombre = App.CurrentUser.usuario.Nombre,
                    Password = usuarioAsis.Contrasena,
                    IdPerfil = App.CurrentUser.PerfilDto.IdPerfil,
                    IdProyecto = usuarioAsis.IdProyecto,
                    IdRegion = usuarioAsis.IdRegion
                };

                if (user.IdUsuario != 0)
                {
                    if (!App.DBasistencia.userAsistencia.Exists(u => u.IdUsuario == user.IdUsuario))
                    {
                        App.DBasistencia.userAsistencia.Add(user);
                        App.DBasistencia.SaveChanges();
                    }
                }

                App.CurrentUserAsistencia = user;
                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IniciarSesionConCredenciales, "Inició sesión con credenciales ingresadas", "LoginPage");
                if (this.IsRemembered)
                {
                    Settings.IsRemembered = "true";
                    Settings.Alias = App.CurrentUserAsistencia.Alias;
                }
                else
                {
                    Settings.IsRemembered = "false";
                    Settings.Alias = string.Empty;
                }
                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.RegistroIMEIDispositivo, App.DeviceIdentifier, "LoginPage");
                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.RegistroAppsIsntaladas, App.ListInstallApps, "LoginPage");
            }
        }


        private async Task<UsuarioAsistenciaDto> GetDataAsist(int idUsuario)
        {
            UsuarioAsistenciaDto usuario = new UsuarioAsistenciaDto();
            var user = await ApiServices.Post<UsuarioAsistenciaDto, Dictionary<string, string>>(
                ApiUrl.sUserData
                , new Dictionary<string, string> { { "IdUsuario", idUsuario.ToString() }, { "Contrasena", this.pass } },
                null);

            if (user != null)
            {
                usuario.IdUsuario = user.IdUsuario;
                usuario.IdRegion = user.IdRegion;
                usuario.IdProyecto = user.IdProyecto;
                usuario.IdCliente = user.IdCliente;
                usuario.Contrasena = user.Contrasena;
            }
            return usuario;
        }

        public async void MsgResponse(string arg)
        {
            await Application.Current.MainPage.DisplayAlert("Mi Asistencia", arg, "Aceptar");

            IsBusy = false;
            IsEnabled = true;
        }

        public async void Register()
        {
            MainViewModel.GetInstance().Registro = new RegistroViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new RegisterPage());
        }

        #endregion
    }
}
