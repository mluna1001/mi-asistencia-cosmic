﻿namespace MiAsistencia.XF.ViewModels
{
    using Cosmic;
    using MiAsistencia.XF.LiteConnection.ORM;
    using MiAsistencia.XF.Models;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using Xamarin.Forms;

    public class MainCodeCheckViewModel : BaseViewModel
    {
        #region Services

        #endregion

        #region Attributes
        ObservableCollection<CheckItemViewModel> _CheckItems;
        ObservableCollection<CheckItem> _ci;
        bool _IsEnabled;
        bool? _IsSelected;
        string _Titulo;
        #endregion

        #region Properties
        public ObservableCollection<CheckItemViewModel> CheckItems
        {
            get { return this._CheckItems; }
            set { SetProperty(ref this._CheckItems, value); }
        }

        public bool IsEnabled
        {
            get { return this._IsEnabled; }
            set { SetProperty(ref this._IsEnabled, value); }
        }

        public bool? IsSelected
        {
            get { return null; }
            //set { SetProperty(ref this._IsEnabled, value); }
        }
        public string Titulo
        {
            get { return _Titulo; }
            set { SetProperty(ref this._Titulo, value); }
        }
        #endregion

        #region Constructor
        public MainCodeCheckViewModel()
        {
            instance = this;
            //MsgResponse("Ingresó a pantalla de selección de Entrada/Salida con código QR");
            var subordinados = App.DBasistencia.Subordinado.Count();

            if (subordinados > 0)
            {
                this.Titulo = "Elige la acción a realizar";

                this.LoadCheckItems();

                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.SeleccionDeEntradaSalida, "Pantalla entrada/salida", "MainCheckPage");
            }
            else
            {
                MsgResponse("Primero descargue datos. Vaya al menú de Descargas");
                App.Navigator.PopAsync();
            }
        }
        #endregion

        #region Singleton
        private static MainCodeCheckViewModel instance;
        public static MainCodeCheckViewModel GetInstance()
        {
            if (instance == null)
                return new MainCodeCheckViewModel();
            return instance;
        }
        #endregion

        #region Methods
        private void LoadCheckItems()
        {
            _ci = new ObservableCollection<CheckItem>()
                {
                    new CheckItem { ItemId = 1, ItemType = "Entrada", Icon = "checkin_logo.png", TypeCheckId = 1 },
                    new CheckItem { ItemId = 2, ItemType = "Salida Almuerzo", Icon = "checkoutlunch_logo.png", TypeCheckId = 2 },
                    new CheckItem { ItemId = 3, ItemType = "Entrada Almuerzo", Icon = "checkinlunch_logo.png", TypeCheckId = 3 },
                    new CheckItem { ItemId = 4, ItemType = "Salida", Icon = "checkout_logo.png", TypeCheckId = 4 },
            };

            IsEnabled = true;
            CheckItems = _ci.To<ObservableCollection<CheckItemViewModel>>();
        }

        public async void MsgResponse(string arg)
        {
            await Application.Current.MainPage.DisplayAlert("Mi Asistencia", arg, "Aceptar");
        }

        #endregion
    }
}
