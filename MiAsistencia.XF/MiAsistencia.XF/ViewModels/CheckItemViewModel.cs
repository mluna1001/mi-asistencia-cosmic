﻿namespace MiAsistencia.XF.ViewModels
{
    using Models;
    using GalaSoft.MvvmLight.Command;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Windows.Input;
    using Views.Pages;

    public class CheckItemViewModel : CheckItem
    {
        #region Commands
        public ICommand SelectCheckCommand
        {
            get
            {
                return new RelayCommand(SelectCheck);
            }
        }

        private async void SelectCheck()
        {
            MainViewModel.GetInstance().Check = new CheckViewModel(this);
            MainViewModel.GetInstance().Title = this.ItemType;
            await App.Navigator.PushAsync(new CheckPage());
            //await Application.Current.MainPage.Navigation.PushAsync(new LandTabbedPage());
        }

        public ICommand SelectCodeCheckCommand
        {
            get { return new RelayCommand(SelectCodeCheck); }
        }

        private async void SelectCodeCheck()
        {
            MainViewModel.GetInstance().CodeCheck = new CodeCheckViewModel(this);
            MainViewModel.GetInstance().Title = this.ItemType;
            await App.Navigator.PushAsync(new CodeCheckPage());
        }
        #endregion
    }
}
