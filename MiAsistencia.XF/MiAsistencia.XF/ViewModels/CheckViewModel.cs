﻿namespace MiAsistencia.XF.ViewModels
{
    using Cosmic;
    using GalaSoft.MvvmLight.Command;
    using LiteConnection.ORM;
    using MiAsistencia.XF.Common;
    using MiAsistencia.XF.Helpers;
    using MiAsistencia.XF.Services;
    using Microsoft.AppCenter.Crashes;
    using Services.API;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Xamarin.Forms;
    //using Plugin.Geofence.Abstractions;
    //using Plugin.Geofence;
    using MiAsistencia.XF.Helpers.GeoHelp;
    using MiAsistencia.XF.Interfaces;
    using MiAsistencia.XF.Models;

    public class CheckViewModel : BaseViewModel
    {
        #region Attributes
        int _UTCTime;
        bool _IsVisibleUbication;
        bool _IsAIRunning;
        bool _IsAIVisible;
        bool _IsDisabled;
        string _TitlePage;
        string _StatusUbication;
        string message = string.Empty;
        decimal _Latitud = 0;
        decimal _Longitud = 0;
        CheckItemViewModel _Item;
        Tienda _StoreSelected;
        ObservableCollection<Tienda> _ListaTiendas;
        List<Asistencia> _ListAsistencias;
        #endregion

        #region Properties
        public bool IsVisibleUbication
        {
            get { return _IsVisibleUbication; }
            set { SetProperty(ref _IsVisibleUbication, value); }
        }

        public bool IsAIRunning
        {
            get { return _IsAIRunning; }
            set { SetProperty(ref _IsAIRunning, value); }
        }

        public bool IsAIVisible
        {
            get { return _IsAIVisible; }
            set { SetProperty(ref _IsAIVisible, value); }
        }

        public bool IsDisabled
        {
            get { return true; }
            set { SetProperty(ref _IsDisabled, value); }
        }

        public string TitlePage
        {
            get { return _TitlePage; }
            set { SetProperty(ref _TitlePage, value); }
        }

        public string StatusUbication
        {
            get { return _StatusUbication; }
            set { SetProperty(ref _StatusUbication, value); }
        }

        public ObservableCollection<Tienda> ListaTiendas
        {
            get { return _ListaTiendas; }
            set { SetProperty(ref _ListaTiendas, value); }
        }

        public Tienda StoreSelected
        {
            get { return _StoreSelected; }
            set { SetProperty(ref _StoreSelected, value); }
        }

        public List<Asistencia> ListAsistencias
        {
            get { return _ListAsistencias; }
            set { SetProperty(ref _ListAsistencias, value); }
        }
        #endregion

        #region Constructor
        public CheckViewModel(CheckItemViewModel item)
        {
            ValidateDevice.Validate("CheckPage");
            try
            {
                this._Item = item;
                this.TitlePage = item.ItemType;
                this.StatusUbication = "Estamos buscando tu ubicación...";
                this.IsAIRunning = true;
                this.IsAIVisible = true;
                if (_Item.ItemId == 1)
                {
                    Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IngresoAPantallaDeEntrada, "Ingresó a pantalla de entrada", "CheckPage");
                }
                else
                {
                    Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IngresoAPantallaDeSalida, "Ingresó a pantalla de salida", "CheckPage");
                }
                GetPosition();
                UploadBinnacle();
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
            }
        }

        #endregion

        #region Methods
        private async Task UploadBinnacle()
        {
            var bitacoras = App.DBasistencia.Bitacora.Where(x => !x.Sincronizado).Take(10).ToList();

            var respBitacora = await ApiServices.Post<Cosmic.Response, List<Bitacora>>(ApiUrl.sSaveBinnacle, bitacoras, null);
            if (respBitacora != null)
            {
                if (respBitacora.Success)
                {
                    foreach (var item in bitacoras)
                    {
                        item.Sincronizado = true;
                        App.DBasistencia.Entry(item).State = Services.DataBase.EntryState.Modify;
                        App.DBasistencia.SaveChanges();
                    }
                }
            }
        }

        private async void GetPosition()
        {
            if (GeoTools.IsLocationAvailable())
            {
                try
                {
                    var position = await GeoTools.GetPositionAsync();
                    if (position != null)
                    {
                        _Latitud = (decimal)position.Latitude;
                        _Longitud = (decimal)position.Longitude;
                        GetAsistencias(App.CurrentUserAsistencia.IdUsuario);
                        GetStores();
                    }
                }
                catch (Exception ex)
                {
                    MsgResponse("En estos momentos no podemos obtener información, verifica tu conexión a internet");
                    if (Device.RuntimePlatform != Device.iOS)
                    {
                        await App.Current.MainPage.Navigation.PopAsync(); 
                    }
                    else
                        await App.Navigator.PopAsync();
                }
            }
        }

        private async void GetStores()
        {
            SendDataStore sendData = new SendDataStore
            {
                Latitud = _Latitud,
                Longitud = _Longitud,
                IdUsuario = App.CurrentUserAsistencia.IdUsuario
            };

            var resptiendas = await ApiServices.Post<Cosmic.Response, SendDataStore>(ApiUrl.sGetStores, sendData, null);

            if (resptiendas != null)
            {
                this.StatusUbication = "¡Ya encontramos tu ubicación!";
                this.IsVisibleUbication = true;
                this.IsAIRunning = false;
                this.IsAIVisible = false;

                if (resptiendas.Data != null)
                {
                    this.ListaTiendas = StoreService.GetTiendasRadio(resptiendas.Data.To<List<TiendaAsistencia>>(), _Latitud, _Longitud)
                        .To<ObservableCollection<Tienda>>();
                        
                    //this.ListaTiendas = resptiendas.Data.To<ObservableCollection<Tienda>>();
                }
                else
                {
                    MsgResponse("No hay tiendas disponibles, intente nuevamente");
                }
            }
            else
            {
                this.StatusUbication = "¡Ya encontramos tu ubicación!";
                this.IsVisibleUbication = true;
                this.IsAIRunning = false;
                this.IsAIVisible = false;
                this.ListaTiendas = StoreService.GetOfflineStores(_Latitud, _Longitud).To<ObservableCollection<Tienda>>();
            }

            if (ListaTiendas != null && ListaTiendas.Count > 0)
            {
                if (ListaTiendas.Count == 1)
                {
                    var tienda = ListaTiendas[0];
                    this.StoreSelected = tienda;
                } 
            }

            if (App.OnOffFences)
            {
                if (this.ListaTiendas.Any())
                {
                    try
                    {
                        var geofence = DependencyService.Get<IGeocerca>();
                        var tiendas = resptiendas.Data.To<List<TiendaAsistencia>>();
                        geofence.AddGeofences(tiendas, EnergyConfig.Medium);
                    }
                    catch (Exception ex)
                    {

                    }
                } 
            }
        }

        public async void GetAsistencias(int IdUsuario)
        {
            var asistencia = new Asistencia { IdUsuario = IdUsuario };

            var respAsistencia = await ApiServices.Post<Response, Asistencia>(ApiUrl.sReadCheck, asistencia, null);

            if (respAsistencia != null)
            {
                if (respAsistencia.Success)
                {
                    _ListAsistencias = new List<Asistencia>();
                    this.ListAsistencias = respAsistencia.Data.To<List<Asistencia>>();

                    if (this.ListAsistencias.Count > 0)
                    {
                        SaveAsistencia(this.ListAsistencias); 
                    }
                }
                else
                {
                    _ListAsistencias = new List<Asistencia>();
                }
            }
        }

        private async void SaveAsistencia(List<Asistencia> listAsistencias)
        {
            var actual = DateTime.Now.Date;

            if (!App.DBasistencia.Asistencia.Any(a => a.IdUsuario == listAsistencias[0].IdUsuario && a.Fecha >= actual))
            {
                foreach (var item in listAsistencias)
                {
                    App.DBasistencia.Add(item);
                    App.DBasistencia.SaveChanges();
                }
            }
        }

        public async void MsgResponse(string arg)
        {
            await Application.Current.MainPage.DisplayAlert("Mi Asistencia", arg, "Aceptar");
        }

        private async Task<Cosmic.Response> UploadServer(Asistencia asistencia)
        {
            Dictionary<string, string> ora = new Dictionary<string, string>();

            var resptiendas = await ApiServices.Post<Cosmic.Response, Asistencia>(ApiUrl.sSaveCheck, asistencia, null);
            if (resptiendas != null)
            {
                if (resptiendas.Success)
                {
                    asistencia.Sincronizado = true;
                    App.DBasistencia.Entry(asistencia).State = Services.DataBase.EntryState.Modify;
                    App.DBasistencia.SaveChanges();
                }
                return resptiendas;
            }
            return new Cosmic.Response { Success = false };
        }

        private void SaveStore()
        {
            if (!App.DBasistencia.TiendaAsistencia.Exists(x => x.IdTienda == this.StoreSelected.IdTienda))
            {
                TiendaAsistencia tienda = new TiendaAsistencia
                {
                    IdTienda = this.StoreSelected.IdTienda,
                    NombreTienda = this.StoreSelected.NombreTienda,
                    Latitud = this.StoreSelected.Latitud,
                    Longitud = this.StoreSelected.Longitud
                };

                App.DBasistencia.TiendaAsistencia.Add(tienda);
                App.DBasistencia.SaveChanges();
            }
        }

        #endregion

        #region Commands
        public ICommand SaveCommand
        {
            get { return new RelayCommand(Save); }
        }

        public async void Save()
        {
            try
            {
                this.IsAIRunning = true;
                this.IsAIVisible = true;
                this.IsVisibleUbication = false;
                
                ValidateDevice.Validate("CheckPage");

                App.CurrentIdTienda = StoreSelected.IdTienda;

                if (StoreSelected != null)
                {
                    Asistencia asistencia = new Asistencia
                    {
                        IdUsuario = App.CurrentUserAsistencia.IdUsuario,
                        IdTienda = StoreSelected.IdTienda,
                        Latitud = _Latitud,
                        Longitud = _Longitud,
                        Estatus = _Item.ItemId == 1 ? true : false,
                        Fecha = DateTime.Now,
                        Sincronizado = false
                    };

                    var date = asistencia.Fecha.Date;

                    if (!asistencia.Estatus)
                    {
                        var entrada = App.DBasistencia.Asistencia.FirstOrDefault
                            (et => et.IdUsuario == asistencia.IdUsuario && et.IdTienda == asistencia.IdTienda
                            && et.Fecha >= date && et.IdTipoAsistencia == (int)TypeCheck.TipoAsistencia.Entrada);

                        if (entrada == null)
                        {
                            Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IntentoRealizarUnaSalidaSinEntrada, "Ingresó a pantalla de entrada", "CheckPage");
                            message = $"Debe realizar una entrada primero para realizar una salida en \"{ StoreSelected.NombreTienda }\"";
                            MsgResponse(message);
                            this.IsAIRunning = false;
                            this.IsAIVisible = false;
                            this.IsVisibleUbication = true;
                            return;
                        }

                        var salida = App.DBasistencia.Asistencia.FirstOrDefault
                            (et => et.IdUsuario == asistencia.IdUsuario && et.IdTienda == asistencia.IdTienda
                            && et.Fecha >= date && et.Estatus == false && et.IdTipoAsistencia == (int)TypeCheck.TipoAsistencia.Salida);

                        if (salida != null)
                        {
                            Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IntentoRealizarNuevamenteUnaSalida, "Salida en la misma tienda", "CheckPage");
                            message = $"No puede realizar una salida más para \"{ StoreSelected.NombreTienda }\" debido a que ya ha realizado una previa en este día.";
                            MsgResponse(message);
                            this.IsAIRunning = false;
                            this.IsAIVisible = false;
                            this.IsVisibleUbication = true;
                            return;
                        }

                        message = "Se ha realizado una salida para ";
                        asistencia.IdTipoAsistencia = (int)TypeCheck.TipoAsistencia.Salida;
                    }
                    else
                    {
                        var entrada = App.DBasistencia.Asistencia.FirstOrDefault
                            (et => et.IdUsuario == asistencia.IdUsuario && et.IdTienda == asistencia.IdTienda
                            && et.Fecha >= date && et.IdTipoAsistencia == (int)TypeCheck.TipoAsistencia.Entrada);

                        if (entrada != null)
                        {
                            Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IntentoRealizarNuevamenteUnaEntrada, "Entrada en la misma tienda", "CheckPage");
                            message = $"No puede realizar una entrada para \"{ StoreSelected.NombreTienda }\" debido a que ya ha realizado una previa en este día.";
                            MsgResponse(message);
                            this.IsAIRunning = false;
                            this.IsAIVisible = false;
                            this.IsVisibleUbication = true;
                            return;
                        }

                        message = "Se ha realizado una entrada para ";
                        asistencia.IdTipoAsistencia = (int)TypeCheck.TipoAsistencia.Entrada;
                    }

                    App.DBasistencia.Asistencia.Add(asistencia);
                    App.DBasistencia.SaveChanges();

                    SaveStore();

                    var upload = await UploadServer(asistencia);

                    if (upload.Success)
                    {
                        var asis = upload.Data.To<AsistenciaReal>();
                        message = message + $"\"{ StoreSelected.NombreTienda }\" con fecha { asistencia.Fecha }. ";
                        message = message + "La información se ha subido al servidor.";
                        var tipoCheck = asistencia.Estatus ? (int)Bitacora.AccionBitacora.EntradaSubidaAlServidor : (int)Bitacora.AccionBitacora.SalidaSubidaAlServidor;
                        Bitacora.SaveBitacora(tipoCheck, $"{ (asistencia.Estatus ? "Entrada" : "Salida") } guardada al servidor para {StoreSelected.IdTienda} - {StoreSelected.NombreTienda}", "CheckPage");

                    }
                    else
                    {
                        message = message + $"\"{ StoreSelected.NombreTienda }\" con fecha { asistencia.Fecha }. ";
                        message = message + "La información se guardó en el dispositivo. Más adelante puede subirla manualmente al servidor.";
                        var tipoCheck = asistencia.Estatus ? (int)Bitacora.AccionBitacora.EntradaGuardadaLocalmente : (int)Bitacora.AccionBitacora.SalidaGuardadaLocalmente;
                        Bitacora.SaveBitacora(tipoCheck, $"{ (asistencia.Estatus ? "Entrada" : "Salida") } guardada localmente para {StoreSelected.IdTienda} - {StoreSelected.NombreTienda}", "CheckPage");
                    }

                    App.CurrentIdTienda = null;
                    MsgResponse(message);
                    await App.Navigator.PopAsync();
                }
                else
                {
                    MsgResponse("Por favor, seleccione una tienda");
                    this.IsAIRunning = false;
                    this.IsAIVisible = false;
                    this.IsVisibleUbication = true;
                }
            }
            catch (Exception ex)
            {
                this.IsDisabled = true;
                MsgResponse("En estos momentos no es posible guardar la asistencia, intente más tarde");
                this.IsAIRunning = false;
                this.IsAIVisible = false;
                this.IsVisibleUbication = true;
                Crashes.TrackError(ex, App.TrackData());
            }
        }
        #endregion
    }
}
