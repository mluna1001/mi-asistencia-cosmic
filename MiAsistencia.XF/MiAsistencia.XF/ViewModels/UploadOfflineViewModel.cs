﻿namespace MiAsistencia.XF.ViewModels
{
    using Cosmic;
    using GalaSoft.MvvmLight.Command;
    using MiAsistencia.XF.LiteConnection.ORM;
    using MiAsistencia.XF.Services.API;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Xamarin.Forms;
    using Microsoft.AppCenter.Crashes;
    using MiAsistencia.XF.Helpers;

    public class UploadOfflineViewModel : BaseViewModel
    {
        #region Attributes
        bool _IsVisibleStackLoad;
        bool _IsVisibleStackStores;
        bool _IsVisibleExistsStores;

        ObservableCollection<TiendaAsistenciaDto> _StoreItems;
        string _Titulo;
        #endregion

        #region Properties
        public ObservableCollection<TiendaAsistenciaDto> StoreItems
        {
            get { return _StoreItems; }
            set { SetProperty(ref _StoreItems, value); }
        }
        public string Titulo
        {
            get { return _Titulo; }
            set { SetProperty(ref _Titulo, value); }
        }
        public bool IsVisibleStackLoad
        {
            get { return _IsVisibleStackLoad; }
            set { SetProperty(ref _IsVisibleStackLoad, value); }
        }
        public bool IsVisibleStackStores
        {
            get { return _IsVisibleStackStores; }
            set { SetProperty(ref _IsVisibleStackStores, value); }
        }
        public bool IsVisibleExistsStores
        {
            get { return _IsVisibleExistsStores; }
            set { SetProperty(ref _IsVisibleExistsStores, value); }
        }
        #endregion

        #region Constructors
        public UploadOfflineViewModel()
        {
            ValidateDevice.Validate("UploadOfflinePage");
            //MsgResponse("Ingresó a pantalla para subir asistencia guardada localmente");
            GetOfflineStores();
        }
        #endregion

        #region Singleton
        public static UploadOfflineViewModel instance;
        public static UploadOfflineViewModel GetInstance()
        {
            if (instance == null)
            {
                return new UploadOfflineViewModel();
            }

            return instance;
        }

        #endregion

        #region Methods

        private async Task<Response> UploadServer(List<Asistencia> asistencias)
        {
            var resptiendas = await ApiServices.Post<Cosmic.Response, List<Asistencia>>(ApiUrl.sSaveAllChecks, asistencias, null);
            if (resptiendas != null)
            {
                if (resptiendas.Success)
                {
                    foreach (var asistencia in asistencias)
                    {
                        asistencia.Sincronizado = true;
                        App.DBasistencia.Entry(asistencia).State = Services.DataBase.EntryState.Modify;
                        App.DBasistencia.SaveChanges();
                    }
                }

                return resptiendas;
            }

            return new Response { Success = false, Message = "En estos momentos no es posible subir la información, intenta más tarde" };
        }

        public void GetOfflineStores()
        {
            var lst = (from a in App.DBasistencia.Asistencia
                       join ta in App.DBasistencia.TiendaAsistencia on a.IdTienda equals ta.IdTienda
                       where !a.Sincronizado
                       select new TiendaAsistenciaDto()
                       {
                           IdAsistencia = a.IdAsistencia,
                           Latitud = a.Latitud,
                           Longitud = a.Longitud,
                           IdUsuario = a.IdUsuario,
                           Estatus = a.Estatus,
                           Fecha = a.Fecha,
                           nombrefoto = a.nombrefoto,
                           IdTienda = a.IdTienda,
                           Ubicacion = $"{a.Latitud}, {a.Longitud}",
                           NombreTienda = ta.NombreTienda,
                           TipoCheck = (a.Estatus ? "Entrada: " : "Salida: ") + a.Fecha.ToString()
                       }).ToList();

            if (lst != null && lst.Count > 0)
            {
                this.IsVisibleStackStores = true;
                this.IsVisibleStackLoad = false;
                this.IsVisibleExistsStores = false;
                this.StoreItems = lst.To<ObservableCollection<TiendaAsistenciaDto>>();
            }
            else
            {
                this.IsVisibleStackStores = false;
                this.IsVisibleStackLoad = false;
                this.IsVisibleExistsStores = true;
            }
        }

        private async Task UploadBinnacle()

        {
            var bitacoras = App.DBasistencia.Bitacora.Where(x => !x.Sincronizado).Take(10).ToList();

            var respBitacora = await ApiServices.Post<Response, List<Bitacora>>(ApiUrl.sSaveBinnacle, bitacoras, null);
            if (respBitacora != null)
            {
                if (respBitacora.Success)
                {
                    foreach (var item in bitacoras)
                    {
                        item.Sincronizado = true;
                        App.DBasistencia.Entry(item).State = Services.DataBase.EntryState.Modify;
                        App.DBasistencia.SaveChanges();
                    }
                }
            }
        }

        public async void Upload()
        {
            try
            {
                this.IsVisibleStackStores = false;
                this.IsBusy = true;
                this.IsVisibleStackLoad = true;
                var lst = this.StoreItems.To<List<Asistencia>>();
                await UploadBinnacle();
                var response = await UploadServer(lst);
                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.TiendasOfflineSubidasAServidor, "Se han subido las tiendas con check-in y check-out offline", "UploadOfflinePage");

                MsgResponse(response.Message);
                GetOfflineStores();
            }
            catch (Exception ex)
            {
                this.IsBusy = false;
                this.IsVisibleStackLoad = false;
                this.IsVisibleStackStores = true;
                MsgResponse("En estos momentos no es posible subir la información, intente más tarde.");
                Crashes.TrackError(ex, App.TrackData());
            }
            //await Application.Current.MainPage.Navigation.PopAsync();
        }

        public async void MsgResponse(string arg)
        {
            await Application.Current.MainPage.DisplayAlert("Mi Asistencia", arg, "Aceptar");
        }

        #endregion

        #region Commands
        public ICommand UploadCommand
        {
            get { return new RelayCommand(Upload); }
        }
        #endregion
    }
}
