﻿namespace MiAsistencia.XF.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using LiteConnection.ORM;
    using MiAsistencia.XF.Views.Pages;
    using Services;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;

    public class MainViewModel
    {
        #region Properties
        //public TokenResponse Token { get; set; }
        private NavigationService navigationService;
        public string Title { get; set; }
        public string AppVersion { get; set; }
        public string NombreUsuario { get; set; }
        public string AliasUsuario { get; set; }
        public string ZonaUsuario { get; set; }
        #endregion

        #region ViewModels
        public LoginViewModel Login { get; set; }
        public InicioViewModel Inicios { get; set; }
        public ObservableCollection<MenuItemViewModel> Menu { get; set; }
        public CheckViewModel Check { get; set; }
        public MainCheckViewModel MainCheck { get; set; }
        public UploadOfflineViewModel UploadOffline { get; set; }
        public AboutViewModel About { get; set; }
        public DescargaViewModel Descarga { get; set; }
        public CodeCheckViewModel CodeCheck { get; set; }
        public MainCodeCheckViewModel MainCodeCheck { get; set; }
        public ReceptionViewModel Reception { get; set; }
        public RegistroViewModel Registro { get; set; }
        #endregion

        public MainViewModel()
        {
            instance = this;
            this.AppVersion = "Mi Asistencia " + new Helpers.Version().AppVersion;
            //Login = new LoginViewModel();
            navigationService = new NavigationService();
            //Bitacora.CleanSincronizedBinnacle();
        }

        #region Singleton
        private static MainViewModel instance;
        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            return instance;
        }
        #endregion

        #region Commands
        public ICommand GoToCommand { get { return new RelayCommand<string>(GoTo); } }

        public ICommand CloseSessionCommand
        {
            get { return new RelayCommand(CloseSession); }
        }

        private async void CloseSession()
        {
            var res = await App.Current.MainPage.DisplayAlert("Mi Asistencia", "¿Estás seguro que deseas cerrar sesión en la aplicación?", "No", "Si");

            if (!res)
            {
                Helpers.Settings.IsRemembered = "false";
                MainViewModel.GetInstance().Login = new LoginViewModel();
                App.Current.MainPage = new NavigationPage(new StandarLogin());
            }
        }
        #endregion

        #region Methods

        public async void CloseMessage(string arg)
        {
            await Application.Current.MainPage.DisplayAlert("Mi Asistencia", arg, "Aceptar");
        }

        public void Start(MasterPage p)
        {
            this.Title = "Bienvenido a MiAsistencia";
            LoadMenu();
            navigationService.SetMainPage(p);
        }
        public void GoTo(string pageName)
        {
            if (!string.IsNullOrEmpty(pageName))
            {
                navigationService.Navigate(pageName);
            }
            else
            {
                GetInstance().MainCheck = new MainCheckViewModel();
                navigationService.Navigate("MainCheckPage");
            }
        }

        public void LoadMenu()
        {
            if (App.CurrentUserAsistencia != null)
            {
                this.NombreUsuario = App.CurrentUserAsistencia.Nombre;
                this.AliasUsuario = App.CurrentUserAsistencia.Alias;
            }

            Menu = new ObservableCollection<MenuItemViewModel>();
            List<MenuUser> menus = new List<MenuUser>();
            if (App.CurrentUser != null)
            {
                App.DBasistencia.Execute("DELETE FROM MenuUser");
                App.DBasistencia.SaveChanges();

                var menusUC = App.CurrentUser.menuDtoL.ToList();

                foreach (var men in menusUC)
                {
                    var menu = new LiteConnection.ORM.MenuUser
                    {
                        Icon = men.Icono,
                        PageName = men.Menu,
                        Title = men.Link
                    };
                    Menu.Add(new MenuItemViewModel { menuUser = menu });
                    App.DBasistencia.menuUser.Add(menu);
                }
                App.DBasistencia.SaveChanges();

                var menuCS = new LiteConnection.ORM.MenuUser
                {
                    Icon = "cerrarsesion_logo.png",
                    PageName = "CloseSessionPage",
                    Title = "Cerrar sesión"
                };
                Menu.Add(new MenuItemViewModel { menuUser = menuCS });
                App.DBasistencia.menuUser.Add(menuCS);
                App.DBasistencia.SaveChanges();
            }
            else
            {
                menus = App.DBasistencia.menuUser.ToList();
                if (menus != null && menus.Count > 0)
                {
                    foreach (var men in menus)
                    {
                        var menu = new LiteConnection.ORM.MenuUser
                        {
                            Icon = men.Icon,
                            PageName = men.PageName,
                            Title = men.Title
                        };
                        Menu.Add(new MenuItemViewModel { menuUser = menu });
                    }
                }
                else
                {
                    var menuCS = new LiteConnection.ORM.MenuUser
                    {
                        Icon = "cerrarsesion_logo.png",
                        PageName = "CloseSessionPage",
                        Title = "Cerrar sesión"
                    };
                    Menu.Add(new MenuItemViewModel { menuUser = menuCS });
                }
            }
        }
        #endregion
    }
}
