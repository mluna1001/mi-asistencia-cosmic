﻿using Cosmic;
using MiAsistencia.XF.Common;
using MiAsistencia.XF.Helpers;
using MiAsistencia.XF.Interfaces;
using MiAsistencia.XF.LiteConnection.ORM;
using MiAsistencia.XF.Services.API;
using MiAsistencia.XF.ViewModels;
using MiAsistencia.XF.Views;
using MiAsistencia.XF.Views.Pages;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Plugin.DeviceInfo;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MiAsistencia.XF
{
    public partial class App : Application
    {
        public static AsistenciaDB DBasistencia { get; set; }

        private static Dictionary<string, ContentPage> paginas { get; set; }
        public static UserAsistencia CurrentUserAsistencia { get; set; }
        public static UsuarioCosmicDto CurrentUser { get; set; }
        public static NavigationPage Navigator { get; internal set; }
        public static MasterPage Master { get; internal set; }
        public static int? CurrentIdTienda { get; set; }
        public static float ScreenWidth { get; set; }
        public static float ScreenHeight { get; set; }
        public static string DeviceIdentifier { get; set; }
        public static string ListInstallApps { get; set; }
        public static bool OnOffFences { get; set; } = false;

        public static IDictionary<string, string> TrackData()
        {
            var dic = new Dictionary<string, string>();
            if (CurrentUser != null)
                dic.Add("usuario", CurrentUser.usuario.ToJson().LimitString(150));
            else
                dic.Add("usuario", "No tiene login");
            return dic;
        }

        public App()
        {
            InitializeComponent();

            DeviceIdentifier = DependencyService.Get<IDevice>().GetIdentifier();
            ListInstallApps = DependencyService.Get<IDevice>().GetInstalledApps().ToJson();

            paginas = new Dictionary<string, ContentPage>();
            DBasistencia = AsistenciaDB.GetInstance(); 
            ApiUrl.SetUrls();
            MainViewModel.GetInstance().Login = new LoginViewModel();
            Bitacora.CleanSincronizedBinnacle();

            if (!Settings.IsRemembered.Equals("true"))
            {
                MainViewModel.GetInstance().Login = new LoginViewModel();
                MainPage = new NavigationPage(new StandarLogin());
            }
            else
            {
                App.CurrentUserAsistencia = DBasistencia.userAsistencia.FirstOrDefault(u => u.Alias.Equals(Settings.Alias));
                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.IniciarSesionRecordado, "Inició sesión con usuario recordado en dispositivo", "MasterPage");
                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.RegistroIMEIDispositivo, App.DeviceIdentifier, "LoginPage");
                Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.RegistroAppsIsntaladas, App.ListInstallApps, "LoginPage");
                var mvm = MainViewModel.GetInstance();
                mvm.Inicios = new InicioViewModel();
                mvm.LoadMenu();

                Current.MainPage = new MasterPage() { Title = "Bienvenido a Mi Asistencia" };
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            AppCenter.Start("android=7947873d-01e2-4fba-aee2-2d596cee6baf;" +
                  "uwp=c52359af-2948-4d4e-8834-9df7f83ab0a5;" +
                  "ios=b388deb1-44a7-4a90-af90-ebe5ef4abbf5",
                  typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
