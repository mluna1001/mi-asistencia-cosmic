﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MiAsistencia.XF.Views.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StandarLogin : ContentPage
	{
		public StandarLogin ()
		{
			InitializeComponent ();
            mainStack.BackgroundColor = new Color(0, 0, 0, 0.5);
		}
	}
}