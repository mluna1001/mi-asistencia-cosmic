﻿using MiAsistencia.XF.Interfaces;
using MiAsistencia.XF.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

[assembly: Xamarin.Forms.Dependency(typeof(MiAsistencia.XF.UWP.Implementation.ReadFile))]
namespace MiAsistencia.XF.UWP.Implementation
{
    public class ReadFile : IReadFile
    {
        public RespaldoBDUsuarioDto FileRead()
        {
            var result = new RespaldoBDUsuarioDto();

            var folder = ApplicationData.Current.LocalFolder.Path;
            var path = Path.Combine(folder, "uwm_mysys.db3");

            result.BasedeDatos = File.ReadAllBytes(path);
            result.Nombre = "uwm_mysysBack";

            return result;
        }
    }
}
