﻿using MiAsistencia.XF.Interfaces;
using MiAsistencia.XF.LiteConnection.ORM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

[assembly:Xamarin.Forms.Dependency(typeof(MiAsistencia.XF.UWP.Implementation.PathService))]
namespace MiAsistencia.XF.UWP.Implementation
{
    public class PathService : IGetDBService
    {
        public MovilResponse GetDatabase()
        {
            var path = Path.Combine(ApplicationData.Current.LocalFolder.Path, "uwm_mysys.db3");
            System.Diagnostics.Debug.WriteLine(path);
            var r = new MovilResponse { Status = false, Message = "Base no encontrada " + path };
            if (File.Exists(path))
            {
                r.Objeto = new AsistenciaDB(path);
                r.Status = true;
            }
            else
            {
                r.Objeto = new AsistenciaDB(path);
                r.Status = true;
            }
            return r;
        }
    }
}
