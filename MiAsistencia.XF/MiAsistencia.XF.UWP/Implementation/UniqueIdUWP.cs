﻿using MiAsistencia.XF.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(MiAsistencia.XF.UWP.Implementation.UniqueIdUWP))]
namespace MiAsistencia.XF.UWP.Implementation
{
    public class UniqueIdUWP : IDevice
    {
        public string GetIdentifier()
        {
            return "UWP_PSM_2019";
        }

        public List<Tuple<string, string>> GetInstalledApps()
        {
            return new List<Tuple<string, string>>();
        }
    }
}
