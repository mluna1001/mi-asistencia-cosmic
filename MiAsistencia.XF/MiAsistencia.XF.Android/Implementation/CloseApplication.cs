﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MiAsistencia.XF;
using MiAsistencia.XF.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(MiAsistencia.XF.Droid.Implementation.CloseApplication))]
namespace MiAsistencia.XF.Droid.Implementation
{
    public class CloseApplication : ICloseApplication
    {
        internal static Context Context;

        public void CloseApp()
        {
            var activity = (Activity)Context;
            activity.FinishAffinity();
        }
    }
}