﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Location;
using Android.Gms.Tasks;
using Android.Locations;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MiAsistencia.XF.Droid;
using MiAsistencia.XF.Interfaces;
using MiAsistencia.XF.LiteConnection.ORM;

[assembly: Xamarin.Forms.Dependency(typeof(MiAsistencia.XF.Droid.Implementation.Geo.GeocercaService))]
namespace MiAsistencia.XF.Droid.Implementation.Geo
{
    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted })]
    public class GeocercaService : BroadcastReceiver, IOnCompleteListener, IGeocerca
    {
        public override void OnReceive(Context context, Intent intent)
        {

        }

        private static GeofencingClient mGeofencingClient;
        //public static IList<IGeofence> mGeofenceList;
        private static PendingIntent mGeofencePendingIntent;

        public GeocercaService()
        {
            instance = this;
        }

        internal static void Initialize(Context context, Activity activity)
        {
            Context = context;
            Activity = activity;

            mGeofencePendingIntent = null;
            mGeofencingClient = LocationServices.GetGeofencingClient(activity);
        }
        static Context Context;
        static Activity Activity;

        private static GeocercaService instance;
        public static GeocercaService GetInstance()
        {
            if (instance == null)
                return new GeocercaService();
            return instance;
        }

        public void Dispose()
        {

        }

        public void OnComplete(Task task)
        {

        }

        #region MyRegion
        public void IniciaGeofences()
        {
            mGeofencingClient.AddGeofences(GetGeofencingRequest(GeoConstants.GEOFENCE_LIST), GetGeofencePendingIntent()).AddOnCompleteListener(this);
        }
        public void AddGeofences(List<TiendaAsistencia> tiendaAsistencias, EnergyConfig energyConfig)
        {
            IList<IGeofence> mGeofenceList = new List<IGeofence>();
            foreach (var entry in tiendaAsistencias)
            {
                entry.Radio = entry.Radio < 49 ? 50 : entry.Radio;
                string stienda = $"{entry.NombreTienda} | {entry.IdTienda}";
                mGeofenceList.Add(new GeofenceBuilder()
                    .SetRequestId(stienda)
                    .SetCircularRegion(
                        (double)entry.Latitud,
                        (double)entry.Longitud,
                        entry.Radio
                    )
                    .SetExpirationDuration(GeoConstants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                    .SetTransitionTypes(Geofence.GeofenceTransitionEnter | 
                                        Geofence.GeofenceTransitionDwell |
                                        Geofence.GeofenceTransitionExit)
                    .SetLoiteringDelay(1)
                    .SetNotificationResponsiveness(1 * 60 * 1000)   // min * sec * mili
                    .Build());
            }
            GeoConstants.GEOFENCE_LIST = mGeofenceList;

            mGeofencingClient.AddGeofences(
                                GetGeofencingRequest(mGeofenceList),
                                GetGeofencePendingIntent())
                                    //.AddOnCompleteListener(this);
                                    .AddOnCompleteListener(this);
        }
        public void RemoveGeofences()
        {
            mGeofencingClient.RemoveGeofences(GetGeofencePendingIntent()).AddOnCompleteListener(this);
        }
        public void RemoveGeofences(string idTienda)
        {
            mGeofencingClient.RemoveGeofences(GetGeofencePendingIntent()).AddOnCompleteListener(this);
        }
        private GeofencingRequest GetGeofencingRequest(IList<IGeofence> tiendaAsistencias)
        {
            GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
            builder.SetInitialTrigger(GeofencingRequest.InitialTriggerEnter | GeofencingRequest.InitialTriggerDwell);
            builder.AddGeofences(tiendaAsistencias);
            return builder.Build();
        }
        private PendingIntent GetGeofencePendingIntent()
        {
            if (mGeofencePendingIntent != null)
            {
                return mGeofencePendingIntent;
            }
            var intent = new Intent(Context, typeof(GeofenceTransitionsIntentService));
            return PendingIntent.GetService(Context, 0, intent, PendingIntentFlags.UpdateCurrent);
        }
        
        private bool GetGeofencesAdded()
        {
            return PreferenceManager.GetDefaultSharedPreferences(Context).GetBoolean(GeoConstants.GEOFENCES_ADDED_KEY, false);
        }

        private void UpdateGeofencesAdded(bool added)
        {
            PreferenceManager.GetDefaultSharedPreferences(Context)
                .Edit()
                .PutBoolean(GeoConstants.GEOFENCES_ADDED_KEY, added)
                .Apply();
        }
        
        #endregion

    }

    public class OnRequestPermissionsResultClickListener : Java.Lang.Object, View.IOnClickListener
    {
        public Activity Activity { get; set; }
        public void OnClick(View v)
        {
            Intent intent = new Intent();
            intent.SetAction(Android.Provider.Settings.ActionApplicationDetailsSettings);
            var uri = Android.Net.Uri.FromParts("package", Activity.PackageName, null);
            intent.SetData(uri);
            intent.SetFlags(ActivityFlags.NewTask);
            Activity.StartActivity(intent);
        }
    }
}
