﻿using Android.App;
using Android.Gms.Location;
using Android.Util;
using Android.Content;
using System.Collections.Generic;
using Android.Support.V4.App;
using Android.Graphics;
using MiAsistencia.XF.Droid.Implementation.Geo;
using System;
using Android.Media;

namespace MiAsistencia.XF.Droid
{
    [Service]
    public class GeofenceTransitionsIntentService : IntentService
    {
        private const string TAG = "GeofenceTransitionsIS";

        public GeofenceTransitionsIntentService() : base(TAG)
        {
        }

        protected override void OnHandleIntent(Intent intent)
        {
            var geofencingEvent = GeofencingEvent.FromIntent(intent);
            var fechaActual = DateTime.Now.Date;
            
            if (geofencingEvent.HasError)
            {
                var errorMessage = GeofenceErrorMessages.GetErrorString(this, geofencingEvent.ErrorCode);
                Log.Error(TAG, errorMessage);
                return;
            }
            
            int geofenceTransition = geofencingEvent.GeofenceTransition;

            if (geofenceTransition == Geofence.GeofenceTransitionEnter ||
                geofenceTransition == Geofence.GeofenceTransitionDwell ||
                geofenceTransition == Geofence.GeofenceTransitionExit)
            {
                IList<IGeofence> triggeringGeofences = geofencingEvent.TriggeringGeofences;
                
                string geofenceTransitionDetails = GetGeofenceTransitionDetails(
                    this, 
                    geofenceTransition, 
                    triggeringGeofences);
                try
                {
                    int tienda = int.Parse(geofenceTransitionDetails.Split('|')[1]);
                    LiteConnection.ORM.Asistencia a = null;

                    switch (geofenceTransition)
                    {
                        case Geofence.GeofenceTransitionEnter:
                        case Geofence.GeofenceTransitionDwell:
                            a = App.DBasistencia.Asistencia
                                .FirstOrDefault(s => s.IdTienda == tienda 
                                && s.Fecha >= fechaActual
                                && s.Estatus);
                            break;
                        case Geofence.GeofenceTransitionExit:
                            a = App.DBasistencia.Asistencia
                                .FirstOrDefault(s => s.IdTienda == tienda 
                                && s.Fecha >= fechaActual
                                && s.Estatus);
                            break;
                    }

                    if (a != null)
                    {
                        var geofence = GeocercaService.GetInstance();
                        geofence.RemoveGeofences();
                        return;
                    }
                }
                catch (System.Exception ex)
                {
                    
                }

                SendNotification(geofenceTransitionDetails, geofenceTransition);
                Log.Info(TAG, geofenceTransitionDetails);                
            }
            else
            {
                // Log the error.
                Log.Error(TAG, GetString(Resource.String.geofence_transition_invalid_type, new[] { new Java.Lang.Integer(geofenceTransition) }));
            }
        }

        string GetGeofenceTransitionDetails(Context context, int geofenceTransition, IList<IGeofence> triggeringGeofences)
        {
            string geofenceTransitionString = GetTransitionString(geofenceTransition);

            var triggeringGeofencesIdsList = new List<string>();
            foreach (IGeofence geofence in triggeringGeofences)
            {
                triggeringGeofencesIdsList.Add(geofence.RequestId);
            }
            var triggeringGeofencesIdsString = string.Join(", ", triggeringGeofencesIdsList);

            return geofenceTransitionString + ": " +
                    triggeringGeofencesIdsString;
        }

        void SendNotification(string ContentTitle, int geofenceTransition)
        {
            var notificationIntent = new Intent(ApplicationContext, typeof(MainActivity));

            var stackBuilder = Android.Support.V4.App.TaskStackBuilder.Create(this);
            stackBuilder.AddParentStack(Java.Lang.Class.FromType(typeof(MainActivity)));
            stackBuilder.AddNextIntent(notificationIntent);

            var notificationPendingIntent = stackBuilder.GetPendingIntent(0, (int)PendingIntentFlags.UpdateCurrent);

            var msg = (geofenceTransition == Geofence.GeofenceTransitionExit ?
                                                    "No olvides realizar tu salida" :
                                                    "Recuerda marcar tu entrada");
            string contentText = $"{msg}. {GetString(Resource.String.geofence_transition_notification_text)}";
            Color color;
            switch (geofenceTransition)
            {
                case Geofence.GeofenceTransitionEnter:
                case Geofence.GeofenceTransitionDwell:
                    color = Color.Green;
                    break;
                case Geofence.GeofenceTransitionExit:
                    color = Color.Orange;
                    break;
                default: color = Color.Red; break;
            }
            var builder = new NotificationCompat.Builder(this, "M_CH_ID");
            builder.SetSmallIcon(Resource.Drawable.mias_logox36)
                .SetLargeIcon(BitmapFactory.DecodeResource(Resources, Resource.Drawable.mias_logox36))
                .SetColor(color)
                .SetContentTitle(ContentTitle)
                .SetContentText(contentText)
                .SetContentIntent(notificationPendingIntent)
                //.SetPriority(Notification.PRIORITY_MAX)
                .SetPriority(2)
                .SetDefaults((int)NotificationDefaults.Sound | (int)NotificationDefaults.Vibrate);
            builder.SetSound(RingtoneManager.GetDefaultUri(RingtoneType.Alarm));
            builder.SetAutoCancel(true);
            //builder.SetCategory

            var mNotificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            mNotificationManager.Notify(0, builder.Build());
        }

        string GetTransitionString(int transitionType)
        {
            switch (transitionType)
            {
                case Geofence.GeofenceTransitionEnter:
                    return GetString(Resource.String.geofence_transition_entered);
                case Geofence.GeofenceTransitionDwell:
                    return GetString(Resource.String.geofence_transition_entered);
                case Geofence.GeofenceTransitionExit:
                    return GetString(Resource.String.geofence_transition_exited);
                default:
                    return GetString(Resource.String.unknown_geofence_transition);
            }
        }
    }
}
