﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MiAsistencia.XF.Interfaces;
using MiAsistencia.XF.Models;

[assembly: Xamarin.Forms.Dependency(typeof(MiAsistencia.XF.Droid.Implementation.ReadFile))]
namespace MiAsistencia.XF.Droid.Implementation
{
    public class ReadFile : IReadFile
    {
        public RespaldoBDUsuarioDto FileRead()
        {
            var result = new RespaldoBDUsuarioDto();

            var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "android_mysys.db3");

            result.BasedeDatos = File.ReadAllBytes(path);
            result.Nombre = "android_mysysBack";

            return result;
        }
    }
}