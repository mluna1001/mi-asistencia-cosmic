﻿using System;
using Android.App;
using Android.Content.PM;
using Android.Telephony;
using MiAsistencia.XF.Droid.Implementation;
using MiAsistencia.XF.Interfaces;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

[assembly: Xamarin.Forms.Dependency(typeof(UniqueIdAndroid))]
namespace MiAsistencia.XF.Droid.Implementation
{
    public class UniqueIdAndroid : IDevice
    {
        public string GetIdentifier()
        {
            string IMEI = string.Empty;
            try
            {
                var telephonyManager = (TelephonyManager)Android.App.Application.Context.GetSystemService(Android.Content.Context.TelephonyService);
                IMEI = telephonyManager.DeviceId;
            }
            catch (Exception e)
            {
                IMEI = e.ToString();
            }

            return IMEI;
        }

        public List<Tuple<string, string>> GetInstalledApps()
        {
            var apps = Application.Context.PackageManager.GetInstalledApplications(PackageInfoFlags.MatchAll).ToList();
            var l = apps
                .Where(s => !AppsNames.Any(d => d.StartsWith(s.ClassName ?? "")))
                .Where(s => !AppsNames.Any(d => d.StartsWith(s.ProcessName ?? "")))
                .Select(s => new Tuple<string, string>(s.ClassName, s.ProcessName))
                .ToList();
            return l;
        }

        static List<string> AppsNames2 = new List<string>
        {
            "com.android",
            "com.lge",
            "com.google",
            "org.telegram",
            "com.microsoft",
            "com.qualcomm",
            "android.process.media",
            "com.android.vending",

            "com.android.systemui",
            "com.android.phone",
            "com.lge.theme.superbatterysaving",
            "com.hy.system.fontserver",
            "com.android.LGSetupWizard",
            "com.android.cts.priv.ctsshim",
            "com.google.android.apps.youtube.app.YouTubeApplication",
            "com.google.android.youtube",
            "com.google.android.ext.services",
            "com.microsoft.office.sfb.SfBApp",
            "com.microsoft.office.lync15",
            "com.lge.sizechangable.weather.platform.application.WeatherServiceApplication",
            "com.lge.sizechangable.weather.platform",
            "com.google.android.apps.gsa.binaries.velvet.app.VelvetApplication",
            "com.google.android.googlequicksearchbox",
            "com.lge.theme.black",
            "com.lge.theme.titan",
            "com.lge.theme.white",
            "com.android.providers.calendar",
            "org.telegram.messenger.ApplicationLoader","org.telegram.messenger",
            "com.google.android.apps.docs.editors.kix.configurations.kixwithchangeling.stable.KixWithChangelingStableApplication",
            "com.lge.systemservice.service.LGSystemServerApplication"
,"com.google.android.onetimeinitializer"
,"com.google.android.ext.shared"
,"com.android.wallpapercropper"
,"com.lge.appbox.client.AppBoxApplication"
,"com.qualcomm.qti.autoregistration"
,"com.lge.lgdmsclient"
,"com.android.documentsui"
,"com.android.externalstorage"
,"com.lge.atservice"
,"com.android.htmlviewer"
,"com.lge.app.floating.res"
,".dataservices"
            ,"com.android.companiondevicemanager"
,"com.google.android.apps.docs.editors.sheets"
,"com.lge.sui.widget"
            ,"com.google.android.apps.docs.editors.slides"
,"android.process.media"
            ,"com.google.android.apps.messaging.release.BugleReleaseApplication"
,"com.lge.camera"
,"com.lge.touchcontrol"
,"com.lge.effect"
,"com.lge.eltest"
,"com.lge.LGSetupView"
,"com.lge.homeselector"
, "com.lge.ime.solution.text"
, "com.lge.springcleaning"
, "com.lge.gnsstest"
,"com.lge.fmradio"
,"com.google.android.configupdater"
,"com.android.defcontainer"
        };
        static List<string> AppsNames = new List<string>
        {
            "com.android.phone",
            "com.lge.theme.superbatterysaving",
            "com.hy.system.fontserver",
            "com.android.LGSetupWizard",
            "com.android.cts.priv.ctsshim",
            "com.google.android.apps.youtube.app.YouTubeApplication",
            "com.google.android.youtube",
            "com.google.android.ext.services",
            "com.microsoft.office.sfb.SfBApp",
            "com.microsoft.office.lync15",
            "com.lge.sizechangable.weather.platform.application.WeatherServiceApplication",
            "com.lge.sizechangable.weather.platform",
            "com.google.android.apps.gsa.binaries.velvet.app.VelvetApplication",
            "com.google.android.googlequicksearchbox",
            "com.lge.theme.black",
            "com.lge.theme.titan",
            "com.lge.theme.white",
            "com.android.providers.calendar",
            "org.telegram.messenger.ApplicationLoader","org.telegram.messenger",
            "com.google.android.apps.docs.editors.kix.configurations.kixwithchangeling.stable.KixWithChangelingStableApplication",
            "com.lge.systemservice.service.LGSystemServerApplication"
,"com.google.android.onetimeinitializer"
,"com.google.android.ext.shared"
,"com.android.wallpapercropper"
,"com.lge.appbox.client.AppBoxApplication"
,"com.qualcomm.qti.autoregistration"
,"com.lge.lgdmsclient"
,"com.android.documentsui"
,"com.android.externalstorage"
,"com.lge.atservice"
,"com.android.htmlviewer"
,"com.lge.app.floating.res"
,".dataservices"
            ,"com.android.companiondevicemanager"
,"com.google.android.apps.docs.editors.sheets"
,"com.lge.sui.widget"
            ,"com.google.android.apps.docs.editors.slides"
,"android.process.media"
            ,"com.google.android.apps.messaging.release.BugleReleaseApplication"
,"com.lge.camera"
,"com.lge.touchcontrol"
,"com.lge.effect"
,"com.lge.eltest"
,"com.lge.LGSetupView"
,"com.lge.homeselector"
, "com.lge.ime.solution.text"
, "com.lge.springcleaning"
, "com.lge.gnsstest"
,"com.lge.fmradio"
,"com.google.android.configupdater"
,"com.android.defcontainer"

        };
    }
}
