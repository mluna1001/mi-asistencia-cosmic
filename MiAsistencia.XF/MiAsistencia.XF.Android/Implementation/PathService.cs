﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MiAsistencia.XF.Interfaces;
using MiAsistencia.XF.LiteConnection.ORM;

[assembly:Xamarin.Forms.Dependency(typeof(MiAsistencia.XF.Droid.Implementation.PathService))]
namespace MiAsistencia.XF.Droid.Implementation
{
    public class PathService : IGetDBService
    {
        public MovilResponse GetDatabase()
        {
            string ipath = string.Empty;

#if DEBUG
            ipath = Android.OS.Environment.ExternalStorageDirectory.Path;
#else
            ipath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
#endif
            string path = Path.Combine(ipath, "android_mysys.db3");
            var r = new MovilResponse { Status = false, Message = "Base no encontrada " + path };
            if (File.Exists(path))
            {
                r.Objeto = new AsistenciaDB(path);
                r.Status = true;
            }
            else
            {
                r.Objeto = new AsistenciaDB(path);
                r.Status = true;
            }
            return r;
        }
    }
}