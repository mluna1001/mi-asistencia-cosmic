﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using MiAsistencia.XF.Droid.Implementation;
using ZXing.Mobile;
using Android;
using System.Threading.Tasks;
using Android.Content;
using Java.Lang;
//using Plugin.Geofence;
//using MiAsistencia.XF.Helpers.GeoHelp;
using Android.Support.V4.App;
using Android.Media;
using Microsoft.AppCenter.Push;
//using Plugin.PushNotification;
using MiAsistencia.XF.Droid.Implementation.Geo;
using System.Linq;

namespace MiAsistencia.XF.Droid
{
    [Activity(Label = "MiAsistencia", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        Task<bool> statuspermisos;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            statuspermisos = TryGetPermission();
            //if (statuspermisos.Status != TaskStatus.RanToCompletion)
            //{
            //    CheckTime.Context = this;
            //    CloseApplication.Context = this;
            //    LoadApplication(new App());
            //}
            //statuspermisos.Wait(3600);

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            CheckTime.Context = this;
            CloseApplication.Context = this;

            //-->Inicia el  scanner
            MobileBarcodeScanner.Initialize(this.Application);

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            if (statuspermisos.Result)
            {
                if (!this.IsFinishing)
                {
                    //show dialog
                    //ShowDialog(1);
                }

                LoadApplication(new App());
                if (App.OnOffFences)
                {
                    GeocercaService.Initialize(this, this);
                    var tiendas = App.DBasistencia.TiendaAsistencia.ToList();
                    if (tiendas.Any())
                    {
                        try
                        {
                            var geofence = GeocercaService.GetInstance();   // DependencyService.Get<IGeocerca>();
                            geofence.RemoveGeofences();
                            geofence.AddGeofences(tiendas, Interfaces.EnergyConfig.Medium);
                        }
                        catch (System.Exception ex)
                        {
                            
                        }
                    }
                }
            }

            if ((int)Build.VERSION.SdkInt < 23)
            {
                LoadApplication(new App());
            }
        }
        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            //PushNotificationManager.ProcessIntent(this, intent);
        }

        async Task<bool> TryGetPermission()
        {
            if ((int)Build.VERSION.SdkInt >= 23)
            {
                return await GetPermissionAsync();
            }
            return false;
        }

        const int RequestLocationId = 0;

        readonly string[] PermissionGroupLocation =
        {
            Manifest.Permission.WriteExternalStorage,
            Manifest.Permission.SystemAlertWindow,
            Manifest.Permission.AccessFineLocation,
            Manifest.Permission.Camera,
            Manifest.Permission.Internet,
            Manifest.Permission.ReadPhoneState, 
            Manifest.Permission.AccessCoarseLocation
        };

        async Task<bool> GetPermissionAsync()
        {
            var status = false;
            const string permission = Manifest.Permission.WriteExternalStorage;
            const string ubicationPermission = Manifest.Permission.AccessFineLocation;
            const string cameraPermission = Manifest.Permission.Camera;
            const string readPhoneStatePermission = Manifest.Permission.ReadPhoneState;
            const string internetStatePermission = Manifest.Permission.Internet;

            if ((CheckSelfPermission(permission) == (int)Android.Content.PM.Permission.Granted) && 
                (CheckSelfPermission(ubicationPermission) == (int)Android.Content.PM.Permission.Granted) &&
                (CheckSelfPermission(cameraPermission) == (int)Android.Content.PM.Permission.Granted) &&
                (CheckSelfPermission(readPhoneStatePermission) == (int)Android.Content.PM.Permission.Granted) &&
                (CheckSelfPermission(internetStatePermission) == (int)Android.Content.PM.Permission.Granted)
                )
            {
                //Toast.MakeText(this, "Permisos especiales concedidos", ToastLength.Short).Show();
                //LoadApplication(new App());
                return true;
            }

            if (ShouldShowRequestPermissionRationale(permission) &&
                ShouldShowRequestPermissionRationale(ubicationPermission) &&
                ShouldShowRequestPermissionRationale(cameraPermission) &&
                ShouldShowRequestPermissionRationale(readPhoneStatePermission) &&
                (CheckSelfPermission(internetStatePermission) == (int)Android.Content.PM.Permission.Granted)
                )
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("Permisos");
                alert.SetMessage("Mi Asistencia requiere permisos para continuar");
                alert.SetPositiveButton("Conceder permisos", (senderAlert, args) =>
                {
                    RequestPermissions(PermissionGroupLocation, RequestLocationId);
                    status = true;
                });
                alert.SetNegativeButton("Cancelar", (senderAlert, args) =>
                {
                    Toast.MakeText(this, "Permisos denegados", ToastLength.Short).Show();
                    status = false;
                });

                Dialog dialog = alert.Create();
                dialog.Show();

                return status;
            }
            RequestPermissions(PermissionGroupLocation, RequestLocationId);
            return false;
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            //Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            //base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            switch (requestCode)
            {
                case RequestLocationId:
                    if (grantResults[0] == (int)Android.Content.PM.Permission.Granted)
                    {
                        //Toast.MakeText(this, "Permisos especiales concedidos", ToastLength.Short).Show();
                        if (!this.IsFinishing)
                        {
                            //show dialog
                            //ShowDialog(1);
                        }
                        LoadApplication(new App());
                        if (App.OnOffFences)
                        {
                            GeocercaService.Initialize(this, this);
                            var tiendas = App.DBasistencia.TiendaAsistencia.ToList();
                            if (tiendas.Any())
                            {
                                try
                                {
                                    var geofence = GeocercaService.GetInstance();   // DependencyService.Get<IGeocerca>();
                                    geofence.RemoveGeofences();
                                    geofence.AddGeofences(tiendas, Interfaces.EnergyConfig.Medium);
                                }
                                catch (System.Exception ex)
                                {

                                }
                            }
                        }
                    }
                    else
                    {
                        Toast.MakeText(this, "Permisos especiales denegados", ToastLength.Short).Show();
                        AlertDialog.Builder alert = new AlertDialog.Builder(this);
                        alert.SetTitle("Permisos");
                        alert.SetMessage("Mi Asistencia requiere permisos para continuar, al ser denegados la aplicación se cerrará.");
                        alert.SetPositiveButton("Salir", (senderAlert, args) =>
                        {
                            CloseApplication var = new CloseApplication();
                            var.CloseApp();
                        });

                        Dialog dialog = alert.Create();
                        dialog.Show();
                    }
                    break;
                default:
                    break;
            }
        }
        
    }
}
