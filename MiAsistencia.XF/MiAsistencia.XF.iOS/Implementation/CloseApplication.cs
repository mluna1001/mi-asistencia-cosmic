﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using MiAsistencia.XF.Interfaces;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(MiAsistencia.XF.iOS.Implementation.CheckTime))]
namespace MiAsistencia.XF.iOS.Implementation
{
    public class CloseApplication : ICloseApplication
    {
        public void CloseApp()
        {
            System.Diagnostics.Process.GetCurrentProcess().CloseMainWindow();
        }
    }
}