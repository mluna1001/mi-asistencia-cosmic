﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foundation;
using MiAsistencia.XF.Interfaces;
using UIKit;
using ZXing.Mobile;

[assembly: Xamarin.Forms.Dependency(typeof(MiAsistencia.XF.iOS.Implementation.QrCodeScanningService))]
namespace MiAsistencia.XF.iOS.Implementation
{
    public class QrCodeScanningService : IQrCodeScanningService
    {
        public async Task<string> ScanAsync()
        {
            var scanner = new MobileBarcodeScanner();
            var scanResults = await scanner.Scan();
            return (scanResults != null) ? scanResults.Text : string.Empty;
        }
    }
}