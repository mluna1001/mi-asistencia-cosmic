﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Foundation;
using MiAsistencia.XF.Interfaces;
using MiAsistencia.XF.Models;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(MiAsistencia.XF.iOS.Implementation.ReadFile))]
namespace MiAsistencia.XF.iOS.Implementation
{
    public class ReadFile : IReadFile
    {
        public RespaldoBDUsuarioDto FileRead()
        {
            var result = new RespaldoBDUsuarioDto();

            string personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, "ios_mysys.db3");

            result.BasedeDatos = File.ReadAllBytes(path);
            result.Nombre = "ios_mysysBack";

            return result;
        }
    }
}