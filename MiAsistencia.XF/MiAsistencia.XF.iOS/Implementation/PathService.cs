﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Foundation;
using MiAsistencia.XF.Interfaces;
using MiAsistencia.XF.LiteConnection.ORM;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(MiAsistencia.XF.iOS.Implementation.PathService))]
namespace MiAsistencia.XF.iOS.Implementation
{
    public class PathService : IGetDBService
    {
        public MovilResponse GetDatabase()
        {
            string personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, "ios_mysys.db3");

            MovilResponse cosmicResponse = new MovilResponse
            {
                Status = false,
                Message = "Existe"
            };

            if (File.Exists(path))
            {
                var psmdatabase = new AsistenciaDB(path);
                cosmicResponse.Objeto = psmdatabase;
                cosmicResponse.Status = true;
            }
            else
            {
                var psmdatabase = new AsistenciaDB(path);
                cosmicResponse.Objeto = psmdatabase;
            }

            return cosmicResponse;
        }
    }
}