﻿using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ExceptionHandling;

namespace MiAsistencia.API.Tools
{
    public class UnhandledExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            //var log = context.Exception.ToString();
            Exception ex = context.Exception;
            Qry_Log.InsertLog(ex, null, null, "UnhandledExceptionLogger");
            Cosmic.Response response = new Cosmic.Response
            {
                excepcion = context.Exception,
                Success = false,
                Message = context.Exception.Message
            };
        }
    }
}