﻿using Cosmic;
using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Querys;
using System.Collections.Generic;
using System.Web.Http;

namespace MiAsistencia.API.Controllers
{
    public class AsistenciaController : ApiController
    {
        [Route("~/Asistencia/SaveCheck")]
        [HttpPost]
        //public IHttpActionResult SaveCheck(MiAsistencia.Comun.Models.DTO.Asistencia objAsistencia)
        public IHttpActionResult SaveCheck(Asistencia objAsistencia)
        {
            var statusCheck = Qry_Asistencia.SaveCheck(objAsistencia.To<Asistencia>());
            return Ok(statusCheck);
        }

        [Route("~/Asistencia/SaveAllChecks")]
        [HttpPost]
        public IHttpActionResult SaveAllChecks(List<Asistencia> Asistencia)
        {
            var statusCheck = Qry_Asistencia.SaveAnyCheck(Asistencia);
            return Ok(statusCheck);
        }
    }
}
