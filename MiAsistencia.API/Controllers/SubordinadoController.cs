﻿using Cosmic;
using MiAsistencia.API.Models.Extensions;
using MiAsistencia.Comun.Models.ORM;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MiAsistencia.API.Controllers
{
    public class SubordinadoController : ApiController
    {
        private string uri = ConfigurationManager.AppSettings["UrlUsuarios"];

        [HttpPost]
        public IHttpActionResult GetSubordinate(SendDataDownloadUser usuario)
        {
            try
            {
                string endp = "userapi/GetSubordinateUsers";
                var r = SrvRest.CallRestParam(uri, endp, usuario.IdUsuario);
                var userslistCosmic = JsonConvert.DeserializeObject<List<Subordinado>>(r.Datos.ToString());

                Cosmic.Response response = new Response
                {
                    Success = userslistCosmic != null ? true : false,
                    Data = userslistCosmic
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                return Ok(new { Exception = ex });
            }
        }
    }
}
