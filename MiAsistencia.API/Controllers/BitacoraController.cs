﻿using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MiAsistencia.API.Controllers
{
    public class BitacoraController : ApiController
    {
        [Route("~/Bitacora/SaveBinnacle")]
        [HttpPost]
        public IHttpActionResult SaveBinnacle(List<Bitacora> Asistencia)
        {
            var statusCheck = Qry_Bitacora.SaveBinnacle(Asistencia);
            return Ok(statusCheck);
        }
    }
}
