﻿using MiAsistencia.Comun.Models.ORM;
using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MiAsistencia.API.Controllers
{
    public class TiendaController : ApiController
    {
        [Route("~/Tienda/GetStores")]
        [HttpPost]
        public IHttpActionResult GetStores(SendDataStore sendData)
        {
            var response = Qry_Tienda.GetTiendas(sendData.IdUsuario, sendData.Latitud, sendData.Longitud);
            return Ok(response);
        }

        [Route("~/Tienda/GetAsignedStores")]
        [HttpPost]
        public IHttpActionResult GetAsignedStores(SendDataStore sendData)
        {
            var response = Qry_Tienda.GetAllTiendas(sendData.IdUsuario);
            return Ok(response);
        }
    }
}
