﻿using MiAsistencia.Comun.Models.ORM;
using MiAsistencia.Dominio.Models.Dto;
using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MiAsistencia.API.Controllers
{
    [Route("miasisapi")]
    public class UsuarioController : ApiController
    {
        [HttpPost]
        [Route("miasisapi/getobjusuario")]
        public IHttpActionResult GetObjectUsuario(SendDataDownloadUser usuario)
        {
            UsuarioDto user = Qry_Usuario.GetUsuario(usuario.IdUsuario);
            BouncyCastleHashing bouncyCastleHashing = new BouncyCastleHashing();
            byte[] hashBytes = bouncyCastleHashing.PBKDF2_SHA256_GetHash(usuario.Contrasena, new byte[16], 10000, 20);

            return Ok(new
            {
                usuario.IdUsuario,
                user.UsuarioProyecto.IdProyecto,
                user.UsuarioRegion.IdRegion,
                user.UsuarioCliente.IdCliente,
                Contrasena = hashBytes
            });
        }
    }
}
