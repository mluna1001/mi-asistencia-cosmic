﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MiAsistencia.API.Models.Extensions
{
    public class SrvRest
    {
        public static RequestAnswer CallRestObject(string uri, string end, object data)
        {
            var client = new RestClient(uri);
            var request = new RestRequest(end, Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-type", "application/json");
            request.AddParameter("application/json; charset=utf-8", JsonConvert.SerializeObject(data), ParameterType.RequestBody);
            var response = new RestResponse();
            //IRestResponse response = client.ExecuteAsync(request, );

            Task.Run(async () =>
            {
                response = await GetResponseContentAsync(client, request) as RestResponse;
            }).Wait();
            //var jsonResponse = JsonConvert.DeserializeObject<job>

            var content = response.Content;
            return new RequestAnswer { Datos = content };
        }

        public static RequestAnswer CallRestParam(string uri, string end, int data)
        {
            var client = new RestClient(uri);
            var request = new RestRequest(end, Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-type", "application/json");
            request.AddParameter("value", data);
            var response = new RestResponse();
            //IRestResponse response = client.ExecuteAsync(request, );

            Task.Run(async () =>
            {
                response = await GetResponseContentAsync(client, request) as RestResponse;
            }).Wait();
            //var jsonResponse = JsonConvert.DeserializeObject<job>

            var content = response.Content;
            return new RequestAnswer { Datos = content };
        }


        public static RequestAnswer CallRestList(string uri, string end, List<int?> data)
        {
            var client = new RestClient(uri);
            var request = new RestRequest(end, Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-type", "application/json");
            request.AddParameter("application/json; charset=utf-8", JsonConvert.SerializeObject(data), ParameterType.RequestBody);
            var response = new RestResponse();
            //IRestResponse response = client.ExecuteAsync(request, );

            Task.Run(async () =>
            {
                response = await GetResponseContentAsync(client, request) as RestResponse;
            }).Wait();
            //var jsonResponse = JsonConvert.DeserializeObject<job>

            var content = response.Content;
            return new RequestAnswer { Datos = content };
        }

        //public static List<MenuDto> CallRestMenu(string uri, string end, object data)
        //{
        //    var client = new RestClient(uri);

        //    var request = new RestRequest(end, Method.POST);
        //    request.RequestFormat = DataFormat.Json;
        //    request.AddHeader("Content-type", "application/json");
        //    request.AddParameter("application/json; charset=utf-8", JsonConvert.SerializeObject(data), ParameterType.RequestBody);
        //    var response = new List<MenuDto>();
        //    //IRestResponse response = client.ExecuteAsync(request, );

        //    Task.Run(async () =>
        //    {
        //        response = await GetResponseContentAsync(client, request) as List<MenuDto>;

        //    }).Wait();
        //    return response;
        //}

        public static Task<IRestResponse> GetResponseContentAsync(RestClient theClient, RestRequest theRequest)
        {
            var tcs = new TaskCompletionSource<IRestResponse>();
            theClient.ExecuteAsync(theRequest, response =>
            {
                tcs.SetResult(response);
            });
            return tcs.Task;
        }

        public static RequestAnswer CallRestParams(string uri, string end, Dictionary<string, object> param)
        {
            var client = new RestClient(uri);

            var request = new RestRequest(end, Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-type", "application/json");
            foreach (var item in param)
                request.AddParameter(item.Key, item.Value);

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return new RequestAnswer { Datos = content };
        }
    }
}