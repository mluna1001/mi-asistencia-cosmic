﻿namespace Cosmic
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.Serialization;
    using ClosedXML.Excel;
    using System.IO;

    public static class ExtensionMethod
    {
        #region Validaciones
        public static bool IsNullEmpty(this string cadena)
        {
            if (string.IsNullOrEmpty(cadena) || string.IsNullOrWhiteSpace(cadena))
                return true;

            return false;
        }

        public static bool IsNullEmpty(this int? entero)
        {
            if (!entero.HasValue || entero == 0)
                return true;

            return false;
        }

        public static bool IsNullEmpty(this DateTime? date)
        {
            if (!date.HasValue || date == DateTime.MinValue)
                return true;
            return false;
        }

        public static bool IsNullEmpty<T>(this List<T> lista)
        {
            if (lista == null || lista.Count == 0)
                return true;
            return false;
        }
        #endregion

        #region Parse
        public static T To<T>(this object obj)
        {
            if (obj == null) return default(T);

            var jsonResolver = new IgnorableSerializerContractResolver();

            var settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.None,
                Formatting = Formatting.Indented,
                ContractResolver = jsonResolver,
                NullValueHandling = NullValueHandling.Ignore,
                TypeNameHandling = TypeNameHandling.None,
                Binder = new EntityFrameworkSerializationBinder()
            };
            string json = JsonConvert.SerializeObject(obj, settings);

            T res = JsonConvert.DeserializeObject<T>(json);

            return res;
        }

        public static string ToJson(this object obj)
        {
            //var ti = obj.GetType().GetTypeInfo().GenericTypeArguments[0];
            //if (obj == null) return default(ti);
            //var t = obj ? (T)(object)typedValue : default(T);

            var jsonResolver = new IgnorableSerializerContractResolver();
            var settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.None,
                Formatting = Newtonsoft.Json.Formatting.Indented,
                ContractResolver = jsonResolver,
                NullValueHandling = NullValueHandling.Ignore,
                TypeNameHandling = TypeNameHandling.None,
                Binder = new EntityFrameworkSerializationBinder()
            };
            string json = JsonConvert.SerializeObject(obj, settings);
            return json;
        }

        /// <summary>
        /// Metodo de extension que parsea un objeto a un Dictionary<string, string>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Dictionary<string, string> ToDictionary(this object obj)
        {
            var ret = new Dictionary<string, string>();
            if (obj is Dictionary<string, string>) return ret;

            foreach (PropertyInfo prop in obj.GetType().GetProperties())
            {
                string propName = prop.Name;
                var val = obj.GetType().GetProperty(propName).GetValue(obj, null);
                if (val != null)
                {
                    ret.Add(propName, val.ToString());
                }
                else
                {
                    ret.Add(propName, null);
                }
            }
            return ret;
        }

        public static string UrlQueryString(this string url, object datos)
        {
            if (datos is Dictionary<string, string>)
            {
                url += "?";
                foreach (var item in (Dictionary<string, string>)datos)
                {
                    url += $"{item.Key}={item.Value}";
                }
            }
            return url;
        }

        #endregion


        public static byte[] workbookToByteArr(XLWorkbook workbook)
        {
            var workbookBytes = new byte[0];
            using (var ms = new MemoryStream())
            {
                workbook.SaveAs(ms);
                workbookBytes = ms.ToArray();
            }
            return workbookBytes;
        }

        public static T GetFirst<T>(this IEnumerable<T> list)
        {
            if (list == null) return default(T);
            if (!list.Any()) return default(T);

            T res = list.FirstOrDefault();

            if (res == null) return default(T);

            return res;
        }

        public static string GetMessage(this Exception ex)
        {
            if (ex.Message != null)
                return ex.Message;

            return ex.ToString();
        }

        public static string LimitString(this string cadena, int maximo)
        {
            if (cadena.Length > maximo)
                cadena = cadena.Substring(0, maximo);
            return cadena;
        }
    }

    public class IgnorableSerializerContractResolver : DefaultContractResolver
    {
        protected readonly Dictionary<Type, HashSet<string>> Ignores;

        public IgnorableSerializerContractResolver()
        {
            this.Ignores = new Dictionary<Type, HashSet<string>>();
        }

        /// <summary>
        /// Explicitly ignore the given property(s) for the given type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="propertyName">one or more properties to ignore.  Leave empty to ignore the type entirely.</param>
        public void Ignore(Type type, params string[] propertyName)
        {
            // start bucket if DNE
            if (!this.Ignores.ContainsKey(type)) this.Ignores[type] = new HashSet<string>();

            foreach (var prop in propertyName)
            {
                this.Ignores[type].Add(prop);
            }
        }

        /// <summary>
        /// Is the given property for the given type ignored?
        /// </summary>
        /// <param name="type"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public bool IsIgnored(Type type, string propertyName)
        {
            if (!this.Ignores.ContainsKey(type)) return false;

            // if no properties provided, ignore the type entirely
            if (this.Ignores[type].Count == 0) return true;

            return this.Ignores[type].Contains(propertyName);
        }

        /// <summary>
        /// The decision logic goes here
        /// </summary>
        /// <param name="member"></param>
        /// <param name="memberSerialization"></param>
        /// <returns></returns>
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty prop = base.CreateProperty(member, memberSerialization);
            var propInfo = member as PropertyInfo;
            if (propInfo != null)
            {
                if (propInfo.GetMethod.IsVirtual && !propInfo.GetMethod.IsFinal)
                {
                    prop.ShouldSerialize = obj => false;
                }
            }

            return prop;
            //JsonProperty property = base.CreateProperty(member, memberSerialization);

            //if (this.IsIgnored(property.DeclaringType, property.PropertyName)
            //// need to check basetype as well for EF -- @per comment by user576838
            //|| this.IsIgnored(property.DeclaringType.BaseType, property.PropertyName))
            //{
            //    property.ShouldSerialize = instance => { return false; };
            //}

            //return property;
        }
    }

    internal class EntityFrameworkSerializationBinder : SerializationBinder
    {
        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;

            //if (serializedType.Namespace == "System.Data.Entity.DynamicProxies")
            //typeName = serializedType.BaseType.FullName;
            //else
            typeName = serializedType.FullName;
        }

        public override Type BindToType(string assemblyName, string typeName)
        {
            throw new NotImplementedException();
        }
    }

}
