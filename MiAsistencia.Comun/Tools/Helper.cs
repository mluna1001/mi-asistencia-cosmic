﻿namespace Cosmic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    
    public static class Helper
    {
        public static string intToString(this List<int> lstInt)
        {
            if (lstInt.Count() == 0)
            {
                return "";
            }
            string ListaTiendas = "";
            foreach (var Item in lstInt)
            {
                ListaTiendas += Item.ToString() + ",";
            }
            return ListaTiendas.Substring(0, ListaTiendas.Count() - 1);
        }
    }


}
