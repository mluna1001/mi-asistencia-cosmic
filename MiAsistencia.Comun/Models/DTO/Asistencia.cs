﻿namespace MiAsistencia.Comun.Models.DTO
{
    using System;

    public class Asistencia
    {
        public int IdAsistencia { get; set; }
        public int IdUsuario { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public bool Estatus { get; set; }
        public int IdTienda { get; set; }
        public string nombrefoto { get; set; }
        public DateTime FechaAlta { get; set; }
        public bool Sincronizado { get; set; }
    }
}
