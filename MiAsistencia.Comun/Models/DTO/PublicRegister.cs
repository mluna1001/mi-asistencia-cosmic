﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Comun.Models.DTO
{
    public class PublicRegister
    {
        public enum Perfil
        {
            Empleado = 206
        }

        public enum Cliente
        {
            Publico = 6
        }

        public enum Proyecto
        {
            Publico = 6
        }

        public enum Region
        {
            Publico = 6
        }
    }
}
