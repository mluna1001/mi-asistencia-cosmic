﻿namespace MiAsistencia.Comun.Models.ORM
{
    public class UsuarioAsistenciaDto
    {
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Alias { get; set; }
        public int IdParent { get; set; }
        public int IdCliente { get; set; }
        public int IdRegion { get; set; }
        public int IdProyecto { get; set; }
    }
}
