﻿namespace Cosmic
{
    using System;
    public class Response
    {
        public Response()
        {
            Success = true;
            Data = null;
            Message = "";
        }

        public Response(object usuario)
        {
            this.Success = true;
            this.Data = usuario.ToJson();
        }

        public bool Success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public Exception excepcion { get; set; }
    }

    public class LoginResponse : Response
    {
        public User User { get; set; }
        public UserLogin UserLogin { get; set; }
        public bool Access { get; set; }
    }

    public class User
    {
        public int IdUsuario { get; set; }
        public Guid Token { get; set; }
    }
    public class UserLogin
    {
        public string alias { get; set; }
        public string password { get; set; }
        public bool dev { get; set; }
        public Guid idPrograma { get; set; }
    }
}
