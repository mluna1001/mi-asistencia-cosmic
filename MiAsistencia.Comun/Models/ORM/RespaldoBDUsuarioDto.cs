﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiAsistencia.Comun.Models.ORM
{
    public class RespaldoBDUsuarioDto
    {
        public int IdUsuario { get; set; }
        public byte[] BasedeDatos { get; set; }
        public string Nombre { get; set; }
        public string RazonRespaldo { get; set; }
    }
}
