﻿namespace Cosmic
{
    using System;
    using System.Collections.Generic;
    public class UsuarioCosmicDto
    {
        public UsuarioCosmicDto()
        {
            menuDtoL = new List<MenuDto>();
        }

        public usuario usuario { get; set; }
        public List<usuario> usuarioL { get; set; }
        public ProgramaUsuarioDt ProgramaUsuario { get; set; }
        public List<MenuDto> menuDtoL { get; set; }
        public List<ActionsDt> ActionsDt { get; set; }
        public PerfilDto PerfilDto { get; set; }
    }

    public partial class UsuarioRegistro
    {
        public int IdUsuario { get; set; }
        public int IdKey { get; set; }
        public string Nombre { get; set; }
        public int NoEmpleado { get; set; }
        public string Contrasena { get; set; }
        public string Alias { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string RFC { get; set; }
        public string IMSS { get; set; }
    }

    public partial class usuario
    {
        public int IdUsuario { get; set; }
        public int IdKey { get; set; }
        public string Nombre { get; set; }
        public int NoEmpleado { get; set; }
        public string Contrasena { get; set; }
        public string Alias { get; set; }
        public string Correo { get; set; }
        public int? IdPerfil { get; set; }
        public int Nivel { get; set; }
        public int? IdUsuarioCosmic { get; set; }
        public int? IdProyecto { get; set; }
        public int? IdRegion { get; set; }
        public bool Bloqueado { get; set; }
        public int? IdParent { get; set; }
        public string UrlFoto { get; set; }
        public int? IdCliente { get; set; }
        public Guid? IdPrograma { get; set; }
        public string Telefono { get; set; }
        public string RFC { get; set; }
        public string IMSS { get; set; }
        public bool? Eliminado { get; set; }
        public List<usuario> Users { get; set; }
        public int? IdUsuarioAlta { get; set; }
        public bool? Dev { get; set; }
    }

    public partial class Subordinado
    {
        public int IdUsuario { get; set; }
        public int IdParent { get; set; }
        public string Alias { get; set; }
        public string Nombre { get; set; }
        public string NoEmpleado { get; set; }
        public bool LunchTime { get; set; }
    }

    public partial class ProgramaUsuarioDt
    {
        public int IdProgramaUsuario { get; set; }
        public int IdUsuario { get; set; }
        public Guid IdPrograma { get; set; }
        public Guid? IdProgramaMovil { get; set; }
        public bool? Acceso { get; set; }
    }

    public class MenuDto
    {
        public int IdMenu { get; set; }
        public string URLMenu { get; set; }
        public string Menu { get; set; }
        public Guid? IdPrograma { get; set; }
        public string Icono { get; set; }
        public string Link { get; set; }
        public bool Eliminar { get; set; }
        public int? IdMenuParent { get; set; }
        public int? Orden { get; set; }
        public List<MenuDto> MenusHijos { get; set; }
    }

    public class PerfilDto
    {
        public int IdPerfil { get; set; }
        public string Perfil1 { get; set; }
        public int? Nivel { get; set; }
        public System.Guid IdPrograma { get; set; }
    }

    public class ActionsDt
    {
        public int idAction { get; set; }
        public string controlador { get; set; }
    }

    public class loginUser
    {
        public string usuario { get; set; }
        public string contrasena { get; set; }
        public Guid IdPrograma { get; set; }
    }
}
