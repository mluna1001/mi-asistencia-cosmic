﻿using MiAsistencia.Dominio.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiAsistencia.Web.Tools
{
    public static class GroupEnumerable
    {
        public static List<OptionTree> BuildTreeAndReturnRootNodes(List<OptionTree> flatItems)
        {
            flatItems = flatItems.GroupBy(c => c.id, (key, c) => c.FirstOrDefault()).ToList();
            var groupParents = flatItems.Where(s => s.idParent == s.id).ToList();

            if (groupParents.Any())
            {
                groupParents.ForEach(s => s.idParent = null);
            }

            var byIdLookup = flatItems.ToLookup(i => i.id);
            foreach (var item in flatItems)
            {
                if (item.idParent != null)
                {
                    var parent = byIdLookup[item.idParent.Value].First();
                    parent.children.Add(item);
                }
            }
            return flatItems.Where(i => i.idParent == null).ToList();
        }
    }


}