import Vue from 'vue'
import Router from 'vue-router'
import Inicio from '../Views/Home.vue'
import Treereport from '../Views/TreeReport.vue'
import Dayreport from '../Views/Dayreport.vue'
import AddUser from "../Views/AddUser.vue";

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'Home',
            component: Inicio
        },
        {
            path: '/inicio',
            name: 'Inicio',
            component: Inicio
        },
        {
            path: '/treereport',
            name: 'Treereport',
            component: Treereport
        },
        {
            path: '/dayreport',
            name: 'Dayreport',
            component: Dayreport
        },
        {
            path: '/adduser',
            name: 'Añadir Usuario',
            component: AddUser
        },
    ]
})
