import Vue from 'vue'
import Vuetify from 'vuetify'
import router from '../router'
import 'vuetify/dist/vuetify.min.css'

import colors from 'vuetify/es5/util/colors'

import VDateRange from 'vuetify-daterange-picker';
import 'vuetify-daterange-picker/dist/vuetify-daterange-picker.css';



Vue.use(Vuetify, {
    theme: {
        primary: '#0F1F40',
        secondary: '#00D8A8',
        accent: '#648C6E',
        error: '#FB0303',
        info: '#9e2a2b',
        success: '#2168A6',
        warning: '#930002'
    }
})
Vue.use(router)

Vue.use(VDateRange)

import Vapp from '../App.vue'
import axios from '../../../node_modules/axios';

new Vue({
    el: '#app',
    router,
    data() {
        return {
            tusuario: {
                usuario: {},
                PerfilDto: {}
            },
            tdia: {},
            isbusy: false,
            vsnackbar: {
                snackbar: false,
                y: 'top',
                color: 'cyan darken-2',
                x: 'right',
                mode: 'vertical',
                timeout: 6000,
                text: 'Hello, I\'m a snackbar',
                colores: ['green', 'info', 'error', 'cyan darken-2'],
            },

            CatCalendar: [],
            fcEvents: [],
            rEvents: []
        }
    },
    methods: {
        Default() {
            this.fSetUser()
        },
        fSetUser() {
            var self = this;
            axios.get("/Home/GetUser")
                .then((response) => {
                    if (response.data === null || response.data === undefined || response.data === '') {
                        window.location = "/Home/Login";
                    }
                    self.tusuario = response.data;
                })
                .catch((error) => {
                    self.$root.fmsg(error)
                })
        },
        toastv(msg, tipo = 3) {
            this.vsnackbar.snackbar = true;
            this.vsnackbar.text = msg;
            this.vsnackbar.color = this.vsnackbar.colores[tipo];
        },
        fmsg(msg, tipo = 3) {
            this.toastv(msg, tipo)
            console.log(msg)
        },
        isBusy(ocupado) {
            this.isbusy = ocupado
        }
    },
    computed: {

    },
    props: {
        source: String
    },
    mounted() {
        this.Default()
    },
    components: {
        Vapp
    }
})

Date.prototype.isSameDay = function (d) {
    return this.getFullYear() === d.getFullYear() &&
        this.getDate() === d.getDate() &&
        this.getMonth() === d.getMonth();
}