﻿using Cosmic;
using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Dto;
using System.Data.SqlClient;
using System.Globalization;
using MiAsistencia.Web.Tools;
using ClosedXML.Excel;
using System.IO;

namespace MiAsistencia.Web.Controllers
{
    public class DayReportController : BaseController
    {
        // GET: Asistencia
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetDataAsistencia(List<int> idUsuario, Pagination pag)
        {
            DateTime fechaDia = DateTime.Now.Date;
            List<string> fecha = new List<string>()
            {
               fechaDia.ToString("yyyy-MM-dd"),
               fechaDia.ToString("yyyy-MM-dd")
            };

            Reporte reporte = new Reporte();
            try
            {
                if (idUsuario != null)
                {
                    var result = GeneraReporte(idUsuario, fecha[0], fecha[1], pag);
                    var enc = encabezado(fecha[0], fecha[1]);

                    reporte.checkAsistencia = result;
                    reporte.Header = enc;
                    reporte.fecha = fecha[0];
                }
                return Json(new Response { Data = reporte, Message = "Se han cargado los datos con exito", Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                return Json(new Response
                {
                    Data = reporte,
                    Message = e.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public List<CheckUserList> GeneraReporte(List<int> usuarios, string fechainicial, string fechafinal, Pagination pag)
        {
            var end = pag.page * pag.rowsPerPage;
            var skip = end - pag.rowsPerPage;
            var idusuariosfiltro = usuarios.Skip(skip).Take(pag.rowsPerPage).ToList();
            string idusuarios = idusuariosfiltro.intToString();

            var tiempol = Qry_Tiempo.rangoidsTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));
            var ret = Qry_Asistencia.readAsistencia(idusuarios, tiempol);

            var asistencias = ajustaIO(ret);
            List<CheckUserList> asistenciaL = new List<CheckUserList>();
            foreach (var item in asistencias)
            {
                foreach (var item2 in item.Children)
                {
                    CheckUserList usuaurioasistencia = new CheckUserList();
                    item2.NombreUsuario = item.NombreUsuario;
                    usuaurioasistencia = item2;
                    asistenciaL.Add(usuaurioasistencia);
                }
            }
            asistenciaL = asistenciaL.OrderBy(s => s.TaskName).ToList();
            return asistenciaL;
        }

        public List<CheckUserList> ajustaIO(List<spu_Asistencias_Result> result)
        {
            List<CheckUserList> listUser = new List<CheckUserList>();
            CheckUserList eUser = new CheckUserList();
            CheckUserList entrada = new CheckUserList();
            CheckUserList ChildrenPunto = new CheckUserList();
            int TaskID = 1;
            bool entra = true;
            bool sale = false;

            foreach (var item in result)
            {
                eUser = listUser.FirstOrDefault(s => s.IdUsuario == item.idusuario && s.Anio == item.anio && s.Mes == item.mesdelanio && s.Semana == item.semanadelanio && s.Dia == item.dia);

                if (eUser == null)
                {
                    DateTime minDia = result.Where(s => s.idusuario == item.idusuario && s.dia == item.dia && s.semanadelanio == item.semanadelanio && s.anio == item.anio).Min(f => f.fecha);
                    DateTime maxDia = result.Where(s => s.idusuario == item.idusuario && s.dia == item.dia && s.semanadelanio == item.semanadelanio && s.anio == item.anio).Max(f => f.fecha);

                    CheckUserList nUser = new CheckUserList();
                    nUser.IdUsuario = item.idusuario;
                    nUser.Anio = item.anio;
                    nUser.Mes = item.mesdelanio;
                    nUser.Semana = item.semanadelanio;
                    nUser.TaskID = TaskID;
                    nUser.TaskName = item.NombreUsuario;
                    nUser.StartDate = minDia.ToString("M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    nUser.EndDate = maxDia.ToString("M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    nUser.Dia = item.dia;
                    nUser.IsParent = true;
                    nUser.estanciaDia = maxDia.Subtract(minDia);

                    List<CheckUserList> listChild = new List<CheckUserList>();
                    nUser.Children = listChild;

                    TaskID++;


                    ChildrenPunto = new CheckUserList();
                    ChildrenPunto.IdUsuario = item.idusuario;
                    ChildrenPunto.TaskID = TaskID;
                    ChildrenPunto.Dia = item.dia;
                    ChildrenPunto.Semana = item.semanadelanio;
                    ChildrenPunto.Anio = item.anio;
                    ChildrenPunto.TaskName = item.NombreUsuario;
                    ChildrenPunto.IdAsistenciaSalida = item.idasistencia;
                    ChildrenPunto.IdTienda = item.IdTienda;
                    ChildrenPunto.Cadena = item.Cadena;
                    ChildrenPunto.Formato = item.Formato;
                    ChildrenPunto.CodigoPostal = item.CP;
                    ChildrenPunto.Latitud = item.Latitud;
                    ChildrenPunto.Longitud = item.Longitud;


                    if (item.Estatus == entra)
                    {
                        ChildrenPunto.LatitudEntrada = item.Latitud;
                        ChildrenPunto.LongitudEntrada = item.Longitud;
                    }
                    else
                    {
                        ChildrenPunto.LatitudSalida = item.Latitud;
                        ChildrenPunto.LongitudSalida = item.Longitud;
                    }
                    ChildrenPunto.Determinante = item.Determinante;
                    ChildrenPunto.NombreTienda = item.NombreTienda;
                    ChildrenPunto.StartDate = item.fecha.ToString("M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    ChildrenPunto.IsParent = false;
                    TaskID++;

                    nUser.Children.Add(ChildrenPunto);
                    listUser.Add(nUser);

                    entrada.IdUsuario = item.idusuario;
                    entrada.TaskName = item.NombreUsuario;
                    entrada.StartDate = item.fecha.ToString("M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    entrada.Fecha = item.fecha;
                    entrada.Estatus = entra;
                    entrada.IdAsistenciaEntrada = item.idasistencia;
                    entrada.Latitud = item.Latitud;
                    entrada.Longitud = item.Longitud;
                    entrada.IdTienda = item.IdTienda;
                    if (item.Estatus == entra)
                    {
                        entrada.LatitudEntrada = item.Latitud;
                        entrada.LongitudEntrada = item.Longitud;
                    }
                    else
                    {
                        entrada.LatitudSalida = item.Latitud;
                        entrada.LongitudSalida = item.Longitud;
                    }
                }
                else
                {
                    //Si entrada.estatus es true y item nuevo estatus es true en entrada estatus pinta un punto guarda el nuevo y sigue proceso
                    if (item.Estatus == entra)
                    {
                        ChildrenPunto = new CheckUserList();
                        ChildrenPunto.IdUsuario = item.idusuario;
                        ChildrenPunto.TaskID = TaskID;
                        ChildrenPunto.TaskName = item.NombreUsuario;
                        ChildrenPunto.IdAsistenciaSalida = item.idasistencia;
                        ChildrenPunto.IdTienda = item.IdTienda;
                        ChildrenPunto.Cadena = item.Cadena;
                        ChildrenPunto.Formato = item.Formato;
                        ChildrenPunto.CodigoPostal = item.CP;
                        ChildrenPunto.Latitud = item.Latitud;
                        ChildrenPunto.Longitud = item.Longitud;

                        if (item.Estatus == entra)
                        {
                            ChildrenPunto.LatitudEntrada = item.Latitud;
                            ChildrenPunto.LongitudEntrada = item.Longitud;
                        }
                        else
                        {
                            ChildrenPunto.LatitudSalida = item.Latitud;
                            ChildrenPunto.LongitudSalida = item.Longitud;
                        }
                        ChildrenPunto.Determinante = item.Determinante;
                        ChildrenPunto.NombreTienda = item.NombreTienda;
                        ChildrenPunto.StartDate = item.fecha.ToString("M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                        ChildrenPunto.IsParent = false;
                        TaskID++;
                        eUser.Children.Add(ChildrenPunto);


                        entrada.IdUsuario = item.idusuario;
                        entrada.StartDate = item.fecha.ToString("M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                        entrada.Fecha = item.fecha;
                        entrada.Estatus = entra;
                        entrada.IdAsistenciaEntrada = item.idasistencia;
                        entrada.Latitud = item.Latitud;
                        entrada.Longitud = item.Longitud;
                        entrada.IdTienda = item.IdTienda;

                        if (item.Estatus == entra)
                        {
                            entrada.LatitudEntrada = item.Latitud;
                            entrada.LongitudEntrada = item.Longitud;
                        }
                        else
                        {
                            entrada.LatitudSalida = item.Latitud;
                            entrada.LongitudSalida = item.Longitud;
                        }

                    }
                    else if (item.Estatus == sale)
                    {
                        if (entrada.Estatus == entra && entrada.IdTienda == item.IdTienda)
                        {
                            CheckUserList ChildrenLast = new CheckUserList();
                            ChildrenLast = eUser.Children.LastOrDefault();

                            CheckUserList Children = new CheckUserList();
                            Children.IdUsuario = item.idusuario;
                            Children.TaskID = TaskID;
                            Children.TaskName = item.NombreUsuario;
                            Children.IdTienda = item.IdTienda;
                            Children.Cadena = item.Cadena;
                            Children.Formato = item.Formato;
                            Children.CodigoPostal = item.CP;
                            Children.Latitud = (item.Latitud + entrada.Latitud) / 2;
                            Children.Longitud = (item.Longitud + entrada.Longitud) / 2;
                            Children.LatitudEntrada = entrada.Latitud;
                            Children.LongitudEntrada = entrada.Longitud;
                            Children.LatitudSalida = item.Latitud;
                            Children.LongitudSalida = item.Longitud;
                            Children.Determinante = item.Determinante;
                            Children.NombreTienda = item.NombreTienda;
                            Children.EndDate = item.fecha.ToString("M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            Children.StartDate = entrada.StartDate;
                            Children.estanciaTienda = item.fecha.Subtract(DateTime.Parse(Children.StartDate));
                            Children.IdAsistenciaEntrada = entrada.IdAsistenciaEntrada;
                            Children.IdAsistenciaSalida = item.idasistencia;

                            TaskID++;

                            eUser.Children.Remove(ChildrenLast);
                            eUser.Children.Add(Children);


                            entrada.IdUsuario = item.idusuario;
                            entrada.StartDate = item.fecha.ToString("M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            entrada.Fecha = item.fecha;
                            entrada.Estatus = sale;
                            entrada.IdAsistenciaEntrada = item.idasistencia;
                            entrada.Latitud = item.Latitud;
                            entrada.Longitud = item.Longitud;
                            entrada.IdTienda = item.IdTienda;

                            if (item.Estatus == entra)
                            {
                                entrada.LatitudEntrada = item.Latitud;
                                entrada.LongitudEntrada = item.Longitud;
                            }
                            else
                            {
                                entrada.LatitudSalida = item.Latitud;
                                entrada.LongitudSalida = item.Longitud;
                            }
                        }
                        else
                        {
                            CheckUserList Children = new CheckUserList();
                            Children.IdUsuario = item.idusuario;
                            Children.TaskID = TaskID;
                            Children.TaskName = item.NombreUsuario;
                            Children.IdTienda = item.IdTienda;
                            Children.Cadena = item.Cadena;
                            Children.Formato = item.Formato;
                            Children.CodigoPostal = item.CP;
                            Children.Latitud = item.Latitud;
                            Children.Longitud = item.Longitud;

                            if (item.Estatus == entra)
                            {
                                Children.LatitudEntrada = item.Latitud;
                                Children.LongitudEntrada = item.Longitud;
                            }
                            else
                            {
                                Children.LatitudSalida = item.Latitud;
                                Children.LongitudSalida = item.Longitud;
                            }

                            Children.Determinante = item.Determinante;
                            Children.NombreTienda = item.NombreTienda;
                            Children.StartDate = item.fecha.ToString("M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            Children.IdAsistenciaEntrada = entrada.IdAsistenciaEntrada;
                            Children.IdAsistenciaSalida = item.idasistencia;
                            TaskID++;
                            eUser.Children.Add(Children);


                            entrada.IdUsuario = item.idusuario;
                            entrada.StartDate = item.fecha.ToString("M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            entrada.Fecha = item.fecha;
                            entrada.Estatus = sale;
                            entrada.IdAsistenciaEntrada = item.idasistencia;
                            entrada.Latitud = item.Latitud;
                            entrada.Longitud = item.Longitud;
                            entrada.IdTienda = item.IdTienda;

                            if (item.Estatus == entra)
                            {
                                entrada.LatitudEntrada = item.Latitud;
                                entrada.LongitudEntrada = item.Longitud;
                            }
                            else
                            {
                                entrada.LatitudSalida = item.Latitud;
                                entrada.LongitudSalida = item.Longitud;
                            }
                        }
                    }
                }
            }
            return listUser;
        }

        public List<SchedulePerson> orderAsistencia(List<CheckUserList> listUser, string fechainicial, string fechafinal)
        {
            List<SchedulePerson> schedulePerson = new List<SchedulePerson>();
            foreach (var item in listUser)
            {
                foreach (var item2 in item.Children)
                {
                    SchedulePerson person = new SchedulePerson();
                    person.Id = item2.TaskID;
                    person.Subject = "A";
                    if (item2.StartDate != null)
                    {
                        person.StartTime = DateTime.Parse(item2.StartDate);
                    }
                    if (item2.EndDate != null)
                    {
                        person.EndTime = DateTime.Parse(item2.EndDate);
                    }
                    person.IdAsistenciaEntrada = item2.IdAsistenciaEntrada;
                    person.IdAsistenciaSalida = item2.IdAsistenciaSalida;
                    person.OwnerId = item2.IdUsuario.ToString();
                    person.NombreTienda = item2.NombreTienda;
                    person.Formato = item2.Formato;
                    person.Cadena = item2.Cadena;
                    person.Latitud = item2.Latitud;
                    person.Longitud = item2.Longitud;
                    person.LatitudEntrada = item2.LatitudEntrada;
                    person.LongitudEntrada = item2.LongitudEntrada;
                    person.LatitudSalida = item2.LatitudSalida;
                    person.LongitudSalida = item2.LongitudSalida;
                    schedulePerson.Add(person);
                }
            }

            List<ScheduleRooms> scheduleRooms = new List<ScheduleRooms>();
            List<CheckUserList> group = new List<CheckUserList>();

            listUser = listUser.GroupBy(s => s.IdUsuario, (key, s) => s.FirstOrDefault()).ToList();

            foreach (var item in listUser)
            {
                ScheduleRooms rooms = new ScheduleRooms();
                rooms.Id = item.IdUsuario.ToString();
                scheduleRooms.Add(rooms);

            }

            var lstTiempo = Qry_Tiempo.rangoTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));
            SchedulePerson tiempo = new SchedulePerson();

            foreach (var item in scheduleRooms)
            {
                foreach (var item2 in lstTiempo)
                {
                    tiempo = schedulePerson.FirstOrDefault(s => s.OwnerId == item.Id && s.StartTime.Day == item2.Fecha.Day && s.StartTime.Month == item2.Fecha.Month && s.StartTime.Year == item2.Fecha.Year);
                    if (tiempo == null)
                    {
                        SchedulePerson person = new SchedulePerson();
                        person.Id = schedulePerson.LastOrDefault().Id + 1;
                        person.Subject = "F";
                        person.StartTime = item2.Fecha;
                        person.EndTime = item2.Fecha;
                        person.OwnerId = item.Id;
                        schedulePerson.Add(person);
                    }
                }
            }
            return schedulePerson.OrderBy(s => s.StartTime).ToList();
        }

        public List<DataAsistencia> orderUsuarioAsistencia(List<SchedulePerson> lstAsistencias, List<int> listUser, string fechainicial, string fechafinal)
        {
            List<DataAsistencia> listUsuarios = new List<DataAsistencia>();
            var lstTiempo = Qry_Tiempo.rangoTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));
            int IdExcel = 1;
            int diasAsistidos = 0;
            TimeSpan horasTotales = new TimeSpan();

            string susuario = listUser.intToString();
            var usuarios = Qry_Usuario.findlstUsuario(susuario);

            foreach (var item in usuarios)
            {
                var value = listUsuarios.FirstOrDefault(d => d.IdUsuario == item.IdUsuario);
                if (value == null)
                {
                    DataAsistencia usuario = new DataAsistencia();
                    usuario.IdUsuario = (int)item.IdUsuario;
                    usuario.Nombre = item.Nombre;
                    usuario.NumEmpleado = item.NoEmpleado;
                    usuario.Correo = item.Correo;
                    usuario.Puesto = item.Perfil;
                    usuario.Region = item.Region;
                    usuario.IdRegion = item.idRegion;

                    var buscaSupervisor = usuarios.FirstOrDefault(s => s.IdUsuario == item.IdParent);
                    if (buscaSupervisor != null)
                    {
                        usuario.Supervisor = buscaSupervisor.Nombre;
                    }


                    usuario.Proyecto = item.Proyecto;
                    usuario.IdProyecto = item.idProyecto;
                    usuario.Id = IdExcel;

                    IdExcel++;

                    diasAsistidos = 0;
                    foreach (var item2 in lstTiempo)
                    {
                        var value2 = lstAsistencias.FirstOrDefault(s => int.Parse(s.OwnerId) == item.IdUsuario && s.StartTime.ToShortDateString() == item2.Fecha.ToShortDateString());
                        if (value2 != null)
                        {
                            DateAsistencia asistencia = new DateAsistencia();
                            asistencia.IdDia = item2.DiaMes;
                            asistencia.Dia = item2.NombreDia;
                            asistencia.Asistio = value2.Subject;
                            if (value2.Subject == "A")
                            {
                                diasAsistidos++;
                            }
                            asistencia.StartTime = value2.StartTime;
                            asistencia.EndTime = value2.EndTime;
                            asistencia.Latitud = value2.Latitud;
                            asistencia.Longitud = value2.Longitud;
                            asistencia.LatitudEntrada = value2.LatitudEntrada;
                            asistencia.LatitudSalida = value2.LatitudSalida;
                            asistencia.LongitudEntrada = value2.LongitudEntrada;
                            asistencia.LongitudSalida = value2.LongitudSalida;
                            asistencia.NombreTienda = value2.NombreTienda;
                            usuario.Asistencia.Add(asistencia);
                        }
                    }
                    usuario.DiasAsistidos = diasAsistidos;
                    listUsuarios.Add(usuario);

                }
                else
                {
                    var buscaRegion = value.Region.Contains(item.Region);
                    if (!buscaRegion)
                    {
                        value.Region = $"{value.Region},{item.Region}";
                    }
                    var buscaProyecto = value.Proyecto.Contains(item.Proyecto);
                    if (!buscaProyecto)
                    {
                        value.Proyecto = $"{value.Proyecto},{item.Proyecto}";
                    }

                }
            }
            return listUsuarios;
        }

        public List<Header> encabezado(string fechainicial, string fechafinal)
        {
            var lstTiempo = Qry_Tiempo.rangoTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));
            List<Header> encabezado = new List<Header>();

            Header nombre = new Header()
            {
                text = "Nombre",
                value = "Nombre",
                align = "Center",
                showc = true,
                sortable = true
            };
            encabezado.Add(nombre);

            foreach (var item in lstTiempo)
            {
                Header enc = new Header()
                {
                    text = item.NombreDia.Substring(0, 2),
                    dia = item.DiaMes,
                    value = item.NombreDia,
                    align = "Center",
                    showc = true,
                    sortable = false
                };
                encabezado.Add(enc);
            }

            Header asistencia = new Header()
            {
                text = "Asistencia",
                value = "Asistencia",
                align = "Center",
                showc = true,
                sortable = false
            };
            encabezado.Add(asistencia);
            return encabezado;
        }

        public JsonResult getDateTime()
        {
            try
            {
                DateTime fechaActual = DateTime.Now;
                string fechainicial = "";
                string fechafinal = "";
                if (fechaActual.Day <= 15)
                {
                    DateTime fechainicialdate = new DateTime(fechaActual.Year, fechaActual.Month - 1, 16);
                    fechainicial = fechainicialdate.ToString("yyyy-MM-dd");


                    DateTime fechafinaldate = new DateTime(fechaActual.Year, fechaActual.Month, 1).AddDays(-1);

                    //fechaActual    .AddMonths(1).AddDays(-1);
                    fechafinal = fechafinaldate.ToString("yyyy-MM-dd");
                }
                else
                {
                    fechainicial = new DateTime(fechaActual.Year, fechaActual.Month, 1).ToString("yyyy-MM-dd");
                    fechafinal = new DateTime(fechaActual.Year, fechaActual.Month, 15).ToString("yyyy-MM-dd");
                }

                List<string> dateTime = new List<string>()
                {
                    fechainicial,fechafinal
                };
                return Json(new Response { Data = dateTime, Message = "Se han cargado los datos con exito", Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new Response
                {
                    Data = "",
                    Message = e.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult asistenciasUsuario(List<int> idUsuarios, string fechainicial, string fechafinal)
        {
            try
            {
                string idusuarios = idUsuarios.ToList().intToString();

                var tiempol = Qry_Tiempo.rangoidsTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));
                var ret = Qry_Asistencia.readAsistencia(idusuarios, tiempol);
                //Asistencias por dia por usuario.
                var asistencias = ajustaIO(ret).OrderBy(i => i.IdUsuario).ThenBy(d => d.StartDate).ToList();
                return Json(new Response { Data = asistencias, Message = "Se han cargado los datos con exito", Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new Response
                {
                    Data = "",
                    Message = e.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
