﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Cosmic
{
    [HandleError]
    public class BaseController : Controller
    {
        #region Propiedades
        public UsuarioCosmicDto tUsuario { get; set; }
        public static Guid sIdPrograma { get; set; } = new Guid("EB1D874A-3DE1-4BA6-8591-AC42B50702E4");

        public const string NameSession = "tUsuario";
        public UsuarioCosmicDto pivoteUsuario { get; set; }
        #endregion Propiedades

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            tUsuario = (UsuarioCosmicDto)Session[NameSession];

            if (tUsuario == null)
                RedirectToAction("Login", "Home");
            else
            {
                //if (sPrintRegistroPatronal.IsNullEmpty()) SetPrintRegistroPatronal();

                if (tUsuario.menuDtoL.IsNullEmpty()) SetUsuario(tUsuario.usuario.IdUsuario, sIdPrograma);
            }
        }

        #region Catalogos
        public void SetCalendar()
        {

        }

        public void SetUsuario(int idusuario, Guid __token)
        {
            tUsuario = UsuarioCosmicSrv.Login2(idusuario, __token);
            Session[NameSession] = tUsuario;
        }

        #endregion
        protected override void OnException(ExceptionContext filterContext)
        {
            //dont interfere if the exception is already handled            NullReferenceException
            if (filterContext.ExceptionHandled)
                return;

            filterContext.Controller.TempData["exception"] = filterContext.Exception;

            ////log exception
            tUsuario = (UsuarioCosmicDto)Session[NameSession];

            //Only return view, no need for redirection
            filterContext.Result = View(new RouteValueDictionary(new { area = "", controller = "Error", action = "NoAccess" }));

            //advise subsequent exception filters not to interfere and stop
            // asp.net from showing yellow screen of death
            filterContext.ExceptionHandled = true;

            //erase any output already generated
            filterContext.HttpContext.Response.Clear();

            //base.OnException(filterContext);
            var st = new StackTrace(filterContext.Exception, true);
            var frame = st.GetFrame(st.FrameCount - 1);
            var controllerName = filterContext.RouteData.Values["controller"];
            var actionName = filterContext.RouteData.Values["action"];
            var line = frame.GetFileLineNumber();
            //QUBitacora.Save(filterContext.Exception, $"controllerName: {controllerName}   actionName: {actionName}   line: {line}    frame: {frame}");

            tUsuario = (UsuarioCosmicDto)Session[NameSession];
            if (tUsuario == null)
                RedirectToAction("Login", "Home");
        }

        protected void Application_Error()
        {
            //Exception exception = Server.GetLastError();
            //// Log the exception.

            //var logger = Container.Get<ILoggingService>();
            //logger.Error(User.Identity.Name, ExceptionHelper.BuildWebExceptionMessage(exception));

            //Response.Clear();

            //HttpException httpException = exception as HttpException;

            //RouteData routeData = new RouteData();
            //routeData.Values.Add("controller", "Error");

            ////if (httpException == null)
            ////{
            //routeData.Values.Add("action", "PublicError");
            ////}
            ////else //It's an Http Exception, Let's handle it.
            ////{
            ////    switch (httpException.GetHttpCode())
            ////    {
            ////        case 404:
            ////            // Page not found.
            ////            routeData.Values.Add("action", "HttpError404");
            ////            break;
            ////        case 500:
            ////            // Server error.
            ////            routeData.Values.Add("action", "HttpError500");
            ////            break;

            ////        // Here you can handle Views to other error codes.
            ////        // I choose a General error template
            ////        default:
            ////            routeData.Values.Add("action", "General");
            ////            break;
            ////    }
            ////}

            //// Pass exception details to the target error View.
            //routeData.Values.Add("error", exception);

            //// Clear the error on server.
            //Server.ClearError();

            //// Avoid IIS7 getting in the middle
            //Response.TrySkipIisCustomErrors = true;

            //// Call target Controller and pass the routeData.
            //IController errorController = new ErrorController();
            //errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }
    }
}