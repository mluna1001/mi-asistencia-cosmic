﻿using Cosmic;
using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Dto;
using MiAsistencia.Dominio.Models.Querys;
using MiAsistencia.Web.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiAsistencia.Web.Controllers.Filters
{
    public class RegionController : BaseController
    {
        [HttpPost]
        public JsonResult gefltRegionCliente(List<Proyecto> obj)
        {
            try
            {
                int idUsuario = tUsuario.usuario.IdUsuario;
                List<int> idsProyecto = obj.Select(s => s.IdProyecto).ToList();
                List<RegionDto> lstRegion = Qry_Region.region_proyecto(idsProyecto).To<List<RegionDto>>();
                string sidsProyecto = idsProyecto.intToString();
                var userTree = Qry_Usuario.GetChildren(idUsuario, sidsProyecto, "");
                var structure = GroupEnumerable.BuildTreeAndReturnRootNodes(userTree);

                FilterData data = new FilterData()
                {
                    regionDto = lstRegion,
                    userTree = structure

                };
                return Json(new Response { Data = data, Message = "Se han cargado los datos con exito", Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new Response
                {
                    Data = "",
                    Message = e.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult getRegionByUser()
        {
            try
            {
                var lst = Qry_Region.region_usuario(tUsuario.usuario.IdUsuario);
                return Json(new Response
                {
                    Data = lst,
                    Message = "Regiones cargadas correctamente",
                    Success = true,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new Response
                {
                    Data = "",
                    Message = ex.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
