﻿using Cosmic;
using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiAsistencia.Web.Controllers.Filters
{
    public class ClienteController : BaseController
    {
        public JsonResult gefltCliente()
        {
            try
            {
                int idUsuario = tUsuario.usuario.IdUsuario;
                List<Cliente> lstCliente = Qry_Cliente.cliente_usuario(idUsuario).To<List<Cliente>>();
                return Json(new Response { Data = lstCliente, Message = "Se han cargado los datos con exito", Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new Response
                {
                    Data = "",
                    Message = e.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}