﻿using Cosmic;
using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiAsistencia.Web.Controllers.Filters
{
    public class ProyectoController : BaseController
    {
        [HttpPost]
        public JsonResult gefltProyectoCliente(List<Cliente> obj)
        {
            try
            {
                int idUsuario = tUsuario.usuario.IdUsuario;
                List<int> idsCliente = obj.Select(s => s.IdCliente).ToList();
                List<Proyecto> lstProyecto = Qry_Proyecto.proyecto_cliente(idsCliente).To<List<Proyecto>>();
                return Json(new Response { Data = lstProyecto, Message = "Se han cargado los datos con exito", Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new Response
                {
                    Data = "",
                    Message = e.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
