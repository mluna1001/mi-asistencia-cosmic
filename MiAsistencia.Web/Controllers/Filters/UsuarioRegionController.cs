﻿using Cosmic;
using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Querys;
using MiAsistencia.Web.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiAsistencia.Web.Controllers.Filters
{
    public class UsuarioRegionController : BaseController
    {
        [HttpPost]
        public JsonResult getUsuarioRegion(List<Region> obj)
        {
            try
            {
                int idUsuario = tUsuario.usuario.IdUsuario;
                List<int> idsRegion = obj.Select(s => s.IdRegion).ToList();
                string sidsRegion = idsRegion.intToString();
                var userTree = Qry_Usuario.GetChildren(idUsuario, "", sidsRegion);
                var structure = GroupEnumerable.BuildTreeAndReturnRootNodes(userTree);
                return Json(new Response { Data = structure, Message = "Se han cargado los datos con exito", Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new Response
                {
                    Data = "",
                    Message = e.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
