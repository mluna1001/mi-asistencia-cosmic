﻿using Cosmic;
using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Dto;
using System.Data.SqlClient;
using System.Globalization;
using MiAsistencia.Web.Tools;
using ClosedXML.Excel;
using System.IO;

namespace MiAsistencia.Web.Controllers
{
    public class AsistenciaController : BaseController
    {
        // GET: Asistencia
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetDataAsistencia(List<int> idUsuario, List<string> fecha, Pagination pag)
        {
            Reporte reporte = new Reporte();
            try
            {
                if (idUsuario != null)
                {
                    var result = GeneraReporte(idUsuario, fecha[0], fecha[1], pag);
                    var enc = encabezado(fecha[0], fecha[1]);

                    reporte.dataAsistencia = result;
                    reporte.Header = enc;
                }
                return Json(new Response { Data = reporte, Message = "Se han cargado los datos con exito", Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                return Json(new Response
                {
                    Data = reporte,
                    Message = e.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public List<DataAsistencia> GeneraReporte(List<int> usuarios, string fechainicial, string fechafinal, Pagination pag)
        {
            var end = pag.page * pag.rowsPerPage;
            var skip = end - pag.rowsPerPage;
            var idusuariosfiltro = usuarios.Skip(skip).Take(pag.rowsPerPage).ToList();
            string idusuarios = idusuariosfiltro.intToString();

            var tiempol = Qry_Tiempo.rangoidsTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));

            var ret = Qry_Asistencia.readAsistencia(idusuarios, tiempol);
            Qry_Log.InsertLog(new Exception(), 1, 2, " salio de readAsistencia");

            var lstAsistencias = orderAsistencia(ajustaIO(ret), fechainicial, fechafinal);
            Qry_Log.InsertLog(new Exception(), 1, 3, " salio de orderasistencia");


            var lstUsuarioAsistencia = orderUsuarioAsistencia(lstAsistencias, idusuariosfiltro, fechainicial, fechafinal);
            return lstUsuarioAsistencia;
        }

        public List<CheckUserList> ajustaIO(List<spu_Asistencias_Result> result)
        {
            List<CheckUserList> listUser = new List<CheckUserList>();
            CheckUserList eUser = new CheckUserList();
            CheckUserList entrada = new CheckUserList();
            CheckUserList entradaAlmuerzo = new CheckUserList();
            CheckUserList ChildrenPunto = new CheckUserList();
            int TaskID = 1;
            bool entra = true;
            bool sale = false;

            foreach (var item in result)
            {
                eUser = listUser.FirstOrDefault(s => s.IdUsuario == item.idusuario && s.Anio == item.anio && s.Mes == item.mesdelanio && s.Semana == item.semanadelanio && s.Dia == item.dia);

                if (eUser == null)
                {
                    DateTime minDia = result.Where(s => s.idusuario == item.idusuario && s.dia == item.dia && s.semanadelanio == item.semanadelanio && s.anio == item.anio).Min(f => f.fecha);
                    DateTime maxDia = result.Where(s => s.idusuario == item.idusuario && s.dia == item.dia && s.semanadelanio == item.semanadelanio && s.anio == item.anio).Max(f => f.fecha);

                    CheckUserList nUser = new CheckUserList();
                    nUser.IdUsuario = item.idusuario;
                    nUser.Anio = item.anio;
                    nUser.Mes = item.mesdelanio;
                    nUser.Semana = item.semanadelanio;
                    nUser.TaskID = TaskID;
                    nUser.TaskName = item.NombreUsuario;
                    nUser.StartDate = minDia.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    nUser.EndDate = maxDia.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    nUser.Dia = item.dia;
                    nUser.IsParent = true;
                    nUser.estanciaDia = maxDia.Subtract(minDia);

                    List<CheckUserList> listChild = new List<CheckUserList>();
                    nUser.Children = listChild;

                    TaskID++;


                    ChildrenPunto = new CheckUserList();
                    ChildrenPunto.IdUsuario = item.idusuario;
                    ChildrenPunto.TaskID = TaskID;
                    ChildrenPunto.Dia = item.dia;
                    ChildrenPunto.Semana = item.semanadelanio;
                    ChildrenPunto.Anio = item.anio;
                    ChildrenPunto.TaskName = item.NombreUsuario;
                    ChildrenPunto.IdAsistenciaSalida = item.idasistencia;
                    ChildrenPunto.IdTienda = item.IdTienda;
                    ChildrenPunto.Cadena = item.Cadena;
                    ChildrenPunto.Formato = item.Formato;
                    ChildrenPunto.CodigoPostal = item.CP;
                    ChildrenPunto.Latitud = item.Latitud;
                    ChildrenPunto.Longitud = item.Longitud;


                    if (item.Estatus == entra)
                    {
                        ChildrenPunto.LatitudEntrada = item.Latitud;
                        ChildrenPunto.LongitudEntrada = item.Longitud;
                    }
                    else
                    {
                        ChildrenPunto.LatitudSalida = item.Latitud;
                        ChildrenPunto.LongitudSalida = item.Longitud;
                    }
                    ChildrenPunto.Determinante = item.Determinante;
                    ChildrenPunto.NombreTienda = item.NombreTienda;
                    ChildrenPunto.IdTipoAsistencia = item.IdTipoAsistencia;
                    ChildrenPunto.DesTipoAsistencia = item.Descripcion;
                    ChildrenPunto.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    ChildrenPunto.IsParent = false;
                    TaskID++;

                    nUser.Children.Add(ChildrenPunto);
                    listUser.Add(nUser);

                    entrada.IdUsuario = item.idusuario;
                    entrada.TaskName = item.NombreUsuario;
                    entrada.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                    entrada.Fecha = item.fecha;
                    entrada.Estatus = entra;
                    entrada.IdAsistenciaEntrada = item.idasistencia;
                    entrada.Latitud = item.Latitud;
                    entrada.Longitud = item.Longitud;
                    entrada.IdTienda = item.IdTienda;
                    entrada.IdTipoAsistencia = item.IdTipoAsistencia;
                    if (item.Estatus == entra)
                    {
                        entrada.LatitudEntrada = item.Latitud;
                        entrada.LongitudEntrada = item.Longitud;
                    }
                    else
                    {
                        entrada.LatitudSalida = item.Latitud;
                        entrada.LongitudSalida = item.Longitud;
                    }
                }
                else
                {
                    //Si entrada.estatus es true y item nuevo estatus es true en entrada estatus pinta un punto guarda el nuevo y sigue proceso
                    if (item.Estatus == entra && item.IdTipoAsistencia == 1)
                    {
                        ChildrenPunto = new CheckUserList();
                        ChildrenPunto.IdUsuario = item.idusuario;
                        ChildrenPunto.TaskID = TaskID;
                        ChildrenPunto.TaskName = item.NombreUsuario;
                        ChildrenPunto.IdAsistenciaSalida = item.idasistencia;
                        ChildrenPunto.IdTienda = item.IdTienda;
                        ChildrenPunto.Cadena = item.Cadena;
                        ChildrenPunto.Formato = item.Formato;
                        ChildrenPunto.CodigoPostal = item.CP;
                        ChildrenPunto.Latitud = item.Latitud;
                        ChildrenPunto.Longitud = item.Longitud;

                        if (item.Estatus == entra)
                        {
                            ChildrenPunto.LatitudEntrada = item.Latitud;
                            ChildrenPunto.LongitudEntrada = item.Longitud;
                        }
                        else
                        {
                            ChildrenPunto.LatitudSalida = item.Latitud;
                            ChildrenPunto.LongitudSalida = item.Longitud;
                        }
                        ChildrenPunto.Determinante = item.Determinante;
                        ChildrenPunto.NombreTienda = item.NombreTienda;
                        ChildrenPunto.IdTipoAsistencia = item.IdTipoAsistencia;
                        ChildrenPunto.DesTipoAsistencia = item.Descripcion;
                        ChildrenPunto.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                        ChildrenPunto.IsParent = false;
                        TaskID++;
                        eUser.Children.Add(ChildrenPunto);


                        entrada.IdUsuario = item.idusuario;
                        entrada.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                        entrada.Fecha = item.fecha;
                        entrada.Estatus = entra;
                        entrada.IdAsistenciaEntrada = item.idasistencia;
                        entrada.Latitud = item.Latitud;
                        entrada.Longitud = item.Longitud;
                        entrada.IdTienda = item.IdTienda;
                        entrada.IdTipoAsistencia = item.IdTipoAsistencia;

                        if (item.Estatus == entra)
                        {
                            entrada.LatitudEntrada = item.Latitud;
                            entrada.LongitudEntrada = item.Longitud;
                        }
                        else
                        {
                            entrada.LatitudSalida = item.Latitud;
                            entrada.LongitudSalida = item.Longitud;
                        }

                    }
                    else if (item.Estatus == sale && item.IdTipoAsistencia == 4)
                    {
                        if (entrada.Estatus == entra && entrada.IdTipoAsistencia == 1 && entrada.IdTienda == item.IdTienda)
                        {
                            CheckUserList ChildrenLast = new CheckUserList();
                            ChildrenLast = eUser.Children.LastOrDefault(s => s.IdTipoAsistencia != 2 && s.IdTipoAsistencia != 3);

                            CheckUserList Children = new CheckUserList();
                            Children.IdUsuario = item.idusuario;
                            Children.TaskID = TaskID;
                            Children.TaskName = item.NombreUsuario;
                            Children.IdTienda = item.IdTienda;
                            Children.Cadena = item.Cadena;
                            Children.Formato = item.Formato;
                            Children.CodigoPostal = item.CP;
                            Children.Latitud = (item.Latitud + entrada.Latitud) / 2;
                            Children.Longitud = (item.Longitud + entrada.Longitud) / 2;
                            Children.LatitudEntrada = entrada.Latitud;
                            Children.LongitudEntrada = entrada.Longitud;
                            Children.LatitudSalida = item.Latitud;
                            Children.LongitudSalida = item.Longitud;
                            Children.Determinante = item.Determinante;
                            Children.NombreTienda = item.NombreTienda;
                            Children.IdTipoAsistencia = item.IdTipoAsistencia;
                            Children.DesTipoAsistencia = item.Descripcion;
                            Children.EndDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            Children.StartDate = entrada.StartDate;
                            Children.estanciaTienda = item.fecha.Subtract(DateTime.Parse(Children.StartDate));
                        
                            Children.IdAsistenciaEntrada = entrada.IdAsistenciaEntrada;
                            Children.IdAsistenciaSalida = item.idasistencia;

                            TaskID++;

                            eUser.Children.Remove(ChildrenLast);
                            eUser.Children.Add(Children);


                            entrada.IdUsuario = item.idusuario;
                            entrada.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            entrada.Fecha = item.fecha;
                            entrada.Estatus = sale;
                            entrada.IdAsistenciaEntrada = item.idasistencia;
                            entrada.Latitud = item.Latitud;
                            entrada.Longitud = item.Longitud;
                            entrada.IdTienda = item.IdTienda;
                            entrada.IdTipoAsistencia = item.IdTipoAsistencia;

                            if (item.Estatus == entra)
                            {
                                entrada.LatitudEntrada = item.Latitud;
                                entrada.LongitudEntrada = item.Longitud;
                            }
                            else
                            {
                                entrada.LatitudSalida = item.Latitud;
                                entrada.LongitudSalida = item.Longitud;
                            }
                        }
                        else
                        {
                            CheckUserList Children = new CheckUserList();
                            Children.IdUsuario = item.idusuario;
                            Children.TaskID = TaskID;
                            Children.TaskName = item.NombreUsuario;
                            Children.IdTienda = item.IdTienda;
                            Children.Cadena = item.Cadena;
                            Children.Formato = item.Formato;
                            Children.CodigoPostal = item.CP;
                            Children.Latitud = item.Latitud;
                            Children.Longitud = item.Longitud;

                            if (item.Estatus == entra)
                            {
                                Children.LatitudEntrada = item.Latitud;
                                Children.LongitudEntrada = item.Longitud;
                            }
                            else
                            {
                                Children.LatitudSalida = item.Latitud;
                                Children.LongitudSalida = item.Longitud;
                            }

                            Children.Determinante = item.Determinante;
                            Children.NombreTienda = item.NombreTienda;
                            Children.IdTipoAsistencia = item.IdTipoAsistencia;
                            Children.DesTipoAsistencia = item.Descripcion;
                            Children.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            Children.IdAsistenciaEntrada = entrada.IdAsistenciaEntrada;
                            Children.IdAsistenciaSalida = item.idasistencia;
                            TaskID++;
                            eUser.Children.Add(Children);


                            entrada.IdUsuario = item.idusuario;
                            entrada.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            entrada.Fecha = item.fecha;
                            entrada.Estatus = sale;
                            entrada.IdAsistenciaEntrada = item.idasistencia;
                            entrada.Latitud = item.Latitud;
                            entrada.Longitud = item.Longitud;
                            entrada.IdTienda = item.IdTienda;
                            entrada.IdTipoAsistencia = item.IdTipoAsistencia;

                            if (item.Estatus == entra)
                            {
                                entrada.LatitudEntrada = item.Latitud;
                                entrada.LongitudEntrada = item.Longitud;
                            }
                            else
                            {
                                entrada.LatitudSalida = item.Latitud;
                                entrada.LongitudSalida = item.Longitud;
                            }
                        }
                    }





                    //---------
                    //Si entrada.estatus es true y item nuevo estatus es true en entrada estatus pinta un punto guarda el nuevo y sigue proceso
                    if (item.Estatus == sale && item.IdTipoAsistencia == 2)
                    {
                        ChildrenPunto = new CheckUserList();
                        ChildrenPunto.IdUsuario = item.idusuario;
                        ChildrenPunto.TaskID = TaskID;
                        ChildrenPunto.TaskName = item.NombreUsuario;
                        ChildrenPunto.IdAsistenciaSalida = item.idasistencia;
                        ChildrenPunto.IdTienda = item.IdTienda;
                        ChildrenPunto.Cadena = item.Cadena;
                        ChildrenPunto.Formato = item.Formato;
                        ChildrenPunto.CodigoPostal = item.CP;
                        ChildrenPunto.Latitud = item.Latitud;
                        ChildrenPunto.Longitud = item.Longitud;

                        if (item.Estatus == entra)
                        {
                            ChildrenPunto.LatitudEntrada = item.Latitud;
                            ChildrenPunto.LongitudEntrada = item.Longitud;
                        }
                        else
                        {
                            ChildrenPunto.LatitudSalida = item.Latitud;
                            ChildrenPunto.LongitudSalida = item.Longitud;
                        }
                        ChildrenPunto.Determinante = item.Determinante;
                        ChildrenPunto.NombreTienda = item.NombreTienda + " - Almuerzo";
                        ChildrenPunto.IdTipoAsistencia = item.IdTipoAsistencia;
                        ChildrenPunto.DesTipoAsistencia = item.Descripcion;
                        ChildrenPunto.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                        ChildrenPunto.IsParent = false;
                        TaskID++;
                        eUser.Children.Add(ChildrenPunto);


                        entradaAlmuerzo.IdUsuario = item.idusuario;
                        entradaAlmuerzo.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                        entradaAlmuerzo.Fecha = item.fecha;
                        entradaAlmuerzo.Estatus = sale;
                        entradaAlmuerzo.IdAsistenciaEntrada = item.idasistencia;
                        entradaAlmuerzo.Latitud = item.Latitud;
                        entradaAlmuerzo.Longitud = item.Longitud;
                        entradaAlmuerzo.IdTienda = item.IdTienda;
                        entradaAlmuerzo.IdTipoAsistencia = item.IdTipoAsistencia;

                        if (item.Estatus == entra)
                        {
                            entradaAlmuerzo.LatitudEntrada = item.Latitud;
                            entradaAlmuerzo.LongitudEntrada = item.Longitud;
                        }
                        else
                        {
                            entradaAlmuerzo.LatitudSalida = item.Latitud;
                            entradaAlmuerzo.LongitudSalida = item.Longitud;
                        }

                    }
                    else if (item.Estatus == entra && item.IdTipoAsistencia == 3)
                    {
                        if (entradaAlmuerzo.Estatus == sale && entradaAlmuerzo.IdTipoAsistencia == 2 && entradaAlmuerzo.IdTienda == item.IdTienda)
                        {
                            CheckUserList ChildrenLast = new CheckUserList();
                            ChildrenLast = eUser.Children.LastOrDefault(s => s.IdTipoAsistencia != 1 && s.IdTipoAsistencia != 4);

                            CheckUserList Children = new CheckUserList();
                            Children.IdUsuario = item.idusuario;
                            Children.TaskID = TaskID;
                            Children.TaskName = item.NombreUsuario;
                            Children.IdTienda = item.IdTienda;
                            Children.Cadena = item.Cadena;
                            Children.Formato = item.Formato;
                            Children.CodigoPostal = item.CP;
                            Children.Latitud = (item.Latitud + entradaAlmuerzo.Latitud) / 2;
                            Children.Longitud = (item.Longitud + entradaAlmuerzo.Longitud) / 2;
                            Children.LatitudEntrada = entradaAlmuerzo.Latitud;
                            Children.LongitudEntrada = entradaAlmuerzo.Longitud;
                            Children.LatitudSalida = item.Latitud;
                            Children.LongitudSalida = item.Longitud;
                            Children.Determinante = item.Determinante;
                            Children.NombreTienda = item.NombreTienda + " - Almuerzo";
                            Children.IdTipoAsistencia = item.IdTipoAsistencia;
                            Children.DesTipoAsistencia = item.Descripcion;
                            Children.EndDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            Children.StartDate = entradaAlmuerzo.StartDate;
                            //Children.estanciaTienda = item.fecha.Subtract(DateTime.Parse(Children.StartDate));
                            Children.estanciaTienda = DateTime.Parse(Children.EndDate).Subtract(DateTime.Parse(Children.StartDate));
                            Children.IdAsistenciaEntrada = entradaAlmuerzo.IdAsistenciaEntrada;
                            Children.IdAsistenciaSalida = item.idasistencia;

                            TaskID++;

                            eUser.Children.Remove(ChildrenLast);
                            eUser.Children.Add(Children);


                            entradaAlmuerzo.IdUsuario = item.idusuario;
                            entradaAlmuerzo.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            entradaAlmuerzo.Fecha = item.fecha;
                            entradaAlmuerzo.Estatus = entra;
                            entradaAlmuerzo.IdAsistenciaEntrada = item.idasistencia;
                            entradaAlmuerzo.Latitud = item.Latitud;
                            entradaAlmuerzo.Longitud = item.Longitud;
                            entradaAlmuerzo.IdTienda = item.IdTienda;
                            entradaAlmuerzo.IdTipoAsistencia = item.IdTipoAsistencia;

                            if (item.Estatus == entra)
                            {
                                entradaAlmuerzo.LatitudEntrada = item.Latitud;
                                entradaAlmuerzo.LongitudEntrada = item.Longitud;
                            }
                            else
                            {
                                entradaAlmuerzo.LatitudSalida = item.Latitud;
                                entradaAlmuerzo.LongitudSalida = item.Longitud;
                            }
                        }
                        else
                        {
                            CheckUserList Children = new CheckUserList();
                            Children.IdUsuario = item.idusuario;
                            Children.TaskID = TaskID;
                            Children.TaskName = item.NombreUsuario;
                            Children.IdTienda = item.IdTienda;
                            Children.Cadena = item.Cadena;
                            Children.Formato = item.Formato;
                            Children.CodigoPostal = item.CP;
                            Children.Latitud = item.Latitud;
                            Children.Longitud = item.Longitud;

                            if (item.Estatus == entra)
                            {
                                Children.LatitudEntrada = item.Latitud;
                                Children.LongitudEntrada = item.Longitud;
                            }
                            else
                            {
                                Children.LatitudSalida = item.Latitud;
                                Children.LongitudSalida = item.Longitud;
                            }

                            Children.Determinante = item.Determinante;
                            Children.NombreTienda = item.NombreTienda + " - Almuerzo";
                            Children.IdTipoAsistencia = item.IdTipoAsistencia;
                            Children.DesTipoAsistencia = item.Descripcion;
                            Children.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            Children.IdAsistenciaEntrada = entradaAlmuerzo.IdAsistenciaEntrada;
                            Children.IdAsistenciaSalida = item.idasistencia;
                            TaskID++;
                            eUser.Children.Add(Children);


                            entradaAlmuerzo.IdUsuario = item.idusuario;
                            entradaAlmuerzo.StartDate = item.fecha.ToString("d/M/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                            entradaAlmuerzo.Fecha = item.fecha;
                            entradaAlmuerzo.Estatus = entra;
                            entradaAlmuerzo.IdAsistenciaEntrada = item.idasistencia;
                            entradaAlmuerzo.Latitud = item.Latitud;
                            entradaAlmuerzo.Longitud = item.Longitud;
                            entradaAlmuerzo.IdTienda = item.IdTienda;
                            entradaAlmuerzo.IdTipoAsistencia = item.IdTipoAsistencia;

                            if (item.Estatus == entra)
                            {
                                entradaAlmuerzo.LatitudEntrada = item.Latitud;
                                entradaAlmuerzo.LongitudEntrada = item.Longitud;
                            }
                            else
                            {
                                entradaAlmuerzo.LatitudSalida = item.Latitud;
                                entradaAlmuerzo.LongitudSalida = item.Longitud;
                            }
                        }
                    }



                }
            }
            return listUser;
        }

        public List<SchedulePerson> orderAsistencia(List<CheckUserList> listUser, string fechainicial, string fechafinal)
        {

            Qry_Log.InsertLog(new Exception(), 1, 3, " entro a orderasistencia");


            List<SchedulePerson> schedulePerson = new List<SchedulePerson>();
            foreach (var item in listUser)
            {
                foreach (var item2 in item.Children)
                {
                    SchedulePerson person = new SchedulePerson();
                    person.Id = item2.TaskID;
                    person.Subject = "A";
                    if (item2.StartDate != null)
                    {
                        person.StartTime = DateTime.Parse(item2.StartDate);
                    }
                    if (item2.EndDate != null)
                    {
                        person.EndTime = DateTime.Parse(item2.EndDate);
                    }
                    person.IdAsistenciaEntrada = item2.IdAsistenciaEntrada;
                    person.IdAsistenciaSalida = item2.IdAsistenciaSalida;
                    person.OwnerId = item2.IdUsuario.ToString();
                    person.NombreTienda = item2.NombreTienda;
                    person.DesTipoAsistencia = item2.DesTipoAsistencia;
                    person.Formato = item2.Formato;
                    person.Cadena = item2.Cadena;
                    person.Latitud = item2.Latitud;
                    person.Longitud = item2.Longitud;
                    person.LatitudEntrada = item2.LatitudEntrada;
                    person.LongitudEntrada = item2.LongitudEntrada;
                    person.LatitudSalida = item2.LatitudSalida;
                    person.LongitudSalida = item2.LongitudSalida;
                    schedulePerson.Add(person);
                }
            }

            List<ScheduleRooms> scheduleRooms = new List<ScheduleRooms>();
            List<CheckUserList> group = new List<CheckUserList>();

            listUser = listUser.GroupBy(s => s.IdUsuario, (key, s) => s.FirstOrDefault()).ToList();

            foreach (var item in listUser)
            {
                ScheduleRooms rooms = new ScheduleRooms();
                rooms.Id = item.IdUsuario.ToString();
                scheduleRooms.Add(rooms);

            }

            var lstTiempo = Qry_Tiempo.rangoTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));
            SchedulePerson tiempo = new SchedulePerson();

            foreach (var item in scheduleRooms)
            {
                foreach (var item2 in lstTiempo)
                {
                    tiempo = schedulePerson.FirstOrDefault(s => s.OwnerId == item.Id && s.StartTime.Day == item2.Fecha.Day && s.StartTime.Month == item2.Fecha.Month && s.StartTime.Year == item2.Fecha.Year);
                    if (tiempo == null)
                    {
                        SchedulePerson person = new SchedulePerson();
                        person.Id = schedulePerson.LastOrDefault().Id + 1;
                        person.Subject = "F";
                        person.StartTime = item2.Fecha;
                        person.EndTime = item2.Fecha;
                        person.OwnerId = item.Id;
                        schedulePerson.Add(person);
                    }
                }
            }
            return schedulePerson.OrderBy(s => s.StartTime).ToList();
        }

        public List<DataAsistencia> orderUsuarioAsistencia(List<SchedulePerson> lstAsistencias, List<int> listUser, string fechainicial, string fechafinal)
        {
            List<DataAsistencia> listUsuarios = new List<DataAsistencia>();
            var lstTiempo = Qry_Tiempo.rangoTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));
            int IdExcel = 1;
            int diasAsistidos = 0;
            TimeSpan horasTotales = new TimeSpan();

            string susuario = listUser.intToString();
            var usuarios = Qry_Usuario.findlstUsuario(susuario);

            foreach (var item in usuarios)
            {
                var value = listUsuarios.FirstOrDefault(d => d.IdUsuario == item.IdUsuario);
                if (value == null)
                {
                    DataAsistencia usuario = new DataAsistencia();
                    usuario.IdUsuario = (int)item.IdUsuario;
                    usuario.Nombre = item.Nombre;
                    usuario.NumEmpleado = item.NoEmpleado;
                    usuario.Correo = item.Correo;
                    usuario.Puesto = item.Perfil;
                    usuario.Region = item.Region;
                    usuario.IdRegion = item.idRegion;

                    var buscaSupervisor = usuarios.FirstOrDefault(s => s.IdUsuario == item.IdParent);
                    if (buscaSupervisor != null)
                    {
                        usuario.Supervisor = buscaSupervisor.Nombre;
                    }


                    usuario.Proyecto = item.Proyecto;
                    usuario.IdProyecto = item.idProyecto;
                    usuario.Id = IdExcel;

                    IdExcel++;

                    diasAsistidos = 0;
                    foreach (var item2 in lstTiempo)
                    {
                        var value2 = lstAsistencias.FirstOrDefault(s => int.Parse(s.OwnerId) == item.IdUsuario && s.StartTime.ToShortDateString() == item2.Fecha.ToShortDateString());
                        if (value2 != null)
                        {
                            DateAsistencia asistencia = new DateAsistencia();
                            asistencia.IdDia = item2.DiaMes;
                            asistencia.Dia = item2.NombreDia;
                            asistencia.Asistio = value2.Subject;
                            if (value2.Subject == "A")
                            {
                                diasAsistidos++;
                            }
                            asistencia.StartTime = value2.StartTime;
                            asistencia.EndTime = value2.EndTime;
                            asistencia.Latitud = value2.Latitud;
                            asistencia.Longitud = value2.Longitud;
                            asistencia.LatitudEntrada = value2.LatitudEntrada;
                            asistencia.LatitudSalida = value2.LatitudSalida;
                            asistencia.LongitudEntrada = value2.LongitudEntrada;
                            asistencia.LongitudSalida = value2.LongitudSalida;
                            asistencia.NombreTienda = value2.NombreTienda;
                            asistencia.DesTipoAsistencia = value2.DesTipoAsistencia;
                            usuario.Asistencia.Add(asistencia);
                        }
                    }
                    usuario.DiasAsistidos = diasAsistidos;
                    listUsuarios.Add(usuario);

                }
                else
                {
                    var buscaRegion = value.Region.Contains(item.Region);
                    if (!buscaRegion)
                    {
                        value.Region = $"{value.Region},{item.Region}";
                    }
                    var buscaProyecto = value.Proyecto.Contains(item.Proyecto);
                    if (!buscaProyecto)
                    {
                        value.Proyecto = $"{value.Proyecto},{item.Proyecto}";
                    }

                }
            }
            return listUsuarios;
        }

        public List<Header> encabezado(string fechainicial, string fechafinal)
        {
            var lstTiempo = Qry_Tiempo.rangoTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));
            List<Header> encabezado = new List<Header>();

            Header nombre = new Header()
            {
                text = "Nombre",
                value = "Nombre",
                align = "Center",
                showc = true,
                sortable = true
            };
            encabezado.Add(nombre);

            foreach (var item in lstTiempo)
            {
                Header enc = new Header()
                {
                    text = item.NombreDia.Substring(0, 2),
                    dia = item.DiaMes,
                    value = item.NombreDia,
                    align = "Center",
                    showc = true,
                    sortable = false
                };
                encabezado.Add(enc);
            }

            Header asistencia = new Header()
            {
                text = "Asistencia",
                value = "Asistencia",
                align = "Center",
                showc = true,
                sortable = false
            };
            encabezado.Add(asistencia);
            return encabezado;
        }

        public JsonResult getDateTime()
        {
            try
            {
                DateTime fechaActual = DateTime.Now;
                string fechainicial = "";
                string fechafinal = "";
                if (fechaActual.Day <= 15)
                {
                    int Mesinicio = fechaActual.Month - 1;
                    int anio = fechaActual.Year;
                    if (Mesinicio == 0)
                    {
                        Mesinicio = 12;
                        anio = fechaActual.Year - 1;

                    }

                    DateTime fechainicialdate = new DateTime(anio, Mesinicio, 16);
                    fechainicial = fechainicialdate.ToString("yyyy-MM-dd");


                    DateTime fechafinaldate = new DateTime(fechaActual.Year, fechaActual.Month, 1).AddDays(-1);

                    //fechaActual    .AddMonths(1).AddDays(-1);
                    fechafinal = fechafinaldate.ToString("yyyy-MM-dd");
                }
                else
                {
                    fechainicial = new DateTime(fechaActual.Year, fechaActual.Month, 1).ToString("yyyy-MM-dd");
                    fechafinal = new DateTime(fechaActual.Year, fechaActual.Month, 15).ToString("yyyy-MM-dd");
                }

                List<string> dateTime = new List<string>()
                {
                    fechainicial,fechafinal
                };
                return Json(new Response { Data = dateTime, Message = "Se han cargado los datos con exito", Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new Response
                {
                    Data = "",
                    Message = e.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public XLWorkbook generaExcel(List<DataAsistencia> usuarios, string fechainicial, string fechafinal)
        {
            //Genera el libro de Excel
            var workbook = new XLWorkbook();
            var hojaAsistencias = workbook.Worksheets.Add("Reporte");
            var hojaDetalle = workbook.Worksheets.Add("Detalle");
            var hojaCoordenadas = workbook.Worksheets.Add("DetalleCoordenadas");

            llenahojaAsistencias(hojaAsistencias, usuarios, fechainicial, fechafinal);
            llenahojaDetalle(hojaDetalle, usuarios, fechainicial, fechafinal);
            llenahojaDetalleCoordenadas(hojaCoordenadas, usuarios, fechainicial, fechafinal);

            return workbook;

        }

        [HttpPost]
        public ActionResult exportaExcel(List<int> idsu, string fechainicial, string fechafinal)
        {
            try
            {
                string idusuarios = idsu.intToString();
                var tiempol = Qry_Tiempo.rangoidsTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));

                var ret = Qry_Asistencia.readAsistencia(idusuarios, tiempol);
                var lstAsistencias = orderAsistencia(ajustaIO(ret), fechainicial, fechafinal);
                List<DataAsistencia> usuarios = orderUsuarioAsistencia(lstAsistencias, idsu, fechainicial, fechafinal);


                var workbook = generaExcel(usuarios, fechainicial, fechafinal);
                var bytearray = ExtensionMethod.workbookToByteArr(workbook);
                return File(bytearray, System.Net.Mime.MediaTypeNames.Application.Octet, "ReporteX.xlsx");
            }
            catch (Exception e)
            {
                return Json(new Response
                {
                    Data = "",
                    Message = e.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public void llenahojaAsistencias(IXLWorksheet hojaAsistencias, List<DataAsistencia> usuarios, string fechainicial, string fechafinal)
        {
            DateTime fechainiciatime = DateTime.Parse(fechainicial);
            DateTime fechafinatime = DateTime.Parse(fechafinal);
            #region worksheet
            // Header
            hojaAsistencias.Cell(3, 3)
                .SetValue("Fecha:")
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                 .Font.SetFontSize(11);

            hojaAsistencias.Cell(3, 4)
              .SetValue(DateTime.Now.ToLongDateString())
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
               .Font.SetFontSize(11)
               .Border.OutsideBorderColor = XLColor.FromArgb(0, 0, 0, 0);

            hojaAsistencias.Cell(3, 6)
               .SetValue("Nombre del COP:")
                .Style.Font.SetBold(false)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .Font.SetFontSize(11);

            hojaAsistencias.Range(3, 7, 3, 10)
               .Merge()
               .SetValue("")
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(11)
               .Border.OutsideBorderColor = XLColor.FromArgb(0, 0, 0, 0);

            hojaAsistencias.Cell(5, 5)
             .SetValue("Año:")
              .Style.Font.SetBold(false)
              .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
              .Font.SetFontSize(11);

            hojaAsistencias.Cell(5, 6)
           .SetValue(fechafinatime.Year.ToString())
            .Style.Font.SetBold(false)
            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
            .Font.SetFontSize(11);

            hojaAsistencias.Range(5, 7, 5, 8)
               .Merge()
               .SetValue("Cliente:")
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Justify)
               .Font.SetFontSize(11);

            hojaAsistencias.Range(5, 9, 5, 14)
                  .Merge()
                  .SetValue("")
                  .Style.Font.SetBold(false)
                      .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Justify)
                  .Font.SetFontSize(11);

            hojaAsistencias.Cell(6, 5)
             .SetValue("Mes:")
              .Style.Font.SetBold(false)
              .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
              .Font.SetFontSize(11);


            string fecha = fechainiciatime.ToString("MMMM").Equals(fechafinatime.ToString("MMMM")) ? fecha = fechainiciatime.ToString("MMMM") : fecha = fechainiciatime.ToString("MMMM") + "-" + fechafinatime.ToString("MMMM");

            hojaAsistencias.Cell(6, 6)
               .SetValue(fecha)
                .Style.Font.SetBold(false)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
                .Font.SetFontSize(11);


            hojaAsistencias.Cell(7, 5)
               .SetValue("Periodo:")
                .Style.Font.SetBold(false)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .Font.SetFontSize(11);

            hojaAsistencias.Cell(7, 6)
           .SetValue("Del " + fechainicial + " al " + fechafinal)
            .Style.Font.SetBold(false)
            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
            .Font.SetFontSize(11);


            hojaAsistencias.Range(1, 5, 1, 23)
               .Merge()
               .SetValue("Reporte de las Asistencias")
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
               .Font.SetFontSize(18);

            int pinkColumn = 3;

            hojaAsistencias.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("# Empleado")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistencias.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("Nombre Completo")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistencias.Range(10, pinkColumn, 12, pinkColumn)
               .Merge()
               .SetValue("Correo")
               .Style.Font.SetBold(true)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistencias.Range(10, pinkColumn, 12, pinkColumn)
               .Merge()
               .SetValue("Region")
               .Style.Font.SetBold(true)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistencias.Range(10, pinkColumn, 12, pinkColumn)
               .Merge()
               .SetValue("Puesto")
               .Style.Font.SetBold(true)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistencias.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("Supervisor")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistencias.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("Proyecto")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;

            int rowHeader = 10;
            int columnHeader = 10;
            int mesAnio = DateTime.Parse(fechainicial).Month;
            int rangoDias = (fechafinatime - fechainiciatime).Days;
            int dia = 0;
            int diasmes = 0;

            do
            {
                DateTime rangodias = fechainiciatime.AddDays(dia);
                diasmes++;

                hojaAsistencias.Cell(rowHeader, columnHeader)
                      .SetValue(rangodias.ToString("ddd"))
                    .Style.Font.SetBold(false)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Font.SetFontSize(11);

                hojaAsistencias.Cell(rowHeader + 1, columnHeader)
                    .SetValue(rangodias.Day)
                    .Style.Font.SetBold(false)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Font.SetFontSize(11);
                columnHeader++;

                if (mesAnio != rangodias.Month)
                {
                    string mes = rangodias.AddMonths(-1).ToString("MMMM");

                    hojaAsistencias.Range(9, columnHeader - diasmes, 9, columnHeader - 2)
                       .Merge()
                       .SetValue(mes)
                       .Style.Font.SetBold(true)
                           .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                       .Font.SetFontSize(11);

                    mesAnio = rangodias.Month;
                    diasmes = 1;
                }

                dia++;
            }
            while (dia <= rangoDias);

            hojaAsistencias.Range(9, columnHeader - diasmes, 9, columnHeader - 1)
                     .Merge()
                     .SetValue(fechafinatime.ToString("MMMM"))
                     .Style.Font.SetBold(true)
                         .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                     .Font.SetFontSize(11);



            hojaAsistencias.Range(rowHeader, columnHeader, rowHeader + 2, columnHeader)
              .Merge()
              .SetValue("Dias Asistencia")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(8);

            hojaAsistencias.Range(1, 2, 1, columnHeader)
            .Style.Border.SetBottomBorder(XLBorderStyleValues.Thick)
            .Border.BottomBorderColor = XLColor.FromArgb(0, 192, 0, 0);

            hojaAsistencias.Range(10, 2, 12, 9)
               .Style.Fill.BackgroundColor = XLColor.FromArgb(0, 242, 221, 220);


            hojaAsistencias.Range(10, 2, 10, columnHeader)
              .Style.Border.SetTopBorder(XLBorderStyleValues.Thick)
              .Border.OutsideBorderColor = XLColor.FromArgb(0, 192, 0, 0);

            hojaAsistencias.Range(12, 2, 12, columnHeader)
             .Style.Border.SetBottomBorder(XLBorderStyleValues.Thick)
             .Border.BottomBorderColor = XLColor.FromArgb(0, 192, 0, 0);


            hojaAsistencias.Range(5, 5, 7, 14)
           .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
           .Border.OutsideBorderColor = XLColor.FromArgb(0, 0, 0, 0);

            // Data
            int rowData = 13;
            foreach (var value in usuarios)
            {
                int columnData = 2;

                hojaAsistencias.Cell(rowData, columnData)
                   .SetValue(value.Id)
                   .Style.Font.SetBold(false)
                       .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                   .Font.SetFontSize(8);

                columnData++;

                hojaAsistencias.Cell(rowData, columnData)
                 .SetValue(value.NumEmpleado)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(8);

                columnData++;

                hojaAsistencias.Cell(rowData, columnData)
                  .SetValue(value.Nombre)
                  .Style.Font.SetBold(false)
                      .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                  .Font.SetFontSize(11);

                columnData++;



                hojaAsistencias.Cell(rowData, columnData)
                 .SetValue(value.Correo)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);

                columnData++;


                hojaAsistencias.Cell(rowData, columnData)
                 .SetValue(value.Region)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);


                columnData++;

                hojaAsistencias.Cell(rowData, columnData)
                 .SetValue(value.Puesto)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);

                columnData++;

                hojaAsistencias.Cell(rowData, columnData)
                .SetValue(value.Supervisor)
                .Style.Font.SetBold(false)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Font.SetFontSize(11);

                columnData++;

                hojaAsistencias.Cell(rowData, columnData)
                 .SetValue(value.Proyecto)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);

                columnData++;

                foreach (var value2 in value.Asistencia)
                {
                    hojaAsistencias.Cell(rowData, columnData)
                     .SetValue(value2.Asistio)
                     .Style.Font.SetBold(false)
                         .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                     .Font.SetFontSize(11);

                    columnData++;
                }

                hojaAsistencias.Cell(rowHeader + 3, columnHeader)
                  .SetValue(value.DiasAsistidos)
                  .Style.Font.SetBold(true)
                      .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                  .Font.SetFontSize(11);
                rowHeader++;

                rowData++;
            }


            hojaAsistencias.Cell(rowHeader + 12, 4)
                .SetValue("Nombre")
                .Style.Font.SetBold(true)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Font.SetFontSize(11)
                    .Border.SetTopBorder(XLBorderStyleValues.Thick)
                    .Border.TopBorderColor = XLColor.FromArgb(0, 192, 0, 0);

            hojaAsistencias.Cell(rowHeader + 13, 4)
               .SetValue("Vo.Bo.")
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                   .Font.SetFontSize(8);

            hojaAsistencias.Range(10, columnHeader, rowHeader + 2, columnHeader)
                .Style.Fill.BackgroundColor = XLColor.FromArgb(0, 242, 221, 220);


            hojaAsistencias.Rows().AdjustToContents();
            hojaAsistencias.Columns().AdjustToContents();
            #endregion
        }

        public void llenahojaDetalle(IXLWorksheet hojaAsistenciasDetalle, List<DataAsistencia> usuarios, string fechainicial, string fechafinal)
        {
            string idusuarios = usuarios.Select(d => d.IdUsuario).ToList().intToString();
            var tiempol = Qry_Tiempo.rangoidsTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));

            var ret = Qry_Asistencia.readAsistencia(idusuarios, tiempol);

            //Asistencias por dia por usuario.
            var asistencias = ajustaIO(ret).OrderBy(i => i.IdUsuario).ThenBy(d => d.StartDate).ToList();

            DateTime fechainiciatime = DateTime.Parse(fechainicial);
            DateTime fechafinatime = DateTime.Parse(fechafinal);
            List<Tiempo> listHeader = Qry_Tiempo.rangoTiempo(fechainiciatime, fechafinatime);

            #region Header
            // Header
            //Encabezado: Armm el cuadro superior del documento de asistencias.
            hojaAsistenciasDetalle.Cell(3, 3)
                .SetValue("Fecha:")
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                 .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Cell(3, 4)
              .SetValue(DateTime.Now.ToLongDateString())
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
               .Font.SetFontSize(11)
               .Border.OutsideBorderColor = XLColor.FromArgb(0, 0, 0, 0);

            hojaAsistenciasDetalle.Cell(3, 6)
               .SetValue("Nombre del COP:")
                .Style.Font.SetBold(false)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Range(3, 7, 3, 10)
               .Merge()
               .SetValue("")
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(11)
               .Border.OutsideBorderColor = XLColor.FromArgb(0, 0, 0, 0);

            hojaAsistenciasDetalle.Cell(5, 5)
             .SetValue("Año:")
              .Style.Font.SetBold(false)
              .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
              .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Cell(5, 6)
           .SetValue(fechafinatime.Year.ToString())
            .Style.Font.SetBold(false)
            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
            .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Range(5, 7, 5, 8)
               .Merge()
               .SetValue("Cliente:")
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Justify)
               .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Range(5, 9, 5, 14)
                  .Merge()
                  .SetValue("")
                  .Style.Font.SetBold(false)
                      .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Justify)
                  .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Cell(6, 5)
             .SetValue("Mes:")
              .Style.Font.SetBold(false)
              .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
              .Font.SetFontSize(11);

            string fecha = fechainiciatime.ToString("MMMM").Equals(fechafinatime.ToString("MMMM")) ? fecha = fechainiciatime.ToString("MMMM") : fecha = fechainiciatime.ToString("MMMM") + "-" + fechafinatime.ToString("MMMM");


            hojaAsistenciasDetalle.Cell(6, 6)
               .SetValue(fecha)
                .Style.Font.SetBold(false)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
                .Font.SetFontSize(11);


            hojaAsistenciasDetalle.Cell(7, 5)
               .SetValue("Periodo:")
                .Style.Font.SetBold(false)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Cell(7, 6)
           .SetValue("Del " + fechainicial + " al " + fechafinal)
            .Style.Font.SetBold(false)
            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
            .Font.SetFontSize(11);


            hojaAsistenciasDetalle.Range(1, 5, 1, 23)
               .Merge()
               .SetValue("Reporte de las Asistencias")
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
               .Font.SetFontSize(18);

            int pinkColumn = 3;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("# Empleado")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("Nombre Completo")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
               .Merge()
               .SetValue("Correo")
               .Style.Font.SetBold(true)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
               .Merge()
               .SetValue("Region")
               .Style.Font.SetBold(true)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
               .Merge()
               .SetValue("Puesto")
               .Style.Font.SetBold(true)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("Supervisor")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("Proyecto")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;


            int rowHeader = 10;
            int columnHeader = 10;
            int rangoDias = (fechafinatime - fechainiciatime).Days;
            int mesAnio = DateTime.Parse(fechainicial).Month;
            int diasr = 0;
            int diasmes = 0;
            //Ciclo que construye el encabezado de la tabla del rango de dias seleccionado.
            do
            {
                DateTime rangodias = fechainiciatime.AddDays(diasr);
                diasmes++;

                hojaAsistenciasDetalle.Cell(rowHeader, columnHeader)
                      .SetValue(rangodias.ToString("ddd"))
                    .Style.Font.SetBold(false)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Font.SetFontSize(11);

                hojaAsistenciasDetalle.Cell(rowHeader + 1, columnHeader)
                    .SetValue(rangodias.Day)
                    .Style.Font.SetBold(false)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Font.SetFontSize(11);
                columnHeader++;

                hojaAsistenciasDetalle.Cell(rowHeader, columnHeader)
                  .SetValue("En Tienda")
                  .Style.Font.SetBold(false)
                      .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                  .Font.SetFontSize(11);
                columnHeader++;


                if (mesAnio != rangodias.Month)
                {
                    string mes = rangodias.AddMonths(-1).ToString("MMMM");

                    hojaAsistenciasDetalle.Range(9, columnHeader - diasmes * 2, 9, columnHeader - 3)
                       .Merge()
                       .SetValue(mes)
                       .Style.Font.SetBold(true)
                           .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                       .Font.SetFontSize(11);

                    mesAnio = rangodias.Month;
                    diasmes = 1;
                }

                diasr++;
            }
            while (diasr <= rangoDias);

            hojaAsistenciasDetalle.Range(9, columnHeader - diasmes * 2, 9, columnHeader - 1)
              .Merge()
              .SetValue(fechafinatime.ToString("MMMM"))
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
              .Font.SetFontSize(11);


            hojaAsistenciasDetalle.Range(rowHeader, columnHeader, rowHeader + 2, columnHeader)
              .Merge()
              .SetValue("Dias Asistencia")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(8);

            hojaAsistenciasDetalle.Range(1, 2, 1, columnHeader)
            .Style.Border.SetBottomBorder(XLBorderStyleValues.Thick)
            .Border.BottomBorderColor = XLColor.FromArgb(0, 192, 0, 0);

            hojaAsistenciasDetalle.Range(10, 2, 12, 9)
               .Style.Fill.BackgroundColor = XLColor.FromArgb(0, 242, 221, 220);


            hojaAsistenciasDetalle.Range(10, 2, 10, columnHeader)
              .Style.Border.SetTopBorder(XLBorderStyleValues.Thick)
              .Border.OutsideBorderColor = XLColor.FromArgb(0, 192, 0, 0);

            hojaAsistenciasDetalle.Range(12, 2, 12, columnHeader)
             .Style.Border.SetBottomBorder(XLBorderStyleValues.Thick)
             .Border.BottomBorderColor = XLColor.FromArgb(0, 192, 0, 0);

            hojaAsistenciasDetalle.Range(5, 5, 7, 14)
               .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
               .Border.OutsideBorderColor = XLColor.FromArgb(0, 0, 0, 0);

            #endregion


            int idUsuario = asistencias[0].IdUsuario;
            int renglon = 13;
            int columna = 2;
            int renglonmaximousuario = 0;
            int renglonMaxDia = 0;


            //Ciclo que itera sobre los usuarios seleccionados en el arbol.
            foreach (var value in usuarios)
            {
                //Columna inicial para contruir el documento de excel.
                columna = 2;

                //Contruye la informacion del empleado.
                hojaAsistenciasDetalle.Cell(renglon, columna)
                   .SetValue(value.Id)
                   .Style.Font.SetBold(false)
                       .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                   .Font.SetFontSize(8);

                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                 .SetValue(value.NumEmpleado)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(8);

                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                  .SetValue(value.Nombre)
                  .Style.Font.SetBold(false)
                      .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                  .Font.SetFontSize(11);

                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                 .SetValue(value.Correo)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);

                columna++;


                hojaAsistenciasDetalle.Cell(renglon, columna)
                 .SetValue(value.Region)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);


                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                 .SetValue(value.Puesto)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);

                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                .SetValue(value.Supervisor)
                .Style.Font.SetBold(false)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Font.SetFontSize(11);

                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                 .SetValue(value.Proyecto)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);

                columna++;


                //Busca las asistencias de un usuario
                var buscaUsuario = asistencias.Where(d => d.IdUsuario == value.IdUsuario).ToList();

                //Definimos en que columna se empezaran a contruir las asistencias del usuario
                int columnadata = 10;

                //Columna donde guardaremos en que posicion empezara a construir la informacion de las asistencias de un dia.
                int columnadia = 10;

                //Renglon donde iniciara la contruccion de las asistencias por usuario.
                int rengloninicio = 13;

                //Si el usuario tiene asistencias
                if (buscaUsuario.Count != 0)
                {
                    //Iteramos sobre un listHeader el cual tiene el rango de dias seleccionados por el usuario a consultar
                    foreach (var value2 in listHeader)
                    {
                        //Le decimos que columndata sera igual a columndia, que es donde empezara a construir la informacion para el excel.
                        columnadata = columnadia;

                        //El reglon donde empezara a construir sera igual al renglo de inicio que es 13 mas renglonmaximousuario que es el maximo de renglones que ocupara un usuario y este sera acumulable
                        renglon = rengloninicio + renglonmaximousuario;
                        TimeSpan horaspordia = new TimeSpan(0, 0, 0);

                        //Buscamos las asistencias del dia que nos manda listheader
                        var buscarAsistenciasDia = buscaUsuario.Where(d => DateTime.Parse(d.StartDate).ToShortDateString() == value2.Fecha.ToShortDateString());
                        //Si el usuario no tiene asistencias en el dia iterado entonces escribimos una F, que es una falta.
                        if (buscarAsistenciasDia.Count() == 0)
                        {
                            hojaAsistenciasDetalle.Cell(renglon, columnadata)
                                .SetValue("F")
                                .Style.Font.SetBold(false)
                                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                                .Font.SetFontSize(11);

                            //Columnadata aumenta en dos ya que son las columnas de "En tienda" y la siguiente es donde se escribira la informacion del siguiente dia.
                            columnadata++;
                            columnadata++;

                            //columndia aumenta en dos mas el acumulado que es el acumulado de columnas de las asistencias de un usuario
                            columnadia += 2;
                        }
                        else
                        {
                            //iteramos sobre las asistencias de un usuario para contruir el excel.
                            foreach (var value3 in buscarAsistenciasDia)
                            {
                                foreach (var value4 in value3.Children)
                                {
                                    string horafinal = value4.EndDate != null ? DateTime.Parse(value4.EndDate).ToShortTimeString() : "";
                                    string horainicial = value4.StartDate != null ? DateTime.Parse(value4.StartDate).ToShortTimeString() : "";

                                    if (value4.LatitudEntrada == 0 && value4.LongitudEntrada == 0)
                                    {
                                        hojaAsistenciasDetalle.Cell(renglon, columnadata)
                                            .SetValue(" x " + " - " + horafinal + "  " + value4.Cadena + " - " + value4.NombreTienda)
                                            .Style.Font.SetBold(false)
                                            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                                            .Font.SetFontSize(8);
                                    }
                                    else if (value4.LongitudSalida == 0 && value4.LatitudSalida == 0)
                                    {
                                        hojaAsistenciasDetalle.Cell(renglon, columnadata)
                                           .SetValue(horainicial + " - " + "x" + "  " + value4.Cadena + " - " + value4.NombreTienda)
                                           .Style.Font.SetBold(false)
                                           .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                                           .Font.SetFontSize(8);
                                    }
                                    else
                                    {
                                        hojaAsistenciasDetalle.Cell(renglon, columnadata)
                                                                            .SetValue(horainicial + " - " + horafinal + "  " + value4.Cadena + " - " + value4.NombreTienda)
                                                                            .Style.Font.SetBold(false)
                                                                            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                                                                            .Font.SetFontSize(8);
                                    }


                                    columnadata++;
                                    hojaAsistenciasDetalle.Cell(renglon, columnadata)
                                           .SetValue(value4.estanciaTienda)
                                           .Style.Font.SetBold(false)
                                           .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                                           .Font.SetFontSize(8);

                                    if (value4.IdTipoAsistencia != 2 && value4.IdTipoAsistencia != 3)
                                        horaspordia = horaspordia.Add(value4.estanciaTienda);

                                    renglon++;
                                    columnadata--;
                                }



                                columnadata++;

                                renglonMaxDia = renglonmaximousuario + buscaUsuario.Max(c => c.Children.Count()) + rengloninicio;

                                hojaAsistenciasDetalle.Cell(renglonMaxDia, columnadata)
                               .SetValue(horaspordia)
                               .Style.Font.SetBold(false)
                               .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                               .Font.SetFontSize(8);


                                columnadata++;
                                columnadia += 2;
                            }
                        }
                    }




                    renglonmaximousuario += buscaUsuario.Max(c => c.Children.Count()) + 1;
                    renglon = rengloninicio + renglonmaximousuario;


                    hojaAsistenciasDetalle.Cell(renglon - 1, columnadata)
                      .SetValue(value.DiasAsistidos)
                      .Style.Font.SetBold(true)
                          .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                      .Font.SetFontSize(11);


                    hojaAsistenciasDetalle.Range(renglon - 1, 2, renglon - 1, columnadata)
                       .Style.Fill.BackgroundColor = XLColor.FromArgb(0, 242, 221, 220);


                }
                else
                {
                    renglonmaximousuario += 1;
                    renglon = rengloninicio + renglonmaximousuario;
                }
            }

            hojaAsistenciasDetalle.Rows().AdjustToContents();
            hojaAsistenciasDetalle.Columns().AdjustToContents();
        }

        public void llenahojaDetalleCoordenadas(IXLWorksheet hojaAsistenciasDetalle, List<DataAsistencia> usuarios, string fechainicial, string fechafinal)
        {
            string idusuarios = usuarios.Select(d => d.IdUsuario).ToList().intToString();
            var tiempol = Qry_Tiempo.rangoidsTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));

            var ret = Qry_Asistencia.readAsistencia(idusuarios, tiempol);
            //Asistencias por dia por usuario.
            var asistencias = ajustaIO(ret).OrderBy(i => i.IdUsuario).ThenBy(d => d.StartDate).ToList();


            DateTime fechainiciatime = DateTime.Parse(fechainicial);
            DateTime fechafinatime = DateTime.Parse(fechafinal);
            List<Tiempo> listHeader = Qry_Tiempo.rangoTiempo(fechainiciatime, fechafinatime);

            #region Header
            // Header
            //Encabezado: Armm el cuadro superior del documento de asistencias.
            hojaAsistenciasDetalle.Cell(3, 3)
                .SetValue("Fecha:")
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                 .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Cell(3, 4)
              .SetValue(DateTime.Now.ToLongDateString())
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
               .Font.SetFontSize(11)
               .Border.OutsideBorderColor = XLColor.FromArgb(0, 0, 0, 0);

            hojaAsistenciasDetalle.Cell(3, 6)
               .SetValue("Nombre del COP:")
                .Style.Font.SetBold(false)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Range(3, 7, 3, 10)
               .Merge()
               .SetValue("")
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(11)
               .Border.OutsideBorderColor = XLColor.FromArgb(0, 0, 0, 0);

            hojaAsistenciasDetalle.Cell(5, 5)
             .SetValue("Año:")
              .Style.Font.SetBold(false)
              .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
              .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Cell(5, 6)
           .SetValue(fechafinatime.Year.ToString())
            .Style.Font.SetBold(false)
            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
            .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Range(5, 7, 5, 8)
               .Merge()
               .SetValue("Cliente:")
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Justify)
               .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Range(5, 9, 5, 14)
                  .Merge()
                  .SetValue("")
                  .Style.Font.SetBold(false)
                      .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Justify)
                  .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Cell(6, 5)
             .SetValue("Mes:")
              .Style.Font.SetBold(false)
              .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
              .Font.SetFontSize(11);


            string fecha = fechainiciatime.ToString("MMMM").Equals(fechafinatime.ToString("MMMM")) ? fecha = fechainiciatime.ToString("MMMM") : fecha = fechainiciatime.ToString("MMMM") + "-" + fechafinatime.ToString("MMMM");

            hojaAsistenciasDetalle.Cell(6, 6)
               .SetValue(fecha)
                .Style.Font.SetBold(false)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
                .Font.SetFontSize(11);


            hojaAsistenciasDetalle.Cell(7, 5)
               .SetValue("Periodo:")
                .Style.Font.SetBold(false)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Cell(7, 6)
           .SetValue("Del " + fechainicial + " al " + fechafinal)
            .Style.Font.SetBold(false)
            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left)
            .Font.SetFontSize(11);


            hojaAsistenciasDetalle.Range(1, 5, 1, 23)
               .Merge()
               .SetValue("Reporte de las Asistencias")
               .Style.Font.SetBold(false)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
               .Font.SetFontSize(18);

            int pinkColumn = 3;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("# Empleado")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("Nombre Completo")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
               .Merge()
               .SetValue("Correo")
               .Style.Font.SetBold(true)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
               .Merge()
               .SetValue("Region")
               .Style.Font.SetBold(true)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
               .Merge()
               .SetValue("Puesto")
               .Style.Font.SetBold(true)
                   .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
               .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("Supervisor")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;

            hojaAsistenciasDetalle.Range(10, pinkColumn, 12, pinkColumn)
              .Merge()
              .SetValue("Proyecto")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(10);
            pinkColumn++;


            int rowHeader = 10;
            int columnHeader = 10;
            int rangoDias = (fechafinatime - fechainiciatime).Days;
            int mesAnio = DateTime.Parse(fechainicial).Month;
            int diasr = 0;
            int diasmes = 0;
            //Ciclo que construye el encabezado de la tabla del rango de dias seleccionado.
            do
            {
                DateTime rangodias = fechainiciatime.AddDays(diasr);
                diasmes++;

                hojaAsistenciasDetalle.Cell(rowHeader, columnHeader)
                      .SetValue(rangodias.ToString("ddd"))
                    .Style.Font.SetBold(false)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Font.SetFontSize(11);

                hojaAsistenciasDetalle.Cell(rowHeader + 1, columnHeader)
                    .SetValue(rangodias.Day)
                    .Style.Font.SetBold(false)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    .Font.SetFontSize(11);
                columnHeader++;

                hojaAsistenciasDetalle.Cell(rowHeader, columnHeader)
                  .SetValue("En Tienda")
                  .Style.Font.SetBold(false)
                      .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                  .Font.SetFontSize(11);
                columnHeader++;

                hojaAsistenciasDetalle.Cell(rowHeader, columnHeader)
                .SetValue("Mapa")
                .Style.Font.SetBold(false)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Font.SetFontSize(11);
                columnHeader++;


                if (mesAnio != rangodias.Month)
                {
                    string mes = rangodias.AddMonths(-1).ToString("MMMM");

                    hojaAsistenciasDetalle.Range(9, columnHeader - diasmes * 3, 9, columnHeader - 4)
                       .Merge()
                       .SetValue(mes)
                       .Style.Font.SetBold(true)
                           .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                       .Font.SetFontSize(11);

                    mesAnio = rangodias.Month;
                    diasmes = 1;
                }

                diasr++;
            }
            while (diasr <= rangoDias);

            hojaAsistenciasDetalle.Range(9, columnHeader - diasmes * 3, 9, columnHeader - 1)
              .Merge()
              .SetValue(fechafinatime.ToString("MMMM"))
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
              .Font.SetFontSize(11);

            hojaAsistenciasDetalle.Range(rowHeader, columnHeader, rowHeader + 2, columnHeader)
              .Merge()
              .SetValue("Dias Asistencia")
              .Style.Font.SetBold(true)
                  .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center).Alignment.SetVertical(XLAlignmentVerticalValues.Center)
              .Font.SetFontSize(8);

            hojaAsistenciasDetalle.Range(1, 2, 1, columnHeader)
            .Style.Border.SetBottomBorder(XLBorderStyleValues.Thick)
            .Border.BottomBorderColor = XLColor.FromArgb(0, 192, 0, 0);

            hojaAsistenciasDetalle.Range(10, 2, 12, 9)
               .Style.Fill.BackgroundColor = XLColor.FromArgb(0, 242, 221, 220);


            hojaAsistenciasDetalle.Range(10, 2, 10, columnHeader)
              .Style.Border.SetTopBorder(XLBorderStyleValues.Thick)
              .Border.OutsideBorderColor = XLColor.FromArgb(0, 192, 0, 0);

            hojaAsistenciasDetalle.Range(12, 2, 12, columnHeader)
             .Style.Border.SetBottomBorder(XLBorderStyleValues.Thick)
             .Border.BottomBorderColor = XLColor.FromArgb(0, 192, 0, 0);

            hojaAsistenciasDetalle.Range(5, 5, 7, 14)
               .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
               .Border.OutsideBorderColor = XLColor.FromArgb(0, 0, 0, 0);

            #endregion


            int idUsuario = asistencias[0].IdUsuario;
            int renglon = 13;
            int columna = 2;
            int renglonmaximousuario = 0;
            int renglonMaxDia = 0;

            //Ciclo que itera sobre los usuarios seleccionados en el arbol.
            foreach (var value in usuarios)
            {
                //Columna inicial para contruir el documento de excel.
                columna = 2;

                //Contruye la informacion del empleado.
                hojaAsistenciasDetalle.Cell(renglon, columna)
                   .SetValue(value.Id)
                   .Style.Font.SetBold(false)
                       .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                   .Font.SetFontSize(8);

                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                 .SetValue(value.NumEmpleado)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(8);

                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                  .SetValue(value.Nombre)
                  .Style.Font.SetBold(false)
                      .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                  .Font.SetFontSize(11);

                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                 .SetValue(value.Correo)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);

                columna++;


                hojaAsistenciasDetalle.Cell(renglon, columna)
                 .SetValue(value.Region)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);


                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                 .SetValue(value.Puesto)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);

                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                .SetValue(value.Supervisor)
                .Style.Font.SetBold(false)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Font.SetFontSize(11);

                columna++;

                hojaAsistenciasDetalle.Cell(renglon, columna)
                 .SetValue(value.Proyecto)
                 .Style.Font.SetBold(false)
                     .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                 .Font.SetFontSize(11);

                columna++;


                //Busca las asistencias de un usuario
                var buscaUsuario = asistencias.Where(d => d.IdUsuario == value.IdUsuario).ToList();

                //Definimos en que columna se empezaran a contruir las asistencias del usuario
                int columnadata = 10;

                //Columna donde guardaremos en que posicion empezara a construir la informacion de las asistencias de un dia.
                int columnadia = 10;

                //Renglon donde iniciara la contruccion de las asistencias por usuario.
                int rengloninicio = 13;

                //Si el usuario tiene asistencias
                if (buscaUsuario.Count != 0)
                {
                    //Iteramos sobre un listHeader el cual tiene el rango de dias seleccionados por el usuario a consultar
                    foreach (var value2 in listHeader)
                    {
                        //Le decimos que columndata sera igual a columndia, que es donde empezara a construir la informacion para el excel.
                        columnadata = columnadia;

                        //El reglon donde empezara a construir sera igual al renglo de inicio que es 13 mas renglonmaximousuario que es el maximo de renglones que ocupara un usuario y este sera acumulable
                        renglon = rengloninicio + renglonmaximousuario;
                        TimeSpan horaspordia = new TimeSpan(0, 0, 0);

                        //Buscamos las asistencias del dia que nos manda listheader
                        var buscarAsistenciasDia = buscaUsuario.Where(d => DateTime.Parse(d.StartDate).ToShortDateString() == value2.Fecha.ToShortDateString());
                        //Si el usuario no tiene asistencias en el dia iterado entonces escribimos una F, que es una falta.
                        if (buscarAsistenciasDia.Count() == 0)
                        {
                            hojaAsistenciasDetalle.Cell(renglon, columnadata)
                                .SetValue("F")
                                .Style.Font.SetBold(false)
                                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                                .Font.SetFontSize(11);

                            //Columnadata aumenta en dos ya que son las columnas de "En tienda" y la siguiente es donde se escribira la informacion del siguiente dia.
                            columnadata++;
                            columnadata++;
                            columnadata++;

                            //columndia aumenta en dos mas el acumulado que es el acumulado de columnas de las asistencias de un usuario
                            columnadia += 3;
                        }
                        else
                        {
                            //iteramos sobre las asistencias de un usuario para contruir el excel.
                            foreach (var value3 in buscarAsistenciasDia)
                            {
                                foreach (var value4 in value3.Children)
                                {
                                    string horafinal = value4.EndDate != null ? DateTime.Parse(value4.EndDate).ToShortTimeString() : "";
                                    string horainicial = value4.StartDate != null ? DateTime.Parse(value4.StartDate).ToShortTimeString() : "";

                                    if (value4.LatitudEntrada == 0 && value4.LongitudEntrada == 0)
                                    {
                                        hojaAsistenciasDetalle.Cell(renglon, columnadata)
                                            .SetValue(" x " + " - " + horafinal + "  " + value4.Cadena + " - " + value4.NombreTienda)
                                            .Style.Font.SetBold(false)
                                            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                                            .Font.SetFontSize(8);
                                    }
                                    else if (value4.LongitudSalida == 0 && value4.LatitudSalida == 0)
                                    {
                                        hojaAsistenciasDetalle.Cell(renglon, columnadata)
                                           .SetValue(horainicial + " - " + "x" + "  " + value4.Cadena + " - " + value4.NombreTienda)
                                           .Style.Font.SetBold(false)
                                           .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                                           .Font.SetFontSize(8);
                                    }
                                    else
                                    {
                                        hojaAsistenciasDetalle.Cell(renglon, columnadata)
                                                                            .SetValue(horainicial + " - " + horafinal + "  " + value4.Cadena + " - " + value4.NombreTienda)
                                                                            .Style.Font.SetBold(false)
                                                                            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                                                                            .Font.SetFontSize(8);
                                    }


                                    columnadata++;
                                    hojaAsistenciasDetalle.Cell(renglon, columnadata)
                                           .SetValue(value4.estanciaTienda)
                                           .Style.Font.SetBold(false)
                                           .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                                           .Font.SetFontSize(8);

                                    columnadata++;
                                    hojaAsistenciasDetalle.Cell(renglon, columnadata)
                                           .SetValue("https://www.google.com.mx/maps/place/" + value4.Latitud + " " + value4.Longitud)
                                           .Style.Font.SetBold(false)
                                           .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                                           .Font.SetFontSize(8);
                                    horaspordia = horaspordia.Add(value4.estanciaTienda);

                                    renglon++;
                                    columnadata--;
                                    columnadata--;
                                }

                                columnadata++;

                                renglonMaxDia = renglonmaximousuario + buscaUsuario.Max(c => c.Children.Count()) + rengloninicio;

                                hojaAsistenciasDetalle.Cell(renglonMaxDia, columnadata)
                               .SetValue(horaspordia)
                               .Style.Font.SetBold(false)
                               .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                               .Font.SetFontSize(8);


                                columnadata++;
                                columnadata++;
                                columnadia += 3;
                            }
                        }
                    }




                    renglonmaximousuario += buscaUsuario.Max(c => c.Children.Count()) + 1;
                    renglon = rengloninicio + renglonmaximousuario;

                    hojaAsistenciasDetalle.Cell(renglon - 1, columnadata)
                     .SetValue(value.DiasAsistidos)
                     .Style.Font.SetBold(true)
                         .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                     .Font.SetFontSize(11);

                    hojaAsistenciasDetalle.Range(renglon - 1, 2, renglon - 1, columnadata)
                       .Style.Fill.BackgroundColor = XLColor.FromArgb(0, 242, 221, 220);


                }
                else
                {
                    renglonmaximousuario += 1;
                    renglon = rengloninicio + renglonmaximousuario;
                }
            }

            hojaAsistenciasDetalle.Rows().AdjustToContents();
            hojaAsistenciasDetalle.Columns().AdjustToContents();
        }

        [HttpPost]
        public JsonResult asistenciasUsuario(List<int> idUsuarios, string fechainicial, string fechafinal)
        {
            try
            {
                string idusuarios = idUsuarios.ToList().intToString();
                var tiempol = Qry_Tiempo.rangoidsTiempo(DateTime.Parse(fechainicial), DateTime.Parse(fechafinal));

                var ret = Qry_Asistencia.readAsistencia(idusuarios, tiempol);
                //Asistencias por dia por usuario.
                var asistencias = ajustaIO(ret).OrderBy(i => i.IdUsuario).ThenBy(d => d.StartDate).ToList();
                return Json(new Response { Data = asistencias, Message = "Se han cargado los datos con exito", Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new Response
                {
                    Data = "",
                    Message = e.Message.ToString(),
                    Success = false
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }

    public class TiempoTienda
    {
        public int IdUsuario { get; set; }
        public string Usuario { get; set; }
        public DateTime Fecha { get; set; }
        public TimeSpan Horas { get; set; }
    }
}
