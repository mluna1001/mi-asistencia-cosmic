﻿using Cosmic;
using System;
using System.Web.Mvc;

namespace MiAsistencia.Web.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index() => View();

        [HttpGet]
        public JsonResult GetTimeSession()
        {
            string resultado = "";
            var timeout = Session.Timeout * 60 * 1000;

            //var timer = 0;
            //timeout -= 1000;
            //if (timeout == 0) { resultado = "Your session has expired!"; }

            //    var t = "";
            //decimal sec = Math.Floor(timeout / 1000);
            //ms = ms % 1000;
            //var min = Math.Floor(sec / 60);
            //    sec = sec % 60;
            //    t = two(sec);
            //    var hr = Math.Floor(min / 60);
            //    min = min % 60;
            //t = hr + ":" + two(min) + ":" + t;
            //resultado = "You session will timeout in " + t;

            return Json(0, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetUser()
        {
            return Json(tUsuario, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Login()
        {
            //if (DEBUG)
            //{
            //    SetUsuario(28, new Guid("927608e1-3530-450c-80ff-15db2dc75469"));
            //    return RedirectToAction("Index", "Home");
            //}
            return Redirect(UsuarioCosmicSrv.Login1());
        }

        [ValidateInput(false)]
        public ActionResult redirectLogin(int idusuario, Guid __token)
        {
            SetUsuario(idusuario, __token);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public JsonResult GetCreateUrl()
        {
            //http://localhost:28359//login/RegistraUser?project=d5593cc8-7119-45bf-9bab-21011de94978&ruta=%20%27registrouser%27&dev=True
            var ruta = UsuarioCosmicSrv.CrudUser();
            return Json(ruta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            Session[NameSession] = null;
            tUsuario = null;
            return Redirect(UsuarioCosmicSrv.Login1());
        }

    }
}
