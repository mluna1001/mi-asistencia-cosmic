﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cosmic
{
    public class UsuarioCosmicSrv
    {
        public static Guid tokenApp { get; set; } = new Guid("EB1D874A-3DE1-4BA6-8591-AC42B50702E4");
        private static string uri = ConfigurationManager.AppSettings["UrlUsuarios"];
        private static string endm = ConfigurationManager.AppSettings["EPMenu"];

        /// <summary>
        /// return Redirect(UsuarioCosmicSrv.Login1());
        /// Primer metodo para uso de Usuario Cosmic, usa en un ACTION
        /// </summary>
        public static string Login1()
        {
            bool dev = false;
#if DEBUG
            dev = true;
#endif
            string ruta = $"{uri}/login?project={tokenApp}&dev={dev}";
            return ruta;
        }

        internal static UsuarioCosmicDto Login2(int idusuario, Guid tokenS)
        {
            return Get(idusuario, tokenS);
        }

        internal static string CrudUser()
        {
            bool dev = false;
#if DEBUG
            dev = true;
#endif
            //var s = HttpContext.Current.Request.Url;
            //string hruta = s.AbsoluteUri.Replace(s.AbsolutePath, "");
            //string creaRuta = $"{hruta}/Usuario/redirectCrud";
            string ruta = $"{uri}/login/RegistraUser?project={tokenApp}&ruta=%20%27registrouser%27&dev={dev}";
            //http://localhost:28359//login/RegistraUser?project=d5593cc8-7119-45bf-9bab-21011de94978&ruta=%20%27./registrouser%27&dev=True#/registrouser
            return ruta;
        }

        public void Create()
        {
        }

        public static UsuarioCosmicDto Get(int idusuario, Guid tokenS)
        {
            var tUsuario = new UsuarioCosmicDto();
            var rr = ApiServices.CallRestObject(uri, "userapi/loginUser/", new getUser { idusuario = idusuario, token = tokenS });
            if (rr.Success)
            {
                tUsuario = JsonConvert.DeserializeObject<UsuarioCosmicDto>(rr.Data.ToString());
                return tUsuario;
            }
            return null;
        }

        internal static object SaveUsuario(usuario usuarioCosmicDto)
        {
            var tUsuario = new usuario();
            var rr = ApiServices.CallRestObject(uri, "userapi/SaveUsuarioCosmic/", new getUser { usuario = usuarioCosmicDto, token = tokenApp });
            if (rr.Success)
            {
                tUsuario = JsonConvert.DeserializeObject<usuario>(rr.Data.ToString());
                return tUsuario;
            }
            return null;
        }

        public void Update()
        {
        }

        public void Delete()
        {
        }

        #region Catalogo        
        internal static List<usuario> GetUsuarios()
        {
            var lista = new List<usuario>();
            var rr = ApiServices.CallRestObject(uri, "catalogapi/getUsuarios/", new getUser { token = tokenApp });
            if (rr.Success)
            {
                lista = JsonConvert.DeserializeObject<List<usuario>>(rr.Data.ToString());
                return lista;
            }

            return lista;
        }
        #endregion

        public class getUser
        {
            public int idusuario { get; set; }
            public Guid token { get; set; }
            public usuario usuario { get; set; }
        }
    }
}
