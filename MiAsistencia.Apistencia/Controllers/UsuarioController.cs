﻿using Cosmic;
using MiAsistencia.Comun.Models.DTO;
using MiAsistencia.Comun.Models.ORM;
using MiAsistencia.Dominio.Models.Dto;
using MiAsistencia.Dominio.Models.Querys;
using Newtonsoft.Json;
using System.Configuration;
using System.Web.Mvc;

namespace MiAsistencia.Apistencia.Controllers
{
    public class UsuarioController : Controller
    {
        [HttpPost]
        [Route("miasisapi/getobjusuario/")]
        public ActionResult GetObjectUsuario(SendDataDownloadUser usuario)
        {
            UsuarioDto user = Qry_Usuario.GetUsuario(usuario.IdUsuario);
            BouncyCastleHashing bouncyCastleHashing = new BouncyCastleHashing();
            byte[] hashBytes = bouncyCastleHashing.PBKDF2_SHA256_GetHash(usuario.Contrasena, new byte[16], 10000, 20);

            return Json(new
            {
                usuario.IdUsuario,
                user.UsuarioProyecto.IdProyecto,
                user.UsuarioRegion.IdRegion,
                user.UsuarioCliente.IdCliente,
                LunchTime = (user.UsuarioLunch == null) ? true: user.UsuarioLunch.LunchTime,
                Contrasena = hashBytes
            });
        }

        // GET: Usuarios
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Register(usuario model)
        {
            var response = new Response();

            var userexist = Qry_Usuario.ExistsUserInUC(model);

            if (!userexist.Success)
            {
                model.IdPerfil = (int)PublicRegister.Perfil.Empleado;
                model.Dev = false;
                string uri = ConfigurationManager.AppSettings["UrlUsuarios"];
                var rr = ApiServices.CallRestObject(uri, "user/Guardar/", model);

                if (rr.Success)
                {
                    var userString = rr.Data.To<string>();
                    var userInserted = JsonConvert.DeserializeObject<UsuarioRegistro>(userString);

                    // Insertará apartados para usuario cliente, usuarioproyecto y usuarioregion
                    response = Qry_Usuario.Register(userInserted);
                }
            }
            else
            {
                response = Qry_Usuario.Register(userexist.Data.To<int>());
            }

            return Json(response);
        }

        [HttpPost]
        public JsonResult ExistsUser(usuario model)
        {
            var response = Qry_Usuario.Exists(model);
            return Json(response);
        }
    }
}