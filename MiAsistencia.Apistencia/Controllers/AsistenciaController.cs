﻿using Cosmic;
using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiAsistencia.Apistencia.Controllers
{
    public class AsistenciaController : Controller
    {
        //[Route("~/Asistencia/SaveCheck")]
        [HttpPost]
        //public IHttpActionResult SaveCheck(MiAsistencia.Comun.Models.DTO.Asistencia objAsistencia)
        public ActionResult SaveCheck(Asistencia objAsistencia)
        {
            var statusCheck = Qry_Asistencia.SaveCheck(objAsistencia.To<Asistencia>());
            return Json(statusCheck);
        }

        //[Route("~/Asistencia/SaveAllChecks")]
        [HttpPost]
        public ActionResult SaveAllChecks(List<Asistencia> Asistencia)
        {
            var statusCheck = Qry_Asistencia.SaveAnyCheck(Asistencia);
            return Json(statusCheck);
        }

        [HttpPost]
        public ActionResult ReadCheck(Asistencia objAsistencia)
        {
            var dtoAsis = Qry_Asistencia.ReadCheckByUser(objAsistencia);
            return Json(dtoAsis);
        }

        [HttpPost]
        public ActionResult SaveSignature(Recepcion objRecepcion)
        {
            var dtoAsist = Qry_Asistencia.SaveSignature(objRecepcion);
            return Json(dtoAsist);
        }

        // GET: Asistencia
        public ActionResult Index()
        {
            return View();
        }
    }
}