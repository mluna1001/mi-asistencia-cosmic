﻿using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiAsistencia.Apistencia.Controllers
{
    public class BitacoraController : Controller
    {
        [HttpPost]
        public ActionResult SaveBinnacle(List<Bitacora> Asistencia)
        {
            var statusCheck = Qry_Bitacora.SaveBinnacle(Asistencia);
            return Json(statusCheck);
        }

        // GET: Bitacora
        public ActionResult Index()
        {
            return View();
        }
    }
}