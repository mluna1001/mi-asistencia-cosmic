﻿using Cosmic;
using MiAsistencia.Apistencia.Models.Extensions;
using MiAsistencia.Comun.Models.ORM;
using MiAsistencia.Dominio.Models.Querys;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiAsistencia.Apistencia.Controllers
{
    public class SubordinadoController : Controller
    {
        private string uri = ConfigurationManager.AppSettings["UrlUsuarios"];

        [HttpPost]
        public ActionResult GetSubordinate(SendDataDownloadUser usuario)
        {
            try
            {
                return Json(Qry_Usuario.GetSubordinados(usuario.IdUsuario));
            }
            catch (Exception ex)
            {
                return Json(new { Exception = ex });
            }

            //try
            //{
            //    string endp = "userapi/GetSubordinateUsers";
            //    var r = SrvRest.CallRestParam(uri, endp, usuario.IdUsuario);
            //    var userslistCosmic = JsonConvert.DeserializeObject<List<Subordinado>>(r.Datos.ToString());

            //    Cosmic.Response response = new Response
            //    {
            //        Success = userslistCosmic != null ? true : false,
            //        Data = userslistCosmic
            //    };

            //    return Json(response);
            //}
            //catch (Exception ex)
            //{
            //    return Json(new { Exception = ex });
            //}
        }

        // GET: Subordinado
        public ActionResult Index()
        {
            return View();
        }
    }
}