﻿using MiAsistencia.Comun.Models.ORM;
using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiAsistencia.Apistencia.Controllers
{
    public class TiendaController : Controller
    {
        [HttpPost]
        public ActionResult GetStores(SendDataStore sendData)
        {
            var response = Qry_Tienda.GetTiendas(sendData.IdUsuario, sendData.Latitud, sendData.Longitud);
            return Json(response);
        }

        [HttpPost]
        public ActionResult GetAsignedStores(SendDataStore sendData)
        {
            var response = Qry_Tienda.GetAllTiendas(sendData.IdUsuario);
            return Json(response);
        }

        public ActionResult GetCompanies()
        {
            var response = Qry_Tienda.GetCompanies();
            return Json(response);
        }

        // GET: Tienda
        public ActionResult Index()
        {
            return View();
        }
    }
}