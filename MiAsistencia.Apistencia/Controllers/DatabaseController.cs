﻿using MiAsistencia.Comun.Models.ORM;
using MiAsistencia.Dominio.Models;
using MiAsistencia.Dominio.Models.Querys;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiAsistencia.Apistencia.Controllers
{
    public class DatabaseController : Controller
    {
        [HttpPost]
        public ActionResult SaveDatabase(string baseDatosStr, HttpPostedFileBase file)
        {
            Cosmic.Response response = new Cosmic.Response();

            RespaldoBDUsuarioDto baseDatosDto = Newtonsoft.Json.JsonConvert.DeserializeObject<RespaldoBDUsuarioDto>(baseDatosStr);

            try
            {
                string Dpath = System.Web.Hosting.HostingEnvironment.MapPath("~/Databases/" + baseDatosDto.IdUsuario);
                if (file != null)
                {
                    if (!Directory.Exists(Dpath))
                        Directory.CreateDirectory(Dpath);
                    string path = Path.Combine(Dpath, $"{baseDatosDto.Nombre}_{baseDatosDto.IdUsuario}_{DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss")}.db3");
                    //System.IO.File.WriteAllBytes(path, baseDatosDto.BasedeDatos);
                    file.SaveAs(path);

                    response = Qry_RespaldoBDUsuario.Save(new RespaldoBDUsuario
                    {
                        Fecha = DateTime.Now,
                        IdUsuario = baseDatosDto.IdUsuario,
                        Nombre = baseDatosDto.Nombre,
                        RutaBD = path
                    });

                    response.Message = "Base de datos respaldada correctamente";
                    response.Success = true;
                }
                else
                {
                    response.Message = "No hay archivo de base de datos que guardar.";
                    response.Success = false;
                }
            }
            catch (Exception ex)
            {
                //Qry_Log.InsertLog(ex, )
                //using (var db = new DataBase())
                //{
                //    db.Log.Add(new Models.Log
                //    {
                //        Error = ex.Message,
                //        Proceso = "SaveDataBase",
                //        IdUsuario = baseDatosDto.IdUsuario,
                //        IdRuta = baseDatosDto.CurrentRespAudit.FirstOrDefault().IdRuta,
                //        Registro = DateTime.Now,
                //        Mensaje = ""
                //    });
                //}
                //return new JsonResult { Data = new CosmicResponse { Status = false, Message = ex.ToString() } };
            }
            return Json(response);
        }

        // GET: Database
        public ActionResult Index()
        {
            return View();
        }
    }
}