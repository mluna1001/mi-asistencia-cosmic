﻿using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cosmic
{
    public class ApiServices
    {
        public static Response CallRestObject(string uri, string end, object data)
        {
            var client = new RestClient(uri);
            var request = new RestRequest(end, Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-type", "application/json");
            request.AddParameter("application/json; charset=utf-8", JsonConvert.SerializeObject(data), ParameterType.RequestBody);
            var response = new RestResponse();
            //IRestResponse response = client.ExecuteAsync(request, );

            Task.Run(async () =>
            {
                response = await GetResponseContentAsync(client, request) as RestResponse;
            }).Wait();
            //var jsonResponse = JsonConvert.DeserializeObject<job>
            if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                return new Response { Success = false, Message = "El recurso solicitado no existe en el servidor" };

            var content = response.Content;
            return new Response { Data = content };
        }

        public static Response CallRestParam(string uri, string end, int data)
        {
            var client = new RestClient(uri);
            var request = new RestRequest(end, Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-type", "application/json");
            request.AddParameter("value", data);
            var response = new RestResponse();

            Task.Run(async () =>
            {
                response = await GetResponseContentAsync(client, request) as RestResponse;
            }).Wait();

            var content = response.Content;
            return new Response { Data = content };
        }

        public static Response CallRestGet(string uri, string end)
        {
            var client = new RestClient(uri);
            var request = new RestRequest(end, Method.GET);
            var queryResult = client.Execute(request);

            return new Response { Data = queryResult.Content, Success = queryResult.StatusCode == System.Net.HttpStatusCode.OK };
        }


        public static Task<IRestResponse> GetResponseContentAsync(RestClient theClient, RestRequest theRequest)
        {
            var tcs = new TaskCompletionSource<IRestResponse>();
            theClient.ExecuteAsync(theRequest, response =>
            {
                tcs.SetResult(response);
            });
            return tcs.Task;
        }

        public static Response CallRestParams(string uri, string end, Dictionary<string, object> param)
        {
            var client = new RestClient(uri);

            var request = new RestRequest(end, Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-type", "application/json");
            foreach (var item in param)
                request.AddParameter(item.Key, item.Value);

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return new Response { Data = content };
        }
    }
}
