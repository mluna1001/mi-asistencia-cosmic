﻿using Cosmic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MiAsistencia.Apistencia.Models.Extensions
{
    public class RequestAnswer
    {
        public RequestAnswer()
        {
            Exito = true;
        }

        public RequestAnswer(object usuario)
        {
            this.Exito = true;
            this.Datos = usuario.ToJson();
        }

        public string Datos { get; set; }
        public bool Exito { get; set; }
        public string Mensaje { get; set; }
        public Exception excepcion { get; set; }
    }
}